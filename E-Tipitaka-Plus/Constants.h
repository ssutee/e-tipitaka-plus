//
//  Constants.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 24/10/2013.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <JSONKit/JSONKit.h>

#ifndef E_Tipitaka_Plus_Constants_h
#define E_Tipitaka_Plus_Constants_h

#define AUTO_SYNC NO

#define kGeoServiceURL      @"https://media1.watnapahpong.org/geo.php"

#define S3_HOST             @"https://watnapahpong.s3.dualstack.us-east-1.amazonaws.com/"
#define THAI_HOST           @"https://download.watnapahpong.org/data/etipitaka/ios/"

#define UPDATE_URL          @"https://download.watnapahpong.org/data/etipitaka/ios/ios.json"

#define USER_DATA_HOST      @"https://data.etipitaka.com"

#define DOCUMENT_PATH   NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0]

#define DB_NAME             @"e-tipitaka-db"

#define THAI_DB             @"thai.sqlite"
#define PALI_DB             @"pali.sqlite"
#define THAIMM_DB           @"thaimm.sqlite"
#define THAIMC_DB           @"thaimc.sqlite"
#define THAIMC2_DB          @"thaimc2.sqlite"
#define THAIMS_DB           @"thaims.sqlite"
#define THAIBT_DB           @"thaibt.sqlite"
#define THAIWN_DB           @"thaiwn.sqlite"
#define THAIPB_DB           @"thaipb.sqlite"
#define THAIMS_DB           @"thaims.sqlite"
#define THAIVN_DB           @"thaivn.sqlite"
#define PALIMC_DB           @"palimc.sqlite"
#define ROMANCT_DB          @"romanct.sqlite"
#define PALINEW_DB          @"palinew.sqlite"
#define PALI_DICT_DB        @"pali_thai.sqlite"
#define THAI_DICT_DB        @"thaidict.sqlite"
#define ENG_DICT_DB         @"engdict.sqlite"
#define SUGGEST_DB          @"suggest.sqlite"
#define READING_STATE_DB    @"reading_state.sqlite"

#define OTHERS_DB           @"others.sqlite"
#define BOOKMARK_DB         @"bookmark.sqlite"
#define HISTORY_DB          @"history.sqlite"
#define HIGHLIGHT_DB        @"highlight.sqlite"
#define HIGHLIGHT_NOTE_DB   @"highlight_note.sqlite"
#define HIGHLIGHT_COLOR_DB  @"highlight_color.sqlite"
#define SAVED_LEXICON_DB    @"saved_lexicon.sqlite"
#define USER_DATA_DB        @"userdata.sqlite"
#define TAG_DB              @"tag.sqlite"
#define NOTE_DB             @"note.sqlite"
#define SEARCH_ALL_DB       @"search_all.sqlite"

#define DB_ZIP           @"e-tipitaka-db.zip"

#define EXPORT_PATH     [DOCUMENT_PATH stringByAppendingPathComponent:@"export"]
#define TEMP_PATH       [DOCUMENT_PATH stringByAppendingPathComponent:@"temp"]

#define THAI_DB_PATH          [DOCUMENT_PATH stringByAppendingPathComponent:THAI_DB]
#define PALI_DB_PATH          [DOCUMENT_PATH stringByAppendingPathComponent:PALI_DB]
#define THAIMM_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIMM_DB]
#define THAIMC_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIMC_DB]
#define THAIMC2_DB_PATH       [DOCUMENT_PATH stringByAppendingPathComponent:THAIMC2_DB]
#define THAIMS_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIMS_DB]
#define THAIBT_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIBT_DB]
#define THAIWN_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIWN_DB]
#define THAIPB_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIPB_DB]
#define THAIMS_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIMS_DB]
#define THAIVN_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:THAIVN_DB]
#define ROMANCT_DB_PATH       [DOCUMENT_PATH stringByAppendingPathComponent:ROMANCT_DB]
#define PALIMC_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:PALIMC_DB]
#define PALINEW_DB_PATH       [DOCUMENT_PATH stringByAppendingPathComponent:PALINEW_DB]
#define THAI_DICT_DB_PATH     [DOCUMENT_PATH stringByAppendingPathComponent:THAI_DICT_DB]
#define ENG_DICT_DB_PATH      [DOCUMENT_PATH stringByAppendingPathComponent:ENG_DICT_DB]
#define PALI_DICT_DB_PATH     [DOCUMENT_PATH stringByAppendingPathComponent:PALI_DICT_DB]
#define SUGGEST_DB_PATH       [DOCUMENT_PATH stringByAppendingPathComponent:SUGGEST_DB]
#define READING_STATE_DB_PATH [DOCUMENT_PATH stringByAppendingPathComponent:READING_STATE_DB]

#define OTHERS_DB_PATH          [DOCUMENT_PATH stringByAppendingPathComponent:OTHERS_DB]
#define BOOKMARK_DB_PATH        [DOCUMENT_PATH stringByAppendingPathComponent:BOOKMARK_DB]
#define HISTORY_DB_PATH         [DOCUMENT_PATH stringByAppendingPathComponent:HISTORY_DB]
#define HIGHLIGHT_DB_PATH       [DOCUMENT_PATH stringByAppendingPathComponent:HIGHLIGHT_DB]
#define HIGHLIGHT_NOTE_DB_PATH  [DOCUMENT_PATH stringByAppendingPathComponent:HIGHLIGHT_NOTE_DB]
#define HIGHLIGHT_COLOR_DB_PATH [DOCUMENT_PATH stringByAppendingPathComponent:HIGHLIGHT_COLOR_DB]
#define SAVED_LEXICON_DB_PATH   [DOCUMENT_PATH stringByAppendingPathComponent:SAVED_LEXICON_DB]
#define TAG_DB_PATH             [DOCUMENT_PATH stringByAppendingPathComponent:TAG_DB]
#define NOTE_DB_PATH            [DOCUMENT_PATH stringByAppendingPathComponent:NOTE_DB]
#define SEARCH_ALL_PATH         [DOCUMENT_PATH stringByAppendingPathComponent:SEARCH_ALL_DB]

#define USER_DATA_DB_PATH       [DOCUMENT_PATH stringByAppendingPathComponent:USER_DATA_DB]


#define DB_ZIP_PATH     [DOCUMENT_PATH stringByAppendingPathComponent:DB_ZIP]
#define DB_PATH         [DOCUMENT_PATH stringByAppendingPathComponent:DB_NAME]

#define DatabaseCompleteNotification                    @"DatabaseCompleteNotification"
#define StartDatabaseNotification                       @"StartDatabaseNotification"
#define ChangeDatabaseNotification                      @"ChangeDatabaseNotification"
#define ChangeVolumeNotification                        @"ChangeVolumeNotification"
#define ChangeSelectedVolumeNotification                @"ChangeSelectedVolumeNotification"
#define ChangeVolumeAndPageNotification                 @"ChangeVolumeAndPageNotification"
#define ChangeVolumeAndPageThenCompareNotification      @"ChangeVolumeAndPageThenCompareNotification"
#define ChangeDatabasAndVolumeAndPageNotification       @"ChangeDatabasAndVolumeAndPageNotification"
#define DidChangeVolumeAndPageNotification              @"DidChangeVolumeAndPageNotification"
#define LoadHistoryNotification                         @"LoadHistoryNotification"
#define LoadSearchAllHistoryNotification @"LoadSearchAllHistoryNotification"
#define RefreshHistoryListNotification                  @"RefreshHistoryListNotification"
#define RefreshBookmarkListNotification                 @"RefreshBookmarkListNotification"
#define OpenPaliDictionaryNotification                  @"OpenPaliDictionaryNotification"
#define OpenThaiDictionaryNotification                  @"OpenThaiDictionaryNotification"
#define OpenEnglishDictionaryNotification               @"OpenEnglishDictionaryNotification"

#define UpdateCurrentPageNotification                   @"UpdateCurrentPageNotification"

#define MAXIMUM_PAGE_SIZE       1
#define DEFAULT_FONT_SIZE       26
#define FOOTER_FONT_SIZE_SCALE  0.90

#define DEFAULT_FOREGROUND_COLOR    @"#000000"
#define DEFAULT_BACKGROUND_COLOR    @"#FFFFFF"

#define THAI_0  @"๐"
#define THAI_1  @"๑"
#define THAI_2  @"๒"
#define THAI_3  @"๓"
#define THAI_4  @"๔"
#define THAI_5  @"๕"
#define THAI_6  @"๖"
#define THAI_7  @"๗"
#define THAI_8  @"๘"
#define THAI_9  @"๙"

#define HEADER_TEMPLATE_1     @"%@  หน้าที่ %@  ข้อที่ %@"
#define HEADER_TEMPLATE_2     @"%@  หน้าที่ %@"
#define HEADER_TEMPLATE_3     @"เล่มที่ %ld หน้าที่ %ld"
#define DETAIL_TEMPLATE       @"%ld หน้า: วิ.(%ld) สุต.(%ld) อภิ.(%ld)"
#define DETAIL2_TEMPLATE      @"%ld หน้า"

#define kAlertViewJumpToPage            0
#define kAlertViewJumpToItem            1
#define kAlertViewDeleteHistory         2
#define kAlertViewDeleteAllHistories    3
#define kAlertViewJumpToVolume          4
#define kAlertViewCompareFiveBooks      5

#define kActionSheetJumpTo              0
#define kActionSheetChoosePage          1
#define kActionSheetChooseItem          2
#define kActionSheetChooseBook          3
#define kActionSheetChooseItemIndexing  4
#define kActionSheetFontColor           5
#define kActionSheetHighlight           6
#define kActionSheetChooseDict          7
#define kActionSheetHighlightNote       8

#define ITEM_1_PATTERN  @"^(\\s*)(\\[[๐๑๒๓๔๕๖๗๘๙]+?\\])"
#define ITEM_2_PATTERN  @"(\\{[๐๑๒๓๔๕๖๗๘๙]+?\\})"
#define ITEM_3_PATTERN  @"(\\{[0-9\\.:]+?\\})"
#define ITEM_4_PATTERN  @"\\[([๐๑๒๓๔๕๖๗๘๙]+?)\\]"
#define REF_PATTERN     @"(ฉบับหลวง|สยามรัฐ|มหามกุฏ|มหาจุฬา)/([๐-๙]+)/([๐-๙]+)/([๐-๙]+)"

#define FONT_SIZE   @"fontsize"

#define FOREGROUND_COLOR    @"foreground_color"
#define BACKGROUND_COLOR    @"background_color"

#define kHistoriesFilename      @"Histories.json"
#define kBookmarksFilename      @"Bookmarks.json"
#define kHighlightsFilename     @"Highlights.json"
#define kLexiconsFilename       @"Lexicons.json"

#define BOOK_POSITION_INDEXES  @[@1, @2, @12, @6, @3, @4, @13, @10, @7, @5, @11, @8]

#define ROMAN_KEYS @[@"ā", @"ī", @"ū", @"ṅ", @"ṃ", @"ñ", @"ṭ", @"ḍ", @"ṇ", @"ḷ"]

#define REF_THAI    @"ฉบับหลวง"
#define REF_PALI    @"สยามรัฐ"
#define REF_THAIMM  @"มหามกุฏ"
#define REF_THAIMC  @"มหาจุฬา"

#define REF_BOOKS   @[REF_THAI, REF_PALI, REF_THAIMM, REF_THAIMC]

#define THAI_NUMS @[@"๐",@"๑",@"๒",@"๓",@"๔",@"๕",@"๖",@"๗",@"๘",@"๙"]

#define SYNC_TIME_FORMAT  @"yyyy-MM-dd'T'HH:mm:ss.SSSZ"

#define DEFAULT_HIGHLIGHT_COLOR_1   @"#B982CB"
#define DEFAULT_HIGHLIGHT_COLOR_2   @"#8C38BD"
#define DEFAULT_HIGHLIGHT_COLOR_3   @"#2DF487"
#define DEFAULT_HIGHLIGHT_COLOR_4   @"#99C860"
#define DEFAULT_HIGHLIGHT_COLOR_5   @"#DDEE0E"
#define DEFAULT_HIGHLIGHT_COLOR_6   @"#FDE0D2"
#define DEFAULT_HIGHLIGHT_COLOR_7   @"#B5E0F4"
#define DEFAULT_HIGHLIGHT_COLOR_8   @"#E78EC2"
#define DEFAULT_HIGHLIGHT_COLOR_9   @"#DD28BD"
#define DEFAULT_HIGHLIGHT_COLOR_10  @"#A52A33"

#define READ_DELAY   60
#define SKIM_DELAY   0.5

#endif
