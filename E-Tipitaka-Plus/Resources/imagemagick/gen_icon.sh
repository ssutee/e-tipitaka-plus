#!/bin/bash

set -x
cd icon
#for i in *.png; do convert $i -resize 114x114 `echo result/$i | sed s/@2x.png/.png/` ;done
for i in *.png; do convert $i -resize 114x114 `echo ../iphone-ipad/Icon-iphone.png` ;done
for i in *.png; do convert $i -resize 120x120 `echo ../iphone-ipad/Icon-iphone7.png` ;done
for i in *.png; do convert $i -resize 144x144 `echo ../iphone-ipad/Icon-ipad.png` ;done
for i in *.png; do convert $i -resize 152x152 `echo ../iphone-ipad/Icon-ipad7.png` ;done
for i in *.png; do convert $i -resize 58x58 `echo ../iphone-ipad/Icon-spotlight-iphone.png` ;done
for i in *.png; do convert $i -resize 80x80 `echo ../iphone-ipad/Icon-spotlight-iphone7.png` ;done
for i in *.png; do convert $i -resize 100x100 `echo ../iphone-ipad/Icon-spotlight-ipad.png` ;done
for i in *.png; do convert $i -resize 80x80 `echo ../iphone-ipad/Icon-spotlight-ipad7.png` ;done
for i in *.png; do convert $i -resize 58x58 `echo ../iphone-ipad/Icon-setting-iphone.png` ;done
for i in *.png; do convert $i -resize 58x58 `echo ../iphone-ipad/Icon-setting-ipad.png` ;done
