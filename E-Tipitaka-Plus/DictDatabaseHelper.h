//
//  DictDatabaseHelper.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/1/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDatabase.h>

@interface DictDatabaseHelper : NSObject
;
+ (BOOL)checkDictionaryTable;
+ (NSArray *)queryDictionary:(NSString *)keyword;
+ (NSArray *)queryThaiDictionary:(NSString *)keyword;
+ (NSArray *)queryEnglishDictionary:(NSString *)keyword;

@end
