//
//  ReadingPageViewModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 25/5/15.
//  Copyright (c) 2015 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ETDataModel.h"

@interface ReadingPageViewModel : NSObject

@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, strong) RACSignal *resultsChangeSignal;
@property (nonatomic, strong) RACSignal *pageChangeSignal;
@property (nonatomic, readonly) NSArray *results;
@property (nonatomic, readonly) ETDataModel *dataModel;
@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, assign) SearchType searchType;

+ (instancetype)sharedInstance;

- (void)open:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page keywords:(NSString *)keywords;
- (void)open:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page;
- (void)open:(LanguageCode)code volume:(NSInteger)volume;
- (void)open:(LanguageCode)code;

@end
