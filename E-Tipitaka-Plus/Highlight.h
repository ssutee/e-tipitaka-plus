//
//  Highlight.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMResultSet.h>

@interface Highlight : NSObject

@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, strong) NSString *selection;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, assign) NSRange range;
@property (nonatomic, assign) HighlightType type;
@property (nonatomic, assign) NSInteger color;
@property (nonatomic, assign) NSInteger volume;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) LanguageCode code;

- (id)initFromResultSet:(FMResultSet *)s;
- (NSDictionary *)dictionaryValue;

@end
