//
//  ComparingContainerViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 28/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ReadingContainerViewController.h"
#import "ComparingContainerViewController.h"

@class ComparingViewController;

@protocol ComparingViewControllerDelegate <NSObject>

- (void) comparingViewController:(ComparingViewController *)controller shouldCompare:(ComparingSide)side;

@end

@interface ComparingViewController : ReadingContainerViewController

@property (nonatomic, assign) ComparingSide side;
@property (nonatomic, weak) id<ComparingViewControllerDelegate> delegate;

@end
