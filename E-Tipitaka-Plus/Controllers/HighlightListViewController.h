//
//  HighlightListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "BaseHighlightListViewController.h"

@interface HighlightListViewController : BaseHighlightListViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingOrderSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingTypeSegmentedControl;

@end
