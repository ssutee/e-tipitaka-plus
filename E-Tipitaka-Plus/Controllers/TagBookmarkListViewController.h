//
//  TagBookmarkListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "BaseBookmarkListViewController.h"
#import "Tag.h"

@interface TagBookmarkListViewController : BaseBookmarkListViewController

@property (nonatomic, strong) Tag *tag;

@end
