//
//  ETTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Models.h"

NS_ASSUME_NONNULL_BEGIN

@interface ETTableViewController : UITableViewController

@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, strong, readonly) ETDataModel *dataModel;


@end

NS_ASSUME_NONNULL_END
