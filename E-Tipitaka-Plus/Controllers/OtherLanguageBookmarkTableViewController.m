//
//  OtherLanguageBookmarkTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/4/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherLanguageBookmarkTableViewController.h"
#import "OtherBookmarkListViewController.h"

@interface OtherLanguageBookmarkTableViewController ()

@end

@implementation OtherLanguageBookmarkTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Bookmark List" sender:indexPath];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Bookmark List"]) {
        OtherBookmarkListViewController *controller = segue.destinationViewController;
        controller.code = self.selectedCode;
        controller.user = self.user;
    }
}

@end
