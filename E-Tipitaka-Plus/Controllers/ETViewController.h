//
//  ETViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 15/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Models.h"

@interface ETViewController : UIViewController

@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, strong, readonly) ETDataModel *dataModel;

@end
