//
//  OthersDataDetailTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OthersDataDetailTableViewController.h"
#import "AccountHelper.h"
#import "OtherLanguageTableViewController.h"

#import <AFNetworking/AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "NSDate+Utilities.h"

@interface OthersDataDetailTableViewController ()

@property (nonatomic ,strong) AFHTTPSessionManager *sessionManager;

@end

@implementation OthersDataDetailTableViewController

@synthesize sessionManager = _sessionManager;

- (AFHTTPSessionManager *)sessionManager
{
    if (!_sessionManager) {
        NSURL *baseURL = [NSURL URLWithString:USER_DATA_HOST];
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    }
    return _sessionManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.user.username;
    
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    
    NSString *refreshKey = [NSString stringWithFormat:@"Last refresh %@", self.user.username];
    
    NSDate *lastRefresh = [[NSUserDefaults standardUserDefaults] objectForKey:refreshKey];
    NSDate *now = [NSDate date];
    if (!lastRefresh || [lastRefresh hoursBeforeDate:now] >= 12) {
        [[NSUserDefaults standardUserDefaults] setObject:now forKey:refreshKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self refresh];
    }
}

- (void)refresh:(id)sender
{
    [self refresh];
}

- (void)refresh
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        return;
    }
    
    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [self.sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    
    [self.sessionManager GET:[NSString stringWithFormat:@"user/%@/", [NSNumber numberWithInteger:self.user.pk]] parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSData *jsonData = [responseObject[@"items"] dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSArray *items = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        
        NSMutableArray *requestArray = [NSMutableArray array];
        
        for (NSDictionary *item in items) {
            NSString *name = item[@"fields"][@"name"];
            if ([item[@"fields"][@"platform"] isEqualToString:@"ios"] && ([name isEqualToString:@"bookmark.sqlite"] || [name isEqualToString:@"history.sqlite"])) {
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/%@/%@/", USER_DATA_HOST, [NSNumber numberWithInteger:self.user.pk], name]];
                NSLog(@"%@", url);
                NSMutableURLRequest *request = [[NSURLRequest requestWithURL:url] mutableCopy];
                [request setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
                AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                [AFImageResponseSerializer serializer];
                
                NSString *dataPath = [DOCUMENT_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.user.pk]]];
                
                NSError *error;
                if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
                    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
                }
                
                __block NSString *tmpFile = [dataPath stringByAppendingPathComponent:[name stringByAppendingString:@".tmp"]];
                __block NSString *filePath = [dataPath stringByAppendingPathComponent:name];
                
                [[NSFileManager defaultManager] removeItemAtPath:tmpFile error:&error];
                
                op.outputStream = [NSOutputStream outputStreamToFileAtPath:tmpFile append:NO];
                op.queuePriority = NSOperationQueuePriorityLow;
                [op setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead){
                    
                }];
                [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:tmpFile error:nil] fileSize];
                    if (fileSize) {
                        NSData *data = [[NSFileManager defaultManager] contentsAtPath:tmpFile];
                        [data writeToFile:filePath atomically:YES];
                        [[NSFileManager defaultManager] removeItemAtPath:tmpFile error:nil];
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"%@", error.localizedDescription);
                }];

                [requestArray addObject:op];
            }
        }
        
        if ([requestArray count]) {
            NSArray *batches = [AFURLConnectionOperation batchOfRequestOperations:requestArray progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
            } completionBlock:^(NSArray *operations) {
                [SVProgressHUD showSuccessWithStatus:@"Completed"];
            }];
            
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showWithStatus:@"Downloading..."];
            [[NSOperationQueue mainQueue] addOperations:batches waitUntilFinished:NO];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)alertNoData
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"No data found", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Close", nil) otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - Tale view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *dataPath = [DOCUMENT_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.user.pk]]];
    
    if (indexPath.row == 0 && ![[NSFileManager defaultManager] fileExistsAtPath:[dataPath stringByAppendingPathComponent:@"history.sqlite"]]) { // History
        [self alertNoData];
        return nil;
    } else if (indexPath.row == 1 && ![[NSFileManager defaultManager] fileExistsAtPath:[dataPath stringByAppendingPathComponent:@"bookmark.sqlite"]]) { // Bookmark
        [self alertNoData];
        return nil;
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) { // History
        [self performSegueWithIdentifier:@"History" sender:indexPath];
    } else if (indexPath.row == 1) { // Bookmark
        [self performSegueWithIdentifier:@"Bookmark" sender:indexPath];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"History", nil);
    } else if (indexPath.row == 1) {
        cell.textLabel.text = NSLocalizedString(@"Bookmark", nil);
    }
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    OtherLanguageTableViewController *controller = segue.destinationViewController;
    controller.user = self.user;
    if ([segue.identifier isEqualToString:@"History"]) {
        
    } else if ([segue.identifier isEqualToString:@"Bookmark"]) {
    
    }
}


@end
