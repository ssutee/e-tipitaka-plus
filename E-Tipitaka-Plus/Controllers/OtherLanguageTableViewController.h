//
//  OtherLanguageTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtherUser.h"

@interface OtherLanguageTableViewController : UITableViewController

@property (nonatomic, assign) LanguageCode selectedCode;
@property (nonatomic, strong) OtherUser *user;

@end
