//
//  ReadingContainerViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import <MMDrawerController/MMDrawerBarButtonItem.h>
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import <KNSemiModalViewController/UIViewController+KNSemiModal.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import <QuartzCore/QuartzCore.h>

#import "ReadingContainerViewController.h"
#import "ReadingPageViewController.h"
#import "AdditionalNoteViewController.h"
#import "ComparingContainerViewController.h"
#import "MultiComparingContainerViewController.h"
#import "DictionaryViewController.h"
#import "HighlightViewController.h"
#import "HighlightNoteViewController.h"
#import "PageViewController.h"
#import "BookDatabaseHelper.h"
#import "UserDatabaseHelper.h"
#import "Highlight.h"
#import "NSString+Thai.h"
#import "UIColor+HexHTMLString.h"
#import "SyncDatabaseManager.h"
#import "AccountHelper.h"
#import "ActionSheetPicker.h"
#import "ReadingState.h"
#import "SearchViewController.h"
#import "NSString+Thai.h"
#import "SyncDatabaseManager.h"
#import "WriteNoteViewController.h"

@interface ReadingContainerViewController ()<ReadingPageViewControllerDelegate, HighlightViewControllerDelegate, PageViewControllerDelegate, AdditionalNoteViewControllerDelegate, HighlightNoteViewControllerDelegate, UIDocumentInteractionControllerDelegate>
{
    BOOL _isModalShowing;
    NSInteger _selectedItem;
    BOOL _isDefaultItemIndex;
    NSInteger _selectedHighlightRowId;
}

@property (nonatomic, strong) NSArray *pages, *items;
@property (nonatomic, strong) AdditionalNoteViewController *modalViewController;
@property (nonatomic, strong) HighlightViewController *highlightViewController;
@property (nonatomic, strong) HighlightNoteViewController *highlightNoteViewController;
@property (nonatomic, strong) DictionaryViewController *dictViewController;
@property (nonatomic, strong) NSArray *selectedBook;
@property (nonatomic, strong) NSMutableArray *bookIndexes;
@property (nonatomic, strong) NSMutableArray *references;
@property (nonatomic, strong) NSTimer *skimmedTimer;
@property (nonatomic, strong) NSTimer *readTimer;
@property (nonatomic, strong) ReadingState *readingState;
@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;
@property (nonatomic, strong) WriteNoteViewController *writeNoteViewController;

@end

@implementation ReadingContainerViewController

@synthesize slider = _slider;
@synthesize headerLabel = _headerLabel;
@synthesize shortNoteSwitch = _shortNoteSwitch;
@synthesize pages = _pages, items = _items;
@synthesize modalViewController = _modalViewController;
@synthesize dictViewController = _dictViewController;
@synthesize highlightViewController = _highlightViewController;
@synthesize highlightNoteViewController = _highlightNoteViewController;
@synthesize bookIndexes = _bookIndexes;
@synthesize dictButton = _dictButton;
@synthesize references = _references;
@synthesize readingStateView = _readingStateView;
@synthesize readingState = _readingState;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(semiModalDidHide:) name:kSemiModalDidHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(semiModalDidShow:) name:kSemiModalDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCode:) name:ChangeDatabaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openDictionary:) name:OpenPaliDictionaryNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openDictionary:) name:OpenThaiDictionaryNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openDictionary:) name:OpenEnglishDictionaryNotification object:nil];
        [self addObserver:self forKeyPath:@"readingState" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (ReadingPageViewController *)readingPageViewController
{
    return ((ReadingPageViewController *)self.childViewControllers[0]);
}

- (NSInteger)page
{
    return self.slider.value + [self.dataModel startPageOfVolume:self.volume] - 1;
}

- (void)changeCode:(NSNotification *)notification
{
    self.code = [notification.object intValue];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeObserver:self forKeyPath:@"readingState"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"fractionCompleted"] && [object isKindOfClass:[NSProgress class]]) {
        NSProgress *progress = (NSProgress *)object;
        NSLog(@"Progress is %f", progress.fractionCompleted);
        [SVProgressHUD showProgress:progress.fractionCompleted status:@"Uploading..."];
    } else if ([keyPath isEqualToString:@"readingState"]) {
        NSLog(@"state changed");
        ReadingState *newReadingState = change[@"new"];
        if (newReadingState.state == ReadingStateTypeUnread) {
            [self.readingStateView setBackgroundColor:[UIColor redColor]];
        } else if (newReadingState.state == ReadingStateTypeRead) {
            [self.readingStateView setBackgroundColor:[UIColor greenColor]];
        } else if (newReadingState.state == ReadingStateTypeSkimmed) {
            [self.readingStateView setBackgroundColor:[UIColor yellowColor]];
        }
        self.readingStateView.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(newReadingState.count ? newReadingState.count : 1)]];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.headerLabel.font = [UIFont systemFontOfSize:16];
        self.readingStateView.layer.cornerRadius = 10;
    } else {
        self.readingStateView.layer.cornerRadius = 15;
    }
    self.readingStateView.clipsToBounds = YES;
    
    self.readingPageViewController.readingPageViewDelegate = self;
    [self setupLeftMenuButton];
    [self setupRightMenuButton];
    
    UIMenuItem *translatePaliMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Pali translation", nil) action:@selector(translatePali:)];
    UIMenuItem *translateThaiMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Thai translation", nil) action:@selector(translateThai:)];
    UIMenuItem *highlightMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Highlight", nil) action:@selector(addHighlight:)];
    [UIMenuController sharedMenuController].menuItems = @[translatePaliMenuItem, translateThaiMenuItem, highlightMenuItem];

    if (AUTO_SYNC) {
        [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(doSync:)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    }
    
    [self doSync:nil];
}

- (void)doSync:(NSNotification *)notification
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([self class] == [ReadingContainerViewController class] && [[userDefaults objectForKey:@"auto_sync_enabled_preference"] boolValue]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if (AUTO_SYNC) {
                [[SyncDatabaseManager sharedInstance] syncWithProgressHUD:nil];
            } else {
                [[SyncDatabaseManager sharedInstance] syncManuallyWithProgressHUD:nil inViewController:self];
            }
        });        
    }
}

- (void)forceClosePopupAction
{
    self.readingPageViewController.webView.userInteractionEnabled = NO;
    self.readingPageViewController.webView.userInteractionEnabled = YES;
    [UIMenuController sharedMenuController].menuVisible = NO;
}

- (void)addHighlight:(id)sender
{
    [[SyncDatabaseManager sharedInstance] checkSyncCompleted];
    
    self.highlightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HighlightViewController"];
    
    self.highlightViewController.delegate = self;
    NSString *selection = [self.readingPageViewController.webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
    self.highlightViewController.selection = selection;
    
    NSArray *result = [[self.readingPageViewController.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCharOffsets();"] objectFromJSONString];
    self.highlightViewController.range = NSMakeRange([result[0] integerValue], [result[1] integerValue] - [result[0] integerValue]);

    NSString *position = [self.readingPageViewController.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCountPosition();"];
    self.highlightViewController.position = position.integerValue;
    
    self.highlightViewController.type = HighlightTypeDescription;
    self.highlightViewController.color = 1;
    
    [self forceClosePopupAction];
    
    self.highlightViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:self.highlightViewController animated:YES completion:^{
        _isModalShowing = YES;
    }];
}

- (void)translatePali:(id)sender
{
    NSString *text = [self.readingPageViewController getTextSelection];
    [self openDictionaryWithType:DictionaryTypePali searchText:text];
}

- (void)translateThai:(id)sender
{
    NSString *text = [self.readingPageViewController getTextSelection];
    [self openDictionaryWithType:DictionaryTypeThai searchText:text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    NSString *selection = [self.readingPageViewController.webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
    NSArray *result = [[self.readingPageViewController.webView stringByEvaluatingJavaScriptFromString:@"getSelectionCharOffsets();"] objectFromJSONString];
    NSRange range = NSMakeRange([result[0] integerValue], [result[1] integerValue] - [result[0] integerValue]);
    BOOL overlap = [UserDatabaseHelper checkOverlapHighlightsInVolume:self.volume page:self.page code:self.code range:range];
    
    if ([[selection stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] != 0 && !self.shortNoteSwitch.on) {
        return action == @selector(translatePali:) || action == @selector(translateThai:) || (action == @selector(addHighlight:) && !overlap);
    } else if ([[selection stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] != 0 && self.shortNoteSwitch.on) {
        return action == @selector(translatePali:) || action == @selector(translateThai:);
    }
    return NO;
}

- (void)semiModalDidHide:(NSNotification *)notification
{
    _isModalShowing = NO;
    [self.readingPageViewController.pageViewController update];
}

- (void)semiModalDidShow:(NSNotification *)notification
{
    _isModalShowing = YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (_isModalShowing) {
        [self dismissViewControllerAnimated:YES completion:^{
            _isModalShowing = NO;
        }];
    }
}

-(void)setupLeftMenuButton
{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton
{
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}

- (IBAction)dict:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose dictionary", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Pali-Thai dictionary", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[NSNotificationCenter defaultCenter] postNotificationName:OpenPaliDictionaryNotification object:[NSNumber numberWithInt:DictionaryTypePali]];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Thai dictionary", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[NSNotificationCenter defaultCenter] postNotificationName:OpenPaliDictionaryNotification object:[NSNumber numberWithInt:DictionaryTypeThai]];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Pali-English dictionary", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[NSNotificationCenter defaultCenter] postNotificationName:OpenPaliDictionaryNotification object:[NSNumber numberWithInt:DictionaryTypeEnglish]];
    }]];
    [self showActionSheet:actionSheet atBarButtonItem:self.dictButton];
}

- (IBAction)slide:(UISlider *)slider
{
    self.headerLabel.text = [[NSString stringWithFormat:HEADER_TEMPLATE_2, [self.dataModel volumeTitle:self.volume], [NSNumber numberWithInteger:slider.value + [self.dataModel startPageOfVolume:self.volume] - 1]] stringByReplacingThaiNumeric];
    self.readingPageViewController.pageIsAnimating = YES;
}

- (IBAction)slideEnd:(UISlider *)slider
{
    [self.readingPageViewController jumpToIndex:slider.value - 1];
    self.readingPageViewController.pageIsAnimating = NO;
}
- (IBAction)showModalViewController:(id)sender
{
    [[SyncDatabaseManager sharedInstance] checkSyncCompleted];
    self.modalViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AdditionalNoteViewController"];
    self.modalViewController.code = self.code;
    self.modalViewController.volume = self.volume;
    self.modalViewController.page = self.slider.value;
    self.modalViewController.delegate = self;
    self.modalViewController.firstEdit = YES;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.modalViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    } else {
        self.modalViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    
    [self presentViewController:self.modalViewController animated:YES completion:^{
        _isModalShowing = YES;
    }];
}

- (void)showWriteNoteWithMain:(BOOL)main
{
    [[SyncDatabaseManager sharedInstance] checkSyncCompleted];
    self.writeNoteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WriteNoteViewController"];
    self.writeNoteViewController.code = self.code;
    self.writeNoteViewController.volume = self.volume;
    self.writeNoteViewController.page = self.slider.value;
    self.writeNoteViewController.delegate = self;
    self.writeNoteViewController.main = main;
    self.writeNoteViewController.firstEdit = YES;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.writeNoteViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    } else {
        self.writeNoteViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    
    [self presentViewController:self.writeNoteViewController animated:YES completion:^{
        _isModalShowing = YES;
    }];
}

- (IBAction)showWriteNote:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose note type", nil) message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    alert.popoverPresentationController.barButtonItem = self.noteButton;
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Main note", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showWriteNoteWithMain:YES];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"New note", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showWriteNoteWithMain:NO];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
    [self presentViewController:alert animated:YES completion:^{
    }];
}

- (void)presentDictionary:(NSDictionary *)dict
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Dictionary" bundle:nil];
    UINavigationController *navController = [storyboard instantiateViewControllerWithIdentifier:@"DictionaryNavigationController"];
    DictionaryViewController *controller = (DictionaryViewController *)navController.topViewController;
    controller.dictionaryType = [[dict objectForKey:@"type"] intValue];
    controller.searchText = [dict objectForKey:@"text"];
    
    navController.modalPresentationStyle = UIModalPresentationPageSheet;
    
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)openDictionaryWithType:(DictionaryType)dictionryType searchText:(NSString *)text
{
    [self performSelector:@selector(presentDictionary:) withObject:@{@"type":[NSNumber numberWithInt:dictionryType], @"text":(text ? text : @"")} afterDelay:0];
}

- (void)openDictionary:(NSNotification *)notification
{
    [self openDictionaryWithType:[notification.object intValue] searchText:nil];
}

- (IBAction)zoomIn:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger fontSize = [[userDefaults objectForKey:FONT_SIZE] integerValue] == 0 ? DEFAULT_FONT_SIZE : [[userDefaults objectForKey:FONT_SIZE] integerValue];
    [userDefaults setObject:[NSNumber numberWithInteger:fontSize+1] forKey:FONT_SIZE];
    [userDefaults synchronize];
    [self.readingPageViewController jumpToIndex:self.slider.value-1];
}

- (IBAction)zoomOut:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger fontSize = [[userDefaults objectForKey:FONT_SIZE] integerValue] == 0 ? DEFAULT_FONT_SIZE : [[userDefaults objectForKey:FONT_SIZE] integerValue];
    [userDefaults setObject:[NSNumber numberWithInteger:fontSize-1] forKey:FONT_SIZE];
    [userDefaults synchronize];
    [self.readingPageViewController jumpToIndex:self.slider.value-1];
}

- (IBAction)fontColor:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose font color", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Black", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self changeFontColor:0];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"White", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self changeFontColor:1];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Sepia", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self changeFontColor:2];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [self showActionSheet:actionSheet atBarButtonItem:self.fontColorButton];
}

- (void)changeFontColor:(NSInteger)index
{
    NSString *foregroundColor, *backgroundColor;
    switch (index) {
        case 0:
            foregroundColor = @"#000000";
            backgroundColor = @"#FFFFFF";
            break;
        case 1:
            foregroundColor = @"#FFFFFF";
            backgroundColor = @"#000000";
            break;
        case 2:
            foregroundColor = @"#5E4933";
            backgroundColor = @"#F9EFD8";
            break;
        default:
            break;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:foregroundColor forKey:FOREGROUND_COLOR];
    [userDefaults setObject:backgroundColor forKey:BACKGROUND_COLOR];
    [userDefaults synchronize];
    [self.readingPageViewController jumpToIndex:self.slider.value-1];
}

- (IBAction)jumpTo:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose action", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Read at volume", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self jumpToVolume];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Read at page", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self jumpToPage];
    }]];
    if (self.dataModel.hasIndexNumber) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Read at item", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self jumpToItem];
        }]];
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [self showActionSheet:actionSheet atBarButtonItem:self.jumpToButton];
}

-(void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, aView.bounds.size.width, 50)];
    
    header.text = [[NSString stringWithFormat:@"%@ %@ %@", self.title, NSLocalizedString(@"Page", nil), [NSNumber numberWithInteger:self.page]] stringByReplacingThaiNumeric];
    header.textAlignment = NSTextAlignmentCenter;
    
    // Creates a mutable data object for updating with binary data, like a byte array
    UIWebView *webView = (UIWebView*)aView;
    NSString *heightStr = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
    
    int height = [heightStr intValue];
    CGFloat screenHeight = webView.bounds.size.height;
    int pages = ceil(height / screenHeight);
    
    NSMutableData *pdfData = [NSMutableData data];
    CGRect frame = [webView frame];
    CGRect singlePageRect = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, height+header.bounds.size.height);
    
    UIGraphicsBeginPDFContextToData(pdfData, singlePageRect, nil);
    UIGraphicsBeginPDFPageWithInfo(singlePageRect, nil);
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    [header.layer renderInContext:currentContext];
    CGContextTranslateCTM(currentContext, 0, header.bounds.size.height);
    for (int i = 0; i < pages; i++) {
        // Check to screenHeight if page draws more than the height of the UIWebView
        if ((i+1) * screenHeight  > height) {
            CGRect f = [webView frame];
            f.size.height -= (((i+1) * screenHeight) - height);
            [webView setFrame: f];
        }
        CGContextTranslateCTM(currentContext, 0, (i > 0 ? screenHeight : 0));
        [[[webView subviews] lastObject] setContentOffset:CGPointMake(0, screenHeight * i) animated:NO];
        [webView.layer renderInContext:currentContext];
    }
    
    UIGraphicsEndPDFContext();
    // Retrieves the document directories from the iOS device
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    [webView setFrame:frame];
}

- (IBAction)convertToPdf:(id)sender
{
    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    NSString* fileName = [NSString stringWithFormat:@"%@-%ld-%ld.pdf", self.dataModel.codeString, self.volume, self.page];
    NSString* path = [documentsPath stringByAppendingPathComponent:fileName];
    [self createPDFfromUIView:self.readingPageViewController.webView saveToDocumentsWithFileName:fileName];
    NSURL *URL = [NSURL fileURLWithPath:path];
    self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
    self.documentInteractionController.delegate = self;
    [self.documentInteractionController presentOpenInMenuFromBarButtonItem:self.pdfButton animated:YES];
}

- (void)compareFiveBooks
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter volume number", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = [NSString stringWithFormat:@"%d - %d", 1, 5];
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        int volume = [[alertController textFields][0] text].intValue;
        if (volume > 0 && volume <= 5) {
            PageInfo *pageInfo = [[PageInfo alloc] initWithCode:LanguageCodeThaiFiveBooks volume:volume page:1];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self compareWithPageInfo:pageInfo];
            });
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)compareReferences
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose an item", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    int buttonIndex = 0;
    for (NSArray *ref in self.references) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:ref[0] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self doCompare:buttonIndex];
        }]];
        buttonIndex++;
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [self showActionSheet:actionSheet atBarButtonItem:self.compareToButton];
}

- (void)compare
{
    int pageNumber = self.slider.value;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        self.items = [self.dataModel queryItemsInVolume:self.volume page:pageNumber];
        if (self.items.count > 1) {
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose an item", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            int buttonIndex = 0;
            for (NSNumber *item in self.items) {
                [actionSheet addAction:[UIAlertAction actionWithTitle:[[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Item", nil), item] stringByReplacingThaiNumeric] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self doCompare:buttonIndex];
                }]];
                buttonIndex++;
            }
            [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showActionSheet:actionSheet atBarButtonItem:self.compareToButton];
            });
        } else if (self.items.count == 1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
                [SVProgressHUD showWithStatus:@"Loading..."];
            });
            PageInfo *pageInfo = [self createComparingPageInfo:[self.items[0] intValue]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self compareWithPageInfo:pageInfo];
            });
        }
    });
}

- (void)selectBook:(NSInteger)buttonIndex
{
    int code = [[self.bookIndexes objectAtIndex:buttonIndex] intValue];
    self.selectedBook = [BookDatabaseHelper book:code];
    if (!self.selectedBook) {
        [self compareFiveBooks];
    } else if (self.selectedBook && !self.dataModel.hasIndexNumber) {
        [self compareReferences];
    } else if (self.selectedBook) {
        [self compare];
    }
}

- (IBAction)compare:(id)sender
{
    self.bookIndexes = [[NSMutableArray alloc] init];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose the book that you want to compare with", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    if (self.dataModel.hasIndexNumber) {
        int buttonIndex = 0;
        for (NSArray *book in [BookDatabaseHelper books]) {
            if (self.code != [book[0] intValue]) {
                [self.bookIndexes addObject:book[0]];
                [actionSheet addAction:[UIAlertAction actionWithTitle:book[2] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self selectBook:buttonIndex];
                }]];
                buttonIndex++;
            }
        }
        if ([BookDatabaseHelper book:self.code]) {
            [self.bookIndexes addObject:[BookDatabaseHelper book:self.code][0]];
            [actionSheet addAction:[UIAlertAction actionWithTitle:[BookDatabaseHelper book:self.code][2] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self selectBook:buttonIndex];
            }]];
        }
    } else {
        PageResult *result = [self.dataModel queryPage:self.slider.value+([self.dataModel startPageOfVolume:self.volume]-1) inVolume:self.volume];
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"([๐๑๒๓๔๕๖๗๘๙][–๐๑๒๓๔๕๖๗๘๙\\s\\-,]+)/([–๐๑๒๓๔๕๖๗๘๙\\s\\-,]+)/([–๐๑๒๓๔๕๖๗๘๙\\s\\-,]+[๐๑๒๓๔๕๖๗๘๙])" options:0 error:&error];
        NSArray *matches =[regex matchesInString:result.content options:0 range:NSMakeRange(0, result.content.length)];
        self.references = [[NSMutableArray alloc] init];
        for (NSTextCheckingResult* match in matches) {
            [self.references addObject:@[[result.content substringWithRange:[match range]], [[result.content substringWithRange:[match rangeAtIndex:1]] numberValueFromThaiNumberic], [[result.content substringWithRange:[match rangeAtIndex:2]] numberValueFromThaiNumberic], [[result.content substringWithRange:[match rangeAtIndex:3]] numberValueFromThaiNumberic]]];
        }
        int buttonIndex = 0;
        if (self.references.count) {
            for (NSArray *bookInfo in [BookDatabaseHelper comparableBooks]) {
                [self.bookIndexes addObject:bookInfo[0]];
                [actionSheet addAction:[UIAlertAction actionWithTitle:bookInfo[2] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self selectBook:buttonIndex];
                }]];
                buttonIndex++;
            }
        }
        [self.bookIndexes addObject:[NSNumber numberWithInt:LanguageCodeThaiFiveBooks]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Five books", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self selectBook:buttonIndex];
        }]];
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [self showActionSheet:actionSheet atBarButtonItem:self.compareToButton];
}

- (void)doCompare:(NSInteger)buttonIndex
{
    if (self.dataModel.hasIndexNumber) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
                [SVProgressHUD showWithStatus:@"Loading..."];
            });
            PageInfo *pageInfo = [self createComparingPageInfo:[self.items[buttonIndex] intValue]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self compareWithPageInfo:pageInfo];
            });
        });
    } else {
        PageInfo *pageInfo = [self createComparingPageInfo:[self.references[buttonIndex][3] integerValue] volume:[self.references[buttonIndex][1] integerValue]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self compareWithPageInfo:pageInfo];
        });
    }
}

- (void)compareWithPageInfo:(PageInfo *)pageInfo
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self performSegueWithIdentifier:@"multicompare" sender:pageInfo];
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Comparing" bundle:nil];
        ComparingContainerViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ComparingContainer"];
        controller.leftPageInfo = [[PageInfo alloc] initWithCode:self.code volume:self.volume page:self.slider.value];
        controller.rightPageInfo = pageInfo;
        controller.switchOn = self.shortNoteSwitch.on;
        controller.keywords = self.readingPageViewController.pageViewController.keywords;
        controller.searchType = self.readingPageViewController.pageViewController.searchType;
        [SVProgressHUD dismiss];
        [self presentViewController:controller animated:YES completion:nil];        
    }
}

- (IBAction)searchAll:(id)sender
{
    [[SyncDatabaseManager sharedInstance] checkSyncCompleted];
    UITabBarController *controller = ((UITabBarController *)self.mm_drawerController.rightDrawerViewController);
    controller.selectedViewController = controller.viewControllers[3];
    [self.mm_drawerController openDrawerSide:MMDrawerSideRight animated:YES completion:nil];

}

- (IBAction)search:(id)sender
{
    [[SyncDatabaseManager sharedInstance] checkSyncCompleted];
    UITabBarController *controller = ((UITabBarController *)self.mm_drawerController.rightDrawerViewController);
    controller.selectedViewController = controller.viewControllers[1];
    [self.mm_drawerController openDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (IBAction)toggleHighlightDetail:(id)sender
{
    [self.readingPageViewController.pageViewController update];
}

- (PageInfo *)createComparingPageInfo:(NSInteger)item volume:(NSInteger)aVolume
{
    NSArray *result1 = [self.dataModel convertToPivot:aVolume page:self.slider.value item:item];
    NSNumber *rVolume = result1[0];
    NSNumber *rItem = result1[1];
    NSNumber *rSubItem = result1[2];
    
    NSArray *result2 = [[SharedDataModel sharedDataModel:[self.selectedBook[0] intValue]] convertFromPivot:rVolume.integerValue item:rItem.integerValue subItem:rSubItem.integerValue];
    NSNumber *volume = result2[0];
    NSNumber *page = result2[1];
    return [[PageInfo alloc] initWithCode:[self.selectedBook[0] intValue] volume:volume.integerValue page:page.integerValue];
}

- (PageInfo *)createComparingPageInfo:(NSInteger)item
{
    return [self createComparingPageInfo:item volume:self.volume];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"multicompare"]) {
        MultiComparingContainerViewController *controller = (MultiComparingContainerViewController *)segue.destinationViewController;
        controller.switchOn = self.shortNoteSwitch.on;
        controller.keywords = self.readingPageViewController.pageViewController.keywords;
        controller.searchType = self.readingPageViewController.pageViewController.searchType;
        controller.firstPageInfo = [[PageInfo alloc] initWithCode:self.code volume:self.volume page:self.slider.value];
        controller.secondPageInfo = sender;
        [SVProgressHUD dismiss];
    }
}

- (void)jumpToVolume
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter volume number", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = [NSString stringWithFormat:@"%d - %ld", 1, [self.dataModel totalVolumes]];
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        int volume = [[alertController textFields][0] text].intValue;
        if (volume > 0 && volume <= [self.dataModel totalVolumes]) {
            [self.readingPageViewController open:self.dataModel.code volume:volume page:1];
            [[NSNotificationCenter defaultCenter] postNotificationName:ChangeSelectedVolumeNotification object:[NSNumber numberWithInteger:volume]];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)jumpToPage
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter page number", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = [NSString stringWithFormat:@"%ld - %ld", [self.dataModel startPageOfVolume:self.volume], [self.readingPageViewController.dataModel totalPagesOfVolume:self.volume]];
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        int page = [[alertController textFields][0] text].intValue;
        [self.readingPageViewController jumpToIndex:page-[self.dataModel startPageOfVolume:self.volume]];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showJumpToItemAlert
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Please enter item number", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = [NSString stringWithFormat:@"1 - %ld", [self.readingPageViewController.dataModel totalItemsOfVolume:self.volume useDefaultItemIndexing:_isDefaultItemIndex]];
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _selectedItem = [[alertController textFields][0] text].intValue;
       self.pages = [self.readingPageViewController.dataModel queryPagesOfItem:_selectedItem inVolume:self.volume isDefault:_isDefaultItemIndex];
       if (self.pages.count == 1) {
           [self.readingPageViewController jumpToIndex:[self.pages[0] intValue]-1 withItem:_selectedItem];
       } else if (self.pages.count > 1) {
           [self choosePage:self.pages];
       }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)jumpToItem
{
    if (self.dataModel.hasDifferentItemIndex) {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose item indexing", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"PaliSiam/RoyalThai", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            _isDefaultItemIndex = YES;
            [self showJumpToItemAlert];
        }]];
        [actionSheet addAction:[UIAlertAction actionWithTitle:self.dataModel.itemIndexingSystemName style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            _isDefaultItemIndex = NO;
            [self showJumpToItemAlert];
        }]];
        [self showActionSheet:actionSheet atBarButtonItem:self.jumpToButton];
    } else {
        _isDefaultItemIndex = YES;
        [self showJumpToItemAlert];
    }
}

- (void)showActionSheet:(UIAlertController *)actionSheet atBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            actionSheet.popoverPresentationController.barButtonItem = barButtonItem;
        }
        [self presentViewController:actionSheet animated:YES completion:nil];
    });
}

- (void)choosePage:(NSArray *)pages
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose a page", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    for (NSNumber *page in [pages sortedArrayUsingSelector:@selector(compare:)]) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:[[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Page", nil), page] stringByReplacingThaiNumeric] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self.readingPageViewController jumpToIndex:page.integerValue-1 withItem:_selectedItem];
        }]];
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self showActionSheet:actionSheet atBarButtonItem:self.jumpToButton];
}

- (void)chooseTags
{
    NSArray *names = [UserDatabaseHelper queryTagNames];
    if (!names.count) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"No tag found", nil) message:NSLocalizedString(@"Please add a tag", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"Select a tag", nil)
                                            rows:names
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           Highlight *highlight = [UserDatabaseHelper getHighlightByRowId:_selectedHighlightRowId];
                                           [UserDatabaseHelper addTagItems:@"highlight" rowId:highlight.rowId ofTag:selectedValue withCode:self.code];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:self.slider];
}

- (void)skimPage:(NSTimer *)timer
{
    [self.readTimer invalidate];
    self.readTimer  = [NSTimer scheduledTimerWithTimeInterval:READ_DELAY  target:self selector:@selector(readPage:) userInfo:timer.userInfo repeats:NO];
    PageInfo *pageInfo = timer.userInfo;
    ReadingState *readingState = [ReadingState new];
    readingState.code = pageInfo.code;
    readingState.volume = pageInfo.volume;
    readingState.page = pageInfo.page;
    readingState.state = ReadingStateTypeSkimmed;
    self.readingState = [UserDatabaseHelper addReadingState:readingState];
}

- (void)readPage:(NSTimer *)timer
{
    PageInfo *pageInfo = timer.userInfo;
    ReadingState *readingState = [ReadingState new];
    readingState.code = pageInfo.code;
    readingState.volume = pageInfo.volume;
    readingState.page = pageInfo.page;
    readingState.state = ReadingStateTypeRead;
    self.readingState = [UserDatabaseHelper addReadingState:readingState];
}

#pragma mark - Button Handlers

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [[SyncDatabaseManager sharedInstance] checkSyncCompleted];
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

#pragma mark - ReadingPageViewControllerDelegate

- (NSInteger)readingPageViewControllerCurrentPage:(ReadingPageViewController *)controller
{
    return self.page;
}

- (NSInteger)readingPageViewControllerCurrentNumber:(ReadingPageViewController *)controller
{
    return self.number;
}

- (void)readingPageViewController:(ReadingPageViewController *)controller changePage:(NSInteger)page items:(NSArray *)items
{
    self.slider.value = page;
    
    NSString *itemText = @"";
    if (items.count == 1) {
        itemText = [NSString stringWithFormat:@"%@", items[0]];
    } else if (items.count > 1) {
        itemText = [NSString stringWithFormat:@"%@-%@", items.firstObject, items.lastObject];
    }
    
    page += [self.dataModel startPageOfVolume:self.volume] - 1;
    
    self.headerLabel.text = self.dataModel.hasIndexNumber ? [[NSString stringWithFormat:HEADER_TEMPLATE_1, [self.dataModel volumeTitle:self.volume], [NSNumber numberWithInteger:page], itemText] stringByReplacingThaiNumeric] : [[NSString stringWithFormat:HEADER_TEMPLATE_2, [self.dataModel volumeTitle:self.volume], [NSNumber numberWithInteger:page]] stringByReplacingThaiNumeric];

    [self.readTimer invalidate];
    [self.skimmedTimer invalidate];
    
    PageInfo *pageInfo = [[PageInfo alloc] initWithCode:self.dataModel.code volume:self.volume page:page];
    self.skimmedTimer  = [NSTimer scheduledTimerWithTimeInterval:SKIM_DELAY  target:self selector:@selector(skimPage:) userInfo:pageInfo repeats:NO];
    
    self.readingState = [UserDatabaseHelper getReadingState:self.dataModel.code volume:self.volume page:page];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DidChangeVolumeAndPageNotification object:@{@"volume":[NSNumber numberWithInteger:self.volume], @"page":[NSNumber numberWithInteger:page]}];
}

- (void)readingPageViewController:(ReadingPageViewController *)controller changeVolume:(NSInteger)volume pages:(NSInteger)pages code:(LanguageCode)code
{
    self.code = code;

    self.title = [[NSString stringWithFormat:@"%@ %@ %@", self.dataModel.shortTitle, NSLocalizedString(@"Volume", nil), [NSNumber numberWithInteger:volume]] stringByReplacingThaiNumeric];
    
    self.slider.value = 1;
    self.slider.minimumValue = 1;
    self.slider.maximumValue = pages;

    self.volume = volume;
}

- (void)readingPageViewController:(ReadingPageViewController *)controller isAnimating:(BOOL)animating
{
    self.slider.enabled = !animating;
    self.shortNoteSwitch.enabled = !animating;
}

- (void)readingPageViewControllerPopupBookmark:(ReadingPageViewController *)controller
{
    self.shortNoteSwitch.on = YES;
    [self.readingPageViewController.pageViewController update];
    double delayInSeconds = 0.25;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.readingPageViewController.pageViewController.webView stringByEvaluatingJavaScriptFromString:@"scrollToItem('extra');"];
    });
}

- (void)readingPageViewControllerJumpToHighlight:(ReadingPageViewController *)controller
{
    self.shortNoteSwitch.on = YES;
}

- (void)readingPageViewControllerRequestComparing
{
    [self compare:nil];
}

- (void)showHighlightController
{
    self.highlightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HighlightViewController"];
    self.highlightViewController.delegate = self;
    Highlight *highlight = [UserDatabaseHelper getHighlightByRowId:_selectedHighlightRowId];
    self.highlightViewController.selection = highlight.selection;
    self.highlightViewController.position = highlight.position;
    self.highlightViewController.range = highlight.range;
    self.highlightViewController.type = highlight.type;
    self.highlightViewController.color = highlight.color;
    self.highlightViewController.note = highlight.note;
    self.highlightViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:self.highlightViewController animated:YES completion:^{
        _isModalShowing = YES;
    }];
}

- (void)showHighlightNoteController
{
    Highlight *highlight = [UserDatabaseHelper getHighlightByRowId:_selectedHighlightRowId];
    
    self.highlightNoteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HighlightNoteViewController"];
    self.highlightNoteViewController.delegate = self;
    self.highlightNoteViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    self.highlightNoteViewController.highlight = highlight;
    [self presentViewController:self.highlightNoteViewController animated:YES completion:^{
    }];
}

- (void)deleteSelectedHighlight
{
    [self.readingPageViewController.pageViewController removeHighlight:[UserDatabaseHelper getHighlightByRowId:_selectedHighlightRowId]];
    [UserDatabaseHelper deleteHighlight:_selectedHighlightRowId];
    [self.readingPageViewController.pageViewController update];
}

#pragma mark - HighlightViewControllerDelegate

- (void)highlightViewController:(HighlightViewController *)controller didSaveWithSelection:(NSString *)selection withPosition:(NSInteger)position andNote:(NSString *)note inRange:(NSRange)range useType:(HighlightType)type withColor:(NSInteger)color
{
    self.highlightViewController.delegate = nil;
    [self dismissViewControllerAnimated:YES completion:NULL];
    Highlight *highlight = [[Highlight alloc] init];
    highlight.selection = selection;
    highlight.position = position;
    highlight.note = note;
    highlight.type = type;
    highlight.color = color;
    highlight.volume = self.volume;
    highlight.page = self.page;
    highlight.code = self.code;
    highlight.range = range;
    if ((highlight.rowId = [UserDatabaseHelper checkExistingHighlight:highlight]) > -1) {
        [UserDatabaseHelper updateHighlight:highlight];
    } else {
        [UserDatabaseHelper saveHighlight:highlight];
    }
    
    [self.readingPageViewController.pageViewController update];
}

#pragma mark - PageViewControllerDelegate

- (void)pageViewController:(PageViewController *)controller didClickAtHighlight:(NSInteger)rowId
{
    _selectedHighlightRowId = rowId;
    Highlight *highlight = [UserDatabaseHelper getHighlightByRowId:rowId];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@ [%@]", highlight.selection, highlight.note] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Edit", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSelector:@selector(showHighlightController) withObject:nil afterDelay:0.5];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Add tag", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSelector:@selector(chooseTags) withObject:nil afterDelay:0.5];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self deleteSelectedHighlight];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    CGRect rect = self.view.frame;
    rect.origin.x = self.view.frame.size.width / 20;
    rect.origin.y = self.view.frame.size.height / 20;
    actionSheet.popoverPresentationController.sourceView = self.view;
    actionSheet.popoverPresentationController.sourceRect = rect;
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)pageViewController:(PageViewController *)controller didClickAtHighlightNote:(NSInteger)rowId
{
    _selectedHighlightRowId = rowId;
    Highlight *highlight = [UserDatabaseHelper getHighlightByRowId:rowId];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@ [%@]", highlight.selection, highlight.note] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Add highlight", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self performSelector:@selector(showHighlightNoteController) withObject:nil afterDelay:0.5];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    CGRect rect = self.view.frame;
    rect.origin.x = self.view.frame.size.width / 20;
    rect.origin.y = self.view.frame.size.height / 20;
    actionSheet.popoverPresentationController.sourceView = self.view;
    actionSheet.popoverPresentationController.sourceRect = rect;
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (BOOL)pageViewControllerShouldShowHighlightDetail:(PageViewController *)controller
{
    return self.shortNoteSwitch.on;
}

#pragma mark - HighlightNoteViewControllerDelegate

- (void)highlightNoteViewController:(HighlightNoteViewController *)controller didChangeHighlight:(Highlight *)highlight
{
    [self.readingPageViewController.pageViewController update];
}

#pragma mark - AdditionalNoteViewControllerDelegate

- (void)additionalNoteViewControllerDidDismiss:(AdditionalNoteViewController *)controller
{
    _isModalShowing = NO;
    [self.readingPageViewController.pageViewController update];
}

- (void)additionalNoteViewController:(AdditionalNoteViewController *)controller changePage:(PageInfo *)pageInfo
{
    
}

@end
