//
//  OtherLanguageBookmarkTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/4/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherLanguageTableViewController.h"

@interface OtherLanguageBookmarkTableViewController : OtherLanguageTableViewController

@end
