//
//  SearchAllTableViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETTableViewController.h"
#import "UIViewController+MMDrawerController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchAllTableViewController : ETTableViewController

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

NS_ASSUME_NONNULL_END
