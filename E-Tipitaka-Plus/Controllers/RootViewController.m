//
//  RootViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 23/10/2013.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "RootViewController.h"
#import "SyncDatabaseManager.h"
#import "DownloadDatabaseHelper.h"

#import <AFNetworking/AFNetworking.h>
#import "SVProgressHUD.h"
#import "UserDatabaseHelper.h"
#import <JSONKit/JSONKit.h>
#import <Bolts/Bolts.h>
#import <SSZipArchive/SSZipArchive.h>
#include <sys/param.h>
#include <sys/mount.h>

@import AppCenter;
@import AppCenterCrashes;

@interface RootViewController ()

@property (nonatomic, strong) DownloadDatabaseHelper *downloadHelper;

@end

@implementation RootViewController

- (void)viewDidLoad
{
    [MSAppCenter start:@"97c329dd-680c-65c0-705d-2c7dc633ca8b" withServices:@[
      [MSCrashes class]
    ]];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    self.downloadHelper = [DownloadDatabaseHelper sharedInstance];
    self.downloadHelper.isFirstTime = YES;
    self.downloadHelper.viewController = self;
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.downloadHelper.isFirstTime) {
        [self.downloadHelper downloadDatabases];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
}

-(void)tapToDismiss:(NSNotification *)notification{
    [SVProgressHUD dismiss];
    [[SyncDatabaseManager sharedInstance] cancelAllUploadTasks];
}

@end
