//
//  MainMenuViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 24/10/2013.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "UIViewController+MMDrawerController.h"

@interface MainMenuViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>

@end
