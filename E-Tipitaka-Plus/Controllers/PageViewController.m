//
//  PageViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 30/10/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "PageViewController.h"
#import "NSString+Thai.h"
#import "UIColor+HexHTMLString.h"
#import "UserDatabaseHelper.h"
#import "Highlight.h"
#import "UIWebView+Javascript.h"
#import "HighlightColorList.h"

@interface PageViewController ()<UIWebViewDelegate, UIGestureRecognizerDelegate>
{
    BOOL _firstTime, _loadedJSFiles;
}

@property (nonatomic, strong) NSString *foregroundColor;
@property (nonatomic, strong) NSString *backgroundColor;

@end

@implementation PageViewController

@synthesize webView = _webView;
@synthesize pageIndex = _pageIndex;
@synthesize content = _content;
@synthesize htmlContent = _htmlContent;
@synthesize footer = _footer;
@synthesize keywords = _keywords;
@synthesize searchType = _searchType;
@synthesize items = _items;
@synthesize anchoredItem = _anchoredItem;
@synthesize shouldScrollToKeywords = _shouldScrollToKeywords;
@synthesize foregroundColor = _foregroundColor;
@synthesize backgroundColor = _backgroundColor;
@synthesize volume = _volume;
@synthesize page = _page;
@synthesize code = _code;
@synthesize delegate = _delegate;

- (void)setHtmlContent:(NSString *)htmlContent
{
    _htmlContent = [htmlContent stringByReplacingOccurrencesOfString:@"\r\n" withString:@"\n"];
}

- (NSString *)content
{
    return _footer ? [NSString stringWithFormat:@"%@\n%@", _content, _footer] : _content;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.webView.delegate = self;
    self.webView.scrollView.bounces = NO;
    self.webView.scalesPageToFit = YES;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.delegate = self;    
    [self.webView addGestureRecognizer:tapRecognizer];
    _firstTime = YES;
}

- (void)handleGesture:(UIGestureRecognizer *)sender
{
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_firstTime) {
        [self reload];
        _firstTime = NO;
    } else {
        [self update];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    self.fontSize = [[userDefaults objectForKey:FONT_SIZE] integerValue] == 0 ? DEFAULT_FONT_SIZE : [[userDefaults objectForKey:FONT_SIZE] integerValue];
    self.foregroundColor = [userDefaults objectForKey:FOREGROUND_COLOR] ? [userDefaults objectForKey:FOREGROUND_COLOR] : DEFAULT_FOREGROUND_COLOR;
    self.backgroundColor = [userDefaults objectForKey:BACKGROUND_COLOR] ? [userDefaults objectForKey:BACKGROUND_COLOR] : DEFAULT_BACKGROUND_COLOR;
    self.webView.backgroundColor = [UIColor pxColorWithHexValue:self.backgroundColor];
    self.view.backgroundColor = [UIColor pxColorWithHexValue:self.backgroundColor];
}

- (void)reload
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    if (self.content || self.htmlContent) {
        [self.webView loadHTMLString:  [self createHtmlContent:self.htmlContent ? self.htmlContent : self.content] baseURL:[NSURL URLWithString:@"https://www.etipitaka.com"]];
    } else {
        NSLog(@"Something wrong!!!");
    }
}

- (void)update
{
    NSString *html = [self createHtmlContent:self.htmlContent ? self.htmlContent : self.content];
    
    html = [html stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    html = [html stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"updateHtml('%@');", [html JSONString]]];
    [self addHighlightsForHtmlContent:self.webView];
    [self highlight:self.webView withKeywords:self.keywords];
}

- (void)removeHighlight:(Highlight *)highlight
{
    if (!self.htmlContent) { return; }
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"removeHighlight(%@,%@);", [NSNumber numberWithInteger:highlight.rowId], [NSNumber numberWithInt:highlight.type]]];
}

- (void)highlight:(UIWebView *)webView withKeywords:(NSString *)keywords
{
    if (keywords.length) {
        NSMutableArray *terms = [[NSMutableArray alloc] init];
        for (NSString *token in [self.keywords componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]) {
            [terms addObject:[token stringByReplacingOccurrencesOfString:@"+" withString:@" "]];
        }
        NSString *script = [NSString stringWithFormat:@"search('%@', %d);", [terms componentsJoinedByString:@"|"], self.searchType];
        NSLog(@"%@", script);
        [webView stringByEvaluatingJavaScriptFromString:script];
    }
}

- (NSString *)createHtmlContent:(NSString *)content
{
    content = [self addNotes:content];
    content = [content stringByReplacingOccurrencesOfString:@"\n" withString:@" <br/>"];
    
    for (NSNumber *item in self.items) {
        NSString *thaiItem = [[NSString stringWithFormat:@"%@", item] stringByReplacingThaiNumeric];
        content = [content stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@]", thaiItem] withString:[NSString stringWithFormat:@"<span class=\"item1\" id=\"i%@\">[%@]</span>", item, thaiItem]];
    }

    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:ITEM_2_PATTERN options:NSRegularExpressionAnchorsMatchLines error:&error];
    content = [regex stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, content.length) withTemplate:@"<span class=\"item2\">$1</span>"];

    regex = [NSRegularExpression regularExpressionWithPattern:ITEM_3_PATTERN options:NSRegularExpressionAnchorsMatchLines error:&error];
    content = [regex stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, content.length) withTemplate:@"<span class=\"item3\">$1</span>"];

    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
    HighlightColor *highlightColor = [HighlightColorList getCurrentHighlightColor];
    
    NSString *color1 = highlightColor ? highlightColor.inlineColors[0] : DEFAULT_HIGHLIGHT_COLOR_1;
    NSString *color2 = highlightColor ? highlightColor.inlineColors[1] : DEFAULT_HIGHLIGHT_COLOR_2;
    NSString *color3 = highlightColor ? highlightColor.inlineColors[2] : DEFAULT_HIGHLIGHT_COLOR_3;
    NSString *color4 = highlightColor ? highlightColor.inlineColors[3] : DEFAULT_HIGHLIGHT_COLOR_4;
    NSString *color5 = highlightColor ? highlightColor.inlineColors[4] : DEFAULT_HIGHLIGHT_COLOR_5;
    
    NSString *color6 = highlightColor ? highlightColor.footerColors[0] : DEFAULT_HIGHLIGHT_COLOR_6;
    NSString *color7 = highlightColor ? highlightColor.footerColors[1] : DEFAULT_HIGHLIGHT_COLOR_7;
    NSString *color8 = highlightColor ? highlightColor.footerColors[2] : DEFAULT_HIGHLIGHT_COLOR_8;
    NSString *color9 = highlightColor ? highlightColor.footerColors[3] : DEFAULT_HIGHLIGHT_COLOR_9;
    NSString *color10 = highlightColor ? highlightColor.footerColors[4] : DEFAULT_HIGHLIGHT_COLOR_10;
    
    NSInteger footerFontSize = self.fontSize * FOOTER_FONT_SIZE_SCALE;
    NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"page" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
    NSString *html = [NSString stringWithFormat:template,
                      self.code == LanguageCodeRomanScript ? @"Helvetica" : @"THSarabunNew",
                      self.fontSize,
                      self.foregroundColor,
                      self.backgroundColor,
                      footerFontSize,
                      color1, color1, color1, color1,
                      color2, color2, color2, color2,
                      color3, color3, color3, color3,
                      color4, color4, color4, color4,
                      color5, color5, color5, color5,
                      color6, color6, color6, color6,
                      color7, color7, color7, color7,
                      color8, color8, color8, color8,
                      color9, color9, color9, color9,
                      color10, color10, color10, color10,
                      content, @""];
    html = [html stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"]; // escape single quote
    return html;
}

- (NSString *)addNotes:(NSString *)content
{
    NSString *text = @"";
    NSArray *highlights = [UserDatabaseHelper queryHighlightsInVolume:self.volume page:self.page code:self.code];
    
    if (highlights.count && !self.htmlContent) {
        text = [text stringByAppendingString:[content substringToIndex:((Highlight *)highlights[0]).range.location]];
    } else {
        text = content;
    }
    
    NSInteger totalLength = text.length;
    
    NSInteger footnoteCount = 0;
    NSMutableArray *footnotes = [[NSMutableArray alloc] init];
    for (int i=0; i<highlights.count;++i) {
        Highlight *highlight = highlights[i];
        Highlight *next = i < highlights.count-1 ? (Highlight *)highlights[i+1] : nil;
        
        NSString *selection = @"";
        if (highlight.range.location >= totalLength) {
            selection = [content substringWithRange:highlight.range];
        } else if (highlight.range.location + highlight.range.length > totalLength) {
            selection = [content substringWithRange:NSMakeRange(totalLength, highlight.range.location+highlight.range.length-totalLength)];
        }
        
        totalLength += selection.length;
        
        NSString *note = @"";
        if ([self.delegate pageViewControllerShouldShowHighlightDetail:self] && highlight.type == HighlightTypeDescription && highlight.note.length) {
            NSString *htmlNote = [UserDatabaseHelper createHtmlHighlightNotes:highlight withBlock:^NSString *(NSString *selection, NSInteger rowId) {
                return [NSString stringWithFormat:@"<span class=\"highlightnote\">%@</span>", selection];
            }];
            note = [NSString stringWithFormat:@"<a href=\"js-call://footnote_click/%@\" style=\"text-decoration:none;display:inline;\"><sub>[%@]</sub></a> ", [NSNumber numberWithInteger:highlight.rowId], htmlNote];
        } else if ([self.delegate pageViewControllerShouldShowHighlightDetail:self] && highlight.type == HighlightTypeFootnote && highlight.note.length) {
            footnoteCount += 1;
            note = [NSString stringWithFormat:@"<sup>(%@)</sup> ", [NSNumber numberWithInteger:footnoteCount]];
            [footnotes addObject:highlight];
        }
        
        // skip highlighting html format content (see addHighlightsForHtmlContent)
        if (!self.htmlContent) {
            NSString *prefix = highlight.note.length > 0 ? @"note-" : @"";
            text = [text stringByAppendingString:[NSString stringWithFormat:@"<a href=\"js-call://highlight_click/%@\" style=\"text-decoration:none;display:inline;\"><span id=\"hi%@\" class=\"%@highlight%d-%ld\">%@</span></a>%@",[NSNumber numberWithInteger:highlight.rowId], [NSNumber numberWithInteger:highlight.rowId], prefix, highlight.type, highlight.color, selection, note]];
            if (next) { // add content to the next highlight
                if (next.range.location > totalLength) {
                    NSInteger location = totalLength;
                    NSInteger length = next.range.location-totalLength;
                    text = [text stringByAppendingString:[content substringWithRange:NSMakeRange(location, length)]];
                    totalLength += length;
                }
            } else { // add the rest of content
                text = [text stringByAppendingString:[content substringFromIndex:highlight.range.location+highlight.range.length]];
            }
        }
        
    }
    
    NSString *footer = @"<p>";
    for (int i=0; i<footnotes.count; ++i) {
        Highlight *highlight = footnotes[i];
        NSString *note = [UserDatabaseHelper createHtmlHighlightNotes:highlight withBlock:^NSString *(NSString *selection, NSInteger rowId) {
            return [NSString stringWithFormat:@"<span class=\"highlightnote\">%@</span>", selection];
        }];
        footer = [footer stringByAppendingString:[NSString stringWithFormat:@"<div class=\"footer\">(%d) <a href=\"js-call://footnote_click/%@\" style=\"text-decoration:none;display:inline;\"><span>%@</span></a></div>", i+1, [NSNumber numberWithInteger:highlight.rowId], note]];
    }
    footer = [footer stringByAppendingString:@"</p>"];
    
    if (footnotes.count) {
        text = [text stringByAppendingString:footer];
    }
    NSString *note = [UserDatabaseHelper getBookmarkByVolume:self.volume page:self.page withCode:self.code].note;
    if ([self.delegate pageViewControllerShouldShowHighlightDetail:self] && note.length) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"<hr/><p id=\"extra\" class=\"note\"><sup>%@</sup>%@</p>", NSLocalizedString(@"Additional text", nil), note]];
    }
    
    return text;
}

- (void)addHighlightsForHtmlContent:(UIWebView *)webView
{
    if (!self.htmlContent) { return; }
    
    NSArray *highlights = [UserDatabaseHelper queryHighlightsInVolume:self.volume page:self.page code:self.code];
    
    for (Highlight *highlight in highlights) {
        NSString *selection =  highlight.selection;
        selection = [selection stringByReplacingOccurrencesOfString:@" \n" withString:@"\\\\s+"];
        selection = [selection stringByReplacingOccurrencesOfString:@"\t" withString:@"\\\\s+"];
        selection = [selection stringByReplacingOccurrencesOfString:@"[" withString:@"\\\\["];
        selection = [selection stringByReplacingOccurrencesOfString:@"]" withString:@"\\\\]"];
        selection = [selection stringByReplacingOccurrencesOfString:@"." withString:@"[.]"];
        selection = [selection stringByReplacingOccurrencesOfString:@"*" withString:@"[*]"];
        selection = [selection stringByReplacingOccurrencesOfString:@"(" withString:@"[(]"];
        selection = [selection stringByReplacingOccurrencesOfString:@")" withString:@"[)]"];
        selection = [selection stringByReplacingOccurrencesOfString:@" " withString:@"\\\\s"];
        selection = [selection stringByReplacingOccurrencesOfString:@"\n" withString:@"\\\\s"];
        selection = [selection stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
        
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"addHighlight('%@', %@, %@, %d, %ld, %@);", selection, [NSNumber numberWithInteger:highlight.position], [NSNumber numberWithInteger:highlight.rowId], highlight.type, highlight.color, [NSNumber numberWithInteger:highlight.note.length]]];
    }
    
    if (![self.delegate pageViewControllerShouldShowHighlightDetail:self]) {
        [webView stringByEvaluatingJavaScriptFromString:@"removeHighlightDecorations();"];
        return;
    }
    
    NSInteger footnoteCount = 0;
    for (Highlight *highlight in highlights) {
        if (highlight.type == HighlightTypeDescription && highlight.note.length) {
            NSString *htmlNote = [UserDatabaseHelper createHtmlHighlightNotes:highlight withBlock:^NSString *(NSString *selection, NSInteger rowId) {
                return [NSString stringWithFormat:@"<span class='highlightnote'>%@</span>", selection];
            }];
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"decorateHighlight(%@, \"%@\");", [NSNumber numberWithInteger:highlight.rowId], [NSString stringWithFormat:@"<a href='js-call://footnote_click/%@' style='text-decoration:none;display:inline;'><sub class='decoration'>[%@]</sub></a>", [NSNumber numberWithInteger:highlight.rowId], htmlNote]]];
        } else if (highlight.type == HighlightTypeFootnote && highlight.note.length) {
            footnoteCount += 1;
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"decorateHighlight(%@, \"%@\");", [NSNumber numberWithInteger:highlight.rowId], [NSString stringWithFormat:@"<sup class='decoration'>(%@)</sup>", [NSNumber numberWithInteger:footnoteCount]]]];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] absoluteString] hasPrefix:@"js-call://highlight_click"]) {
        [self.delegate pageViewController:self didClickAtHighlight:[request.URL.pathComponents.lastObject integerValue]];
        return NO;
    } else if ([[[request URL] absoluteString] hasPrefix:@"js-call://footnote_click"]) {
        [self.delegate pageViewController:self didClickAtHighlightNote:[request.URL.pathComponents.lastObject integerValue]];
        NSLog(@"%@", request.URL.pathComponents.lastObject);
        return NO;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [webView.window makeKeyAndVisible];
    [webView becomeFirstResponder];
    [self.spinner stopAnimating];
    
    if (!_loadedJSFiles) {
        [webView loadJavascriptFileFromResourceBundle:@"rangy-core.js"];
        [webView loadJavascriptFileFromResourceBundle:@"rangy-textrange.js"];
        [webView loadJavascriptFileFromResourceBundle:@"rangy-cssclassapplier.js"];
        [webView loadJavascriptFileFromResourceBundle:@"rangy-serializer.js"];
        [webView loadJavascriptFileFromResourceBundle:@"jquery-2.1.1.min.js"];
        [webView stringByEvaluatingJavaScriptFromString:@"rangy.init();"];
        _loadedJSFiles = YES;
    }
    
    [self highlight:webView withKeywords:self.keywords];
    
    [self addHighlightsForHtmlContent:webView];
    
    if (self.anchoredItem > 0) {
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"scrollToItem('i%@');", [NSNumber numberWithInteger:self.anchoredItem]]];
        self.anchoredItem = 0;
    }
    
    if (self.shouldScrollToKeywords) {
        [webView stringByEvaluatingJavaScriptFromString:@"scrollToKeywords();"];
        self.shouldScrollToKeywords = NO;
    }
}

@end
