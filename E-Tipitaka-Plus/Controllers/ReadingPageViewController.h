//
//  ReadingPageViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Models.h"
#import "PageInfo.h"
#import "PageViewController.h"

@class ReadingPageViewController;

@protocol ReadingPageViewControllerDelegate <NSObject>

@required
- (NSInteger)readingPageViewControllerCurrentPage:(ReadingPageViewController *)controller;
- (NSInteger)readingPageViewControllerCurrentNumber:(ReadingPageViewController *)controller;

@optional
- (void)readingPageViewController:(ReadingPageViewController *)controller changeVolume:(NSInteger)volume pages:(NSInteger)pages code:(LanguageCode)code;
- (void)readingPageViewController:(ReadingPageViewController *)controller changePage:(NSInteger)page items:(NSArray *)items;
- (void)readingPageViewController:(ReadingPageViewController *)controller isAnimating:(BOOL)animating;
- (void)readingPageViewControllerPopupBookmark:(ReadingPageViewController *)controller;
- (void)readingPageViewControllerJumpToHighlight:(ReadingPageViewController *)controller;
- (void)readingPageViewControllerRequestComparing;

@end

@interface ReadingPageViewController : UIPageViewController

@property (nonatomic, weak) id<ReadingPageViewControllerDelegate, PageViewControllerDelegate> readingPageViewDelegate;
@property (nonatomic, readonly) LanguageCode code;
@property (nonatomic, readonly) ETDataModel *dataModel;
@property (nonatomic, assign) BOOL pageIsAnimating;
@property (nonatomic, readonly) UIWebView *webView;
@property (nonatomic, readonly) PageViewController *pageViewController;
@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, assign) SearchType searchType;

- (void)jumpToIndex:(NSInteger)index;
- (void)jumpToIndex:(NSInteger)index withItem:(NSInteger)item;
- (void)start;
- (void)open:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page;
- (void)open:(PageInfo *)pageInfo;
- (void)open:(PageInfo *)pageInfo keywords:(NSString *)keywords;
- (NSString *)getTextSelection;

@end
