//
//  ComparingContainerViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 28/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ComparingViewController.h"
#import "ReadingPageViewController.h"
#import "PageInfo.h"
#import <KNSemiModalViewController/UIViewController+KNSemiModal.h>

@interface ComparingViewController ()

@end

@implementation ComparingViewController

@synthesize side = _side;
@synthesize delegate = _delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.headerLabel.font = [UIFont systemFontOfSize:14];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        UILabel *nav_titlelbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.navigationItem.titleView.frame.size.width, 40)];
        nav_titlelbl.text = self.title;
        nav_titlelbl.textAlignment = NSTextAlignmentCenter;
        UIFont *lblfont = [UIFont boldSystemFontOfSize:12];
        [nav_titlelbl setFont:lblfont];
        self.navigationItem.titleView = nav_titlelbl;
    }
}
    
- (void)changeCode:(NSNotification *)notification
{
}

- (void)setupLeftMenuButton
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
}

- (void)setupRightMenuButton
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(compare:)];
}

- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        PageInfo *pageInfo = [[PageInfo alloc] initWithCode:self.code volume:self.volume page:self.page-([self.dataModel startPageOfVolume:self.volume]-1)];
        pageInfo.keywords = self.readingPageViewController.pageViewController.keywords;
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeDatabasAndVolumeAndPageNotification object:pageInfo];
    }];
}

- (IBAction)compare:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(comparingViewController:shouldCompare:)]) {
        [self.delegate comparingViewController:self shouldCompare:self.side];
    }
}

@end
