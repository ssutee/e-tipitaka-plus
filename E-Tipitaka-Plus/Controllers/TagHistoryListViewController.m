//
//  TagHistoryListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "TagHistoryListViewController.h"
#import "UITableView+LongPressMove.h"
#import "UserDatabaseHelper.h"

@interface TagHistoryListViewController()<LongPressMoveDelegate>

@end

@implementation TagHistoryListViewController

@synthesize tag = _tag;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = self.tag.name;
    self.code = self.tag.code;
    self.tableView.longPressMoveDelegate = self;
    [self.tableView enableLongPressMove:1];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self refresh];
}

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target
{
    [self.tag.history exchangeObjectAtIndex:target.row withObjectAtIndex:source.row];
    [UserDatabaseHelper updateTagItems:@"history" items:self.tag.history ofTag:self.tag.name withCode:self.tag.code];
}

- (void)refresh
{
    self.histories = [[UserDatabaseHelper queryHistoriesInRowIds:self.tag.history withCode:self.tag.code] mutableCopy];
    [self.tableView reloadData];
}

- (void)deleteSelectedHistory
{
    History *history = [self.histories objectAtIndex:self.selectedIndexPath.row];
    [self.histories removeObjectAtIndex:self.selectedIndexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tag.history removeObject:[NSNumber numberWithInteger:history.rowId]];
    [UserDatabaseHelper updateTagItems:@"history" items:self.tag.history ofTag:self.tag.name withCode:self.tag.code];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"History", nil);
}

- (NSString *)confirmDeleteMessage
{
    return NSLocalizedString(@"Do you want to remove the item from this tag?", nil);
}

@end
