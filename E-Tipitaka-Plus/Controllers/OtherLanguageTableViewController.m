//
//  OtherLanguageTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherLanguageTableViewController.h"

@interface OtherLanguageTableViewController ()

@end

@implementation OtherLanguageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];    
    self.clearsSelectionOnViewWillAppear = NO;
    self.title = NSLocalizedString(@"Choose book", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        self.selectedCode = LanguageCodeThaiSiam;
    } else if (indexPath.row == 1) {
        self.selectedCode = LanguageCodePaliSiam;
    } else if (indexPath.row == 2) {
        self.selectedCode = LanguageCodeThaiWatna;
    } else if (indexPath.row == 3) {
        self.selectedCode = LanguageCodeThaiMahaMakut;
    } else if (indexPath.row == 4) {
        self.selectedCode = LanguageCodePaliMahaChula;
    } else if (indexPath.row == 5) {
        self.selectedCode = LanguageCodeThaiPocketBook;
    } else if (indexPath.row == 6) {
        self.selectedCode = LanguageCodeThaiFiveBooks;
    } else if (indexPath.row == 7) {
        self.selectedCode = LanguageCodeRomanScript;
    }
    return indexPath;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Book Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"Thai Royal Menu", nil);
    } else if (indexPath.section == 0 && indexPath.row == 1) {
        cell.textLabel.text = NSLocalizedString(@"Pali Siam Menu", nil);
    } else if (indexPath.section == 0 && indexPath.row == 2) {
        cell.textLabel.text = NSLocalizedString(@"Buddhawajana Tipitaka", nil);
    } else if (indexPath.section == 0 && indexPath.row == 3) {
        cell.textLabel.text = NSLocalizedString(@"Thai Maha Makut Menu", nil);
    } else if (indexPath.section == 0 && indexPath.row == 4) {
        cell.textLabel.text = NSLocalizedString(@"Thai Maha Chula Menu", nil);
    } else if (indexPath.section == 0 && indexPath.row == 5) {
        cell.textLabel.text = NSLocalizedString(@"Buddhawajana Pocket Book", nil);
    } else if (indexPath.section == 0 && indexPath.row == 6) {
        cell.textLabel.text = NSLocalizedString(@"Five books from Buddha speech", nil);
    } else if (indexPath.section == 0 && indexPath.row == 7) {
        cell.textLabel.text = NSLocalizedString(@"Roman Script", nil);
    }
    
    return cell;
}

@end
