//
//  BaseHighlightListViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "BaseHighlightListViewController.h"
#import "NoteViewCell.h"
#import "Highlight.h"
#import "NSString+Thai.h"
#import "UserDatabaseHelper.h"

@implementation BaseHighlightListViewController

@synthesize items = _items;


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchBar resignFirstResponder];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    Highlight *highlight = self.items[indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:ChangeVolumeAndPageNotification object:@{@"volume":[NSNumber numberWithInt:highlight.volume], @"page":[NSNumber numberWithInt:highlight.page - [self.dataModel startPageOfVolume:highlight.volume] + 1], @"highlight":highlight}];
}


#pragma mark - UITableViewDataSource

- (NoteViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Highlight Cell";
    NoteViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[NoteViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Highlight *highlight = self.items[indexPath.row];
    cell.noteLabel.text = [NSString stringWithFormat:@"%@ - %@", [highlight.selection stringByReplacingOccurrencesOfString:@"\n" withString:@" "], highlight.note];
    cell.volumeLabel.text = [[NSString stringWithFormat:HEADER_TEMPLATE_3, highlight.volume, highlight.page] stringByReplacingThaiNumeric];
    cell.infoLabel.text = [self.dataModel volumeTitle:highlight.volume];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:highlight.type == HighlightTypeDescription ? @"highlight1" : @"highlight2"]];
    
    NSArray *tagNames = [UserDatabaseHelper queryTagNamesFrom:@"highlight" byRowId:highlight.rowId withCode:highlight.code];
    if (tagNames.count > 0) {
        cell.tagLabel.text = [NSString stringWithFormat:@"[%@]", [tagNames componentsJoinedByString:@"|"]];
    } else {
        cell.tagLabel.text = @"";
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


@end
