//
//  WriteNoteViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 20/8/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "WriteNoteViewController.h"
#import "UserDatabaseHelper.h"
#import "WriteNote.h"

@interface WriteNoteViewController ()

@end

@implementation WriteNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSString *)note {
    
    WriteNote *writeNote = self.main ? [UserDatabaseHelper getMainWriteNote] : [UserDatabaseHelper getWriteNote:self.rowId];
    if (writeNote && self.rowId < 1) {
        self.rowId = writeNote.rowId;
    }
    return writeNote ? writeNote.note : @"";
}

- (NSString *)timestamp {
    WriteNote *writeNote = self.main ? [UserDatabaseHelper getMainWriteNote] : [UserDatabaseHelper getWriteNote:self.rowId];
    if (writeNote && self.rowId < 1) {
        self.rowId = writeNote.rowId;
    }
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setLocalizedDateFormatFromTemplate:@"dd/MM/yyyy HH:mm:ss"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"th"]];
    [dateFormatter stringFromDate:writeNote.created];
    return writeNote ? [dateFormatter stringFromDate:writeNote.created] : @"";
}

- (BOOL)starred {
    
    WriteNote *writeNote = self.main ? [UserDatabaseHelper getMainWriteNote] : [UserDatabaseHelper getWriteNote:self.rowId];
    if (writeNote && self.rowId < 1) {
        self.rowId = writeNote.rowId;
    }
    return writeNote ? writeNote.starred : NO;
}

- (void)save:(NSString *)text starred:(BOOL)starred
{
    if (self.rowId > 0) {
        [UserDatabaseHelper updateWirteNote:text starred:starred rowId:self.rowId];
    } else {
        self.rowId = [UserDatabaseHelper saveWriteNote:text starred:starred created:[NSDate date] main:self.main];
    }
}

- (void)markStar:(BOOL)value {
    if (self.rowId > 0) {
        [UserDatabaseHelper updateWirteNote:value rowId:self.rowId];
    }
}

@end
