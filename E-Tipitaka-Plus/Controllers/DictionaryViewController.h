//
//  DictionaryViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/1/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DictionaryViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UIToolbar *romanToolbar1;
@property (nonatomic, weak) IBOutlet UIToolbar *romanToolbar2;

@property (nonatomic, strong) NSString *searchText;
@property (nonatomic, assign) DictionaryType dictionaryType;

@end
