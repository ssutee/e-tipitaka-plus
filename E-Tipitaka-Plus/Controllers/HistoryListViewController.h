//
//  HistoryListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
#import "ETViewController.h"
#import "BaseHistoryListViewController.h"

@interface HistoryListViewController : BaseHistoryListViewController<UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingOrderSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingTypeSegmentedControl;

@end
