//
//  TagHighlightViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "BaseHighlightListViewController.h"
#import "Tag.h"

@interface TagHighlightViewController : BaseHighlightListViewController

@property (nonatomic, strong) Tag *tag;

@end
