//
//  BaseHistoryListViewController.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HHPanningTableViewCell/HHPanningTableViewCell.h>

#import "ETViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "History.h"

@interface BaseHistoryListViewController : ETViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISwitch *showAllSwitch;

@property (nonatomic, strong) HHPanningTableViewCell *activeCell;
@property (nonatomic, strong) History *activeHistory;
@property (nonatomic, strong) History *selectedHistory;
@property (nonatomic, strong) NSMutableArray *histories;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;


- (UIView *)drawerView:(CGRect)frame;
- (NSString *)confirmDeleteMessage;
- (void)refresh;
- (void)setActiveItem:(NSIndexPath *)indexPath;
- (void)updateState:(NSInteger)state atRowId:(NSInteger)rowId;
- (NSString *)activeKeywords;
- (NSInteger)activeRowId;
- (NSInteger)itemsCount;
- (NSInteger)stateAtIndexPath:(NSIndexPath *)indexPath;
- (LanguageCode)codeAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)keywordsAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)noteAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)detailAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)searchTypeAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)noteStateAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)rowIdAtIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)queryTagNames:(NSInteger)rowId code:(LanguageCode)code;
- (void)deleteSelectedHistory;

@end
