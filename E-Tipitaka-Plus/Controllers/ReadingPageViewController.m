//
//  ReadingPageViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 1/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>

#import "ReadingPageViewController.h"
#import "ReadingPageViewModel.h"
#import "PageViewController.h"
#import "Highlight.h"
#import "UserDatabaseHelper.h"
#import "UIViewController+MMDrawerController.h"

@interface ReadingPageViewController ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (nonatomic, strong) NSMutableDictionary *pages;
@property (nonatomic, strong) ReadingPageViewModel *viewModel;

@end

@implementation ReadingPageViewController

@synthesize pages = _pages;
@synthesize code = _code;
@synthesize dataModel = _dataModel;
@synthesize keywords = _keywords;
@synthesize searchType = _searchType;

- (id)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.viewModel = [ReadingPageViewModel new];
        [self initialize];
    }
    return self;
}

// should be used for testing
- (id)initWithViewModel:(ReadingPageViewModel *)viewModel
{
    self = [super init];
    if (self) {
        self.viewModel = viewModel;
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    [self registerNotificationObservers];
}

- (LanguageCode)code
{
    return self.viewModel.code;
}

- (ETDataModel *)dataModel
{
    return self.viewModel.dataModel;
}

- (void)setKeywords:(NSString *)keywords
{
    self.viewModel.keywords = keywords;
}

- (void)setSearchType:(SearchType)searchType
{
    self.viewModel.searchType = searchType;
}

- (UIWebView *)webView
{
    return self.viewControllers.count ? ((PageViewController *)self.viewControllers[0]).webView : nil;
}

- (PageViewController *)pageViewController
{
    return self.viewControllers.count ? self.viewControllers[0] : nil;
}

- (void)bindViewModel
{
    [self.viewModel.resultsChangeSignal subscribeNext:^(id x) {
        if (x) {
            [self.pages removeAllObjects];
            [self.pageViewController update];
        }
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataSource = self;
    self.delegate = self;
    self.pages = [[NSMutableDictionary alloc] init];
    
    for (UIGestureRecognizer *recognizer in self.gestureRecognizers) {
        if ([recognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            recognizer.enabled = NO;
        }
    }
    [self bindViewModel];
}

- (void)registerNotificationObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(start) name:DatabaseCompleteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeDatabase:) name:ChangeDatabaseNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeVolume:) name:ChangeVolumeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeVolumeAndPage:) name:ChangeVolumeAndPageNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeVolumeAndPage:) name:ChangeVolumeAndPageThenCompareNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeDatabaseAndVolumeAndPage:) name:ChangeDatabasAndVolumeAndPageNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update:) name:UpdateCurrentPageNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)saveVolume:(NSInteger)volume page:(NSInteger)page code:(LanguageCode)code
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@{@"page":[NSNumber numberWithInteger:page], @"volume":[NSNumber numberWithInteger:volume], @"code":[NSNumber numberWithInt:code]} forKey:@"LastPage"];
    [userDefaults synchronize];
}

- (void)savePagePosition
{
    NSInteger page = [self.readingPageViewDelegate readingPageViewControllerCurrentPage:self];
    [self saveVolume:self.viewModel.volume page:page code:self.viewModel.code];
}

- (void)update:(NSNotification *)notification
{
    [self.pageViewController update];
}

- (void)changeDatabaseAndVolumeAndPage:(NSNotification *)notification
{
    PageInfo *pageInfo = (PageInfo *)notification.object;
    [self open:pageInfo keywords:pageInfo.keywords];
}

- (void)changeDatabase:(NSNotification*)notification
{
    if (self.dataModel.code == [notification.object integerValue]) {
        return;
    }
    
    NSArray *result1 = [self.dataModel convertToPivot:self.viewModel.volume page:1 item:1];
    NSNumber *rVolume = result1[0];
    NSNumber *rItem = result1[1];
    NSNumber *rSubItem = result1[2];
    
    NSArray *result2 = [[SharedDataModel sharedDataModel:[notification.object intValue]]
                        convertFromPivot:rVolume.integerValue
                        item:(rItem.integerValue == 0 ? 1 : rItem.integerValue)
                        subItem:rSubItem.integerValue];
    
    NSInteger volume = [result2[0] integerValue];
    
    if ([notification.object integerValue] == LanguageCodeThaiWatna) {
        volume = 11;
    } else if (!self.dataModel.isTipitaka || [notification.object integerValue] == LanguageCodeThaiFiveBooks || [notification.object integerValue] == LanguageCodeThaiPocketBook || [notification.object integerValue] == LanguageCodeThaiVinaya) {
        volume = 1;
    }
    
    [self open:[notification.object intValue] volume:(volume==0 ? 1 : volume) page:1];
    [[NSNotificationCenter defaultCenter] postNotificationName:ChangeSelectedVolumeNotification object:[NSNumber numberWithInteger:volume]];
}

- (void)changeVolumeAndPage:(NSNotification *)notification
{
    NSDictionary *result = notification.object;
    
    NSInteger volume = [result[@"volume"] integerValue];
    NSInteger page = [result[@"page"] integerValue];
    NSInteger number = 0;
    
    if ([result objectForKey:@"number"]) {
        number = [[result objectForKey:@"number"] integerValue];
    }
    
    if ([self.readingPageViewDelegate readingPageViewControllerCurrentNumber:self] != number) {
        return;
    }
    
    self.viewModel.searchType = result[@"type"] ? [result[@"type"] intValue] : SearchTypeAll;
    
    [self open:self.viewModel.code volume:volume page:page keywords:[result objectForKey:@"keywords"]];
    
    if ([self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewControllerJumpToHighlight:)] && result[@"highlight"]) {
        [self.readingPageViewDelegate readingPageViewControllerJumpToHighlight:self];
    }
    
    if ([self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewControllerPopupBookmark:)] && result[@"bookmark"]) {
        double delayInSeconds = 0.25;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self.readingPageViewDelegate readingPageViewControllerPopupBookmark:self];
        });
    } else if (result[@"highlight"]) {
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            Highlight *highlight = result[@"highlight"];
            [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"scrollToItem('hi%d');", highlight.rowId]];
        });
    }
    
    if ([result[@"compare"] boolValue] && [self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewControllerRequestComparing)]) {
        [self.readingPageViewDelegate readingPageViewControllerRequestComparing];
    }
}

- (void)changeVolume:(NSNotification *)notification
{
    NSInteger volume = [notification.object integerValue];
    if (volume != self.viewModel.volume) {
        [self open:self.viewModel.code volume:volume page:1];
    }
}

- (void)open:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page keywords:(NSString *)keywords
{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Loading..."];
    NSLog(@"open volume=%d page=%d", volume, page);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ETDataModel *dataModel = [SharedDataModel sharedDataModel:code];
        self.viewModel.searchType = dataModel.isBuddhawajana ? SearchTypeBuddhawaj : SearchTypeAll;
        [self.viewModel open:code volume:volume page:page keywords:keywords];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewController:changeVolume:pages:code:)]) {
                [self.readingPageViewDelegate readingPageViewController:self changeVolume:volume pages:[self resultsCount] code:self.viewModel.code];
            }
            
            [self jumpToIndex:page-1 withItem:0 shouldScrollToKeywords:keywords!=nil];
            if ([self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewController:changePage:items:)]) {
                [self.readingPageViewDelegate readingPageViewController:self changePage:page items:[self.dataModel queryItemsInVolume:self.viewModel.volume page:page]];
            }
            [SVProgressHUD dismiss];
        });
    });
}

- (void)open:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page
{
    [self open:code volume:volume page:page keywords:nil];
}

- (void)open:(PageInfo *)pageInfo keywords:(NSString *)keywords
{
    [self open:pageInfo.code volume:pageInfo.volume page:pageInfo.page keywords:keywords];
}

- (void)open:(PageInfo *)pageInfo
{
    [self open:pageInfo.code volume:pageInfo.volume page:pageInfo.page];
}

- (void)start
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *lastPage = [userDefaults objectForKey:@"LastPage"];
    NSInteger volume = [lastPage[@"volume"] integerValue];
    NSInteger page = [lastPage[@"page"] integerValue];
    if (lastPage && [lastPage[@"code"] intValue] > 0 && [lastPage[@"page"] integerValue] > 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:StartDatabaseNotification object:[NSNumber numberWithInt:[lastPage[@"code"] intValue]]];
        [[NSNotificationCenter defaultCenter] postNotificationName:ChangeSelectedVolumeNotification object:lastPage[@"volume"]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self open:[lastPage[@"code"] intValue] volume:volume page:page-[self.dataModel startPageOfVolume:volume]+1];
        });
    } else {
        [self open:LanguageCodeThaiSiam volume:self.viewModel.volume page:1];
    }
}

- (NSInteger)resultsCount
{
    return self.viewModel.results.count;
}

- (PageResult *)pageResultAtIndex:(NSInteger)index
{
    return self.viewModel.results[index];
}

- (void)jumpToIndex:(NSInteger)index withItem:(NSInteger)item shouldScrollToKeywords:(BOOL)shouldScroll
{
    if (index < 0 || index >= self.viewModel.results.count) {
        PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
        controller.content = NSLocalizedString(@"Database is not ready", nil);
        controller.delegate = self.readingPageViewDelegate;
        [self setViewControllers:@[controller] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        return;
    }
    
    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    PageResult *pageResult = [self pageResultAtIndex:index];
    controller.content = pageResult.content;
    controller.htmlContent = pageResult.htmlContent;
    controller.footer = pageResult.footer;
    controller.pageIndex = index;
    controller.keywords = self.viewModel.keywords;
    controller.searchType = self.viewModel.searchType;
    controller.items = pageResult.items;
    controller.anchoredItem = item;
    controller.shouldScrollToKeywords = shouldScroll;
    controller.volume = pageResult.volume.integerValue;
    controller.page = pageResult.page.integerValue;
    controller.code = pageResult.code;
    controller.delegate = self.readingPageViewDelegate;
    
    ReadingPageViewController *weakSelf = self;
    
    [self setViewControllers:@[controller] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
        if ([weakSelf.readingPageViewDelegate respondsToSelector:@selector(readingPageViewController:changePage:items:)]) {
            [weakSelf.readingPageViewDelegate readingPageViewController:weakSelf changePage:index+1 items:[weakSelf.dataModel queryItemsInVolume:weakSelf.viewModel.volume page:index+1]];
        }
    }];

    [self saveVolume:pageResult.volume.integerValue page:pageResult.page.integerValue code:pageResult.code];
}

- (void)jumpToIndex:(NSInteger)index
{
    [self jumpToIndex:index withItem:0];
}

- (void)jumpToIndex:(NSInteger)index withItem:(NSInteger)item
{
    [self jumpToIndex:index withItem:item shouldScrollToKeywords:NO];
}

- (void)cleanUnusedPages:(NSInteger)index
{
    for (int i=0; i<index-(MAXIMUM_PAGE_SIZE/2)-1; ++i) {
        [self.pages removeObjectForKey:[NSNumber numberWithInt:i]];
    }
    
    for (int i=index+(MAXIMUM_PAGE_SIZE/2)+1; i<[self resultsCount]; ++i) {
        [self.pages removeObjectForKey:[NSNumber numberWithInt:i]];
    }
}

- (NSString *)getTextSelection
{
    PageViewController *controller = self.viewControllers[0];
    return [[controller.webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    if (self.viewModel.results.count == 0) {
        return nil;
    }
    
    PageViewController *controller = (PageViewController *)viewController;

    if (self.pageIsAnimating || controller.pageIndex == [self resultsCount] - 1) { return nil; }
 
    [self cleanUnusedPages:controller.pageIndex];
    
    PageViewController *nextController = [self.pages objectForKey:[NSNumber numberWithInteger:controller.pageIndex+1]];
    
    if (!nextController) {
        nextController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
        PageResult *pageResult = [self pageResultAtIndex:controller.pageIndex + 1];
        nextController.content = pageResult.content;
        nextController.htmlContent = pageResult.htmlContent;
        nextController.footer = pageResult.footer;
        nextController.volume = pageResult.volume.integerValue;
        nextController.page = pageResult.page.integerValue;
        nextController.code = pageResult.code;
        nextController.pageIndex = controller.pageIndex + 1;
        nextController.keywords = self.viewModel.keywords;
        nextController.searchType = self.viewModel.searchType;
        nextController.items = pageResult.items;
        nextController.delegate = self.readingPageViewDelegate;
        [self.pages setObject:nextController forKey:[NSNumber numberWithInteger:controller.pageIndex + 1]];
    }
    
    return nextController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if (self.viewModel.results.count == 0) {
        return nil;
    }
    
    PageViewController *controller = (PageViewController *)viewController;
    
    if (self.pageIsAnimating || controller.pageIndex == 0) { return nil; }
    
    [self cleanUnusedPages:controller.pageIndex];
    
    PageViewController *prevController = [self.pages objectForKey:[NSNumber numberWithInteger:controller.pageIndex-1]];
    
    if (!prevController) {
        prevController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
        PageResult *pageResult = [self pageResultAtIndex:controller.pageIndex - 1];
        prevController.content = pageResult.content;
        prevController.htmlContent = pageResult.htmlContent;
        prevController.footer = pageResult.footer;
        prevController.volume = pageResult.volume.integerValue;
        prevController.page = pageResult.page.integerValue;
        prevController.code = pageResult.code;
        prevController.pageIndex = controller.pageIndex - 1;
        prevController.keywords = self.viewModel.keywords;
        prevController.searchType = self.viewModel.searchType;
        prevController.items = pageResult.items;
        prevController.delegate = self.readingPageViewDelegate;
        [self.pages setObject:prevController forKey:[NSNumber numberWithInteger:controller.pageIndex - 1]];
    }
    
    return prevController;
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    self.pageIsAnimating = YES;
    if ([self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewController:isAnimating:)]) {
        [self.readingPageViewDelegate readingPageViewController:self isAnimating:self.pageIsAnimating];
    }
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!((self.pageIsAnimating = !(completed || finished)) || pageViewController.viewControllers.count == 0)) {
        PageViewController *controller = pageViewController.viewControllers[0];
        if ([self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewController:changePage:items:)]) {
            [self.readingPageViewDelegate readingPageViewController:self changePage:controller.pageIndex+1 items:[self.dataModel queryItemsInVolume:self.viewModel.volume page:controller.pageIndex+1]];
        }
    }
    if ([self.readingPageViewDelegate respondsToSelector:@selector(readingPageViewController:isAnimating:)]) {
        [self.readingPageViewDelegate readingPageViewController:self isAnimating:self.pageIsAnimating];
    }
    [self savePagePosition];
}

@end
