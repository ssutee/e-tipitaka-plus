//
//  OthersDataTableViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OthersDataTableViewController.h"
#import "OthersDataDetailTableViewController.h"
#import "AccountHelper.h"
#import "UserDatabaseHelper.h"
#import "OtherUser.h"

#import <AFNetworking/AFNetworkReachabilityManager.h>
#import <AFNetworking/AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface OthersDataTableViewController ()

@property (nonatomic, strong) NSMutableArray *userList;
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong) NSMutableArray *tasks;

@end


@implementation OthersDataTableViewController

@synthesize userList = _userList;
@synthesize sessionManager = _sessionManager;
@synthesize tasks = _tasks;

- (NSMutableArray *)tasks
{
    if (_tasks) {
        _tasks = [NSMutableArray new];
    }
    return _tasks;
}

- (NSMutableArray *)userList
{
    if (!_userList) {
        _userList = [NSMutableArray new];
    }
    return _userList;
}

- (AFHTTPSessionManager *)sessionManager
{
    if (!_sessionManager) {
        NSURL *baseURL = [NSURL URLWithString:USER_DATA_HOST];
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    }
    return _sessionManager;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tapToDismiss:) name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}

-(void)tapToDismiss:(NSNotification *)notification{
    [SVProgressHUD showErrorWithStatus:@"Cancel"];
    for (NSURLSessionDataTask *task in self.tasks) {
        [task cancel];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidReceiveTouchEventNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    self.title = NSLocalizedString(@"See others data", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    self.userList = [[UserDatabaseHelper queryOthers] mutableCopy];
    if (![self.userList count]) {
        [self refresh];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)refresh
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        return;
    }
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:@"Loading data"];
    
    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [self.sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@", [AccountHelper currentToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask *task = [self.sessionManager GET:@"user_list/" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSMutableArray *users = [NSMutableArray new];
        for (NSDictionary *dict in [responseObject objectForKey:@"items"]) {
            OtherUser *user = [[OtherUser alloc] initWithDictionary:dict];
            [UserDatabaseHelper addOtherUser:user];
            [users addObject:user];
        }
        
        self.userList = [[UserDatabaseHelper queryOthers] mutableCopy];
        
        if (users.count < self.userList.count) {
            for (OtherUser *user in self.userList) {
                BOOL found = NO;
                for (OtherUser *u in users) {
                    if (u.pk == user.pk) {
                        found = YES;
                    }
                }
                if (!found) {
                    [UserDatabaseHelper deleteOther:user];
                }
            }
            self.userList = [[UserDatabaseHelper queryOthers] mutableCopy];
        }
        
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error.localizedDescription);
        [SVProgressHUD dismiss];
    }];
    [self.tasks addObject:task];
}

- (void)refresh:(id)sender
{
    [self refresh];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Other Detail" sender:indexPath];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [UserDatabaseHelper countOthers];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"User Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    OtherUser *user = self.userList[indexPath.row];
    
    cell.textLabel.text = user.username;
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Other Detail"]) {
        NSIndexPath *indexPath = sender;
        OthersDataDetailTableViewController *controller = segue.destinationViewController;
        controller.user = [self.userList objectAtIndex:indexPath.row];
    }
}


@end
