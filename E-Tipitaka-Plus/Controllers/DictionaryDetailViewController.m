//
//  DictionaryDetailViewController.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/10/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "DictionaryDetailViewController.h"

@interface DictionaryDetailViewController ()

@end

@implementation DictionaryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = self.lexicon.head;
    NSString *font = self.dictionaryType == DictionaryTypeEnglish ? @"Helvetica" : @"THSarabunNew";
    NSString *html = [NSString stringWithFormat:
                        @"<html><head><style type=\"text/css\">body {font-family:\"%@\";font-size:64;margin:25;padding:5;}</style></head><body><h3>%@</h3><p>%@</p></body></html>",
                        font, self.lexicon.head,
                        [self.lexicon.translation stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]];
    
    self.webView.scrollView.bounces = NO;
    [self.webView loadHTMLString:html baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
