//
//  HighlightColorList.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDatabaseHelper.h"

@class HighlightColorList;

@protocol HighlightColorListDelegate <NSObject>

- (void)highlightColorListDidChange:(HighlightColorList *)colorList;

@end

@interface HighlightColorList : NSObject

@property (nonatomic, weak) id<HighlightColorListDelegate> delegate;

- (HighlightColor *)highlightColorAtIndex:(NSInteger)index;
- (void)addHighlightColor:(HighlightColor *)color;
- (void)removeHighlightColor:(HighlightColor *)color;
- (void)updateHighlightColor:(HighlightColor *)color;
- (NSInteger)size;
+ (HighlightColor *)getCurrentHighlightColor;
+ (void)setCurrentHighlightColor:(NSInteger)rowId;

@end
