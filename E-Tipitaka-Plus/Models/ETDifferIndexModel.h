//
//  ETDifferIndexModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 18/4/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "ETBasicDataModel.h"

@interface ETDifferIndexModel : ETBasicDataModel

- (NSDictionary *)itemMappingTable;

@end
