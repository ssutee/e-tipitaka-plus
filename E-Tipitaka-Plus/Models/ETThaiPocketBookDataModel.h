//
//  ETThaiPocketBookDataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 8/10/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "ETHandbookDataModel.h"

@interface ETThaiPocketBookDataModel : ETHandbookDataModel

@end
