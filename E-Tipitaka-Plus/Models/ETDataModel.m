//
//  ETDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 13/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "ETDataModel.h"
#import "UserDatabaseHelper.h"
#import "BookDatabaseHelper.h"

@interface ETDataModel()

@end

@implementation ETDataModel

@synthesize codeString = _codeString;
@synthesize code = _code;
@synthesize totalVolumes = _totalVolumes;
@synthesize title = _title;

- (id)init
{
    self = [super init];
    if (self) {
        self.titles = [BookDatabaseHelper bookNames];
    }
    return self;
}

- (BOOL)isBuddhawajana
{
    return NO;
}

- (BOOL)hasHtmlContent
{
    return NO;
}

- (NSInteger)firstVolume
{
    return 1;
}

- (BOOL)hasIndexNumber
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];    
}

- (NSInteger)totalVolumes
{
    NSInteger total = 0;
    for (int i=0; i < self.totalSections; ++i) {
        total += [self totalVolumesInSection:i+1];
    }
    return total;
}

- (NSInteger)totalSections
{
    return 3;
}

- (NSString *)title
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSString *)shortTitle
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSString *)abbrTitle
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSString *)placeholder
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSInteger) totalPagesOfVolume:(NSInteger)volume
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSInteger) totalItemsOfVolume:(NSInteger)volume
{
    return [self totalItemsOfVolume:volume useDefaultItemIndexing:YES];
}

- (NSInteger) totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (PageResult *) queryPage:(NSInteger)page inVolume:(NSInteger)volume
{
    return [BookDatabaseHelper queryPage:page inVolume:volume withCode:self.code];
}

- (NSArray *) queryPagesInVolume:(NSInteger)volume
{
    return [BookDatabaseHelper queryPagesInVolume:volume withCode:self.code];
}

- (NSArray *) queryItemsInVolume:(NSInteger)volume page:(NSInteger)page
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume
{
    return [self queryPagesOfItem:item inVolume:volume isDefault:NO];
}

- (NSArray *) queryPagesOfItem:(NSInteger)item inVolume:(NSInteger)volume isDefault:(BOOL)isDefault
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSArray *) search:(NSString *)keyword
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSArray *)search:(NSString *)keyword type:(SearchType)type
{
    return @[];
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];    
}

- (NSString *) volumeTitle:(NSInteger)volume
{
    return [self.titles objectForKey:[NSString stringWithFormat:@"%@_%d", self.codeString, volume]];
}

- (void)saveSearchResults:(NSArray *)results ofKeywords:(NSString *)keywords
{
    [UserDatabaseHelper saveSearchResults:results ofKeywords:keywords withCode:self.code];
}

- (NSInteger) getSubItemInVolume:(NSInteger)volume page:(NSInteger)page item:(NSInteger *)item
{
    NSDictionary *dict = [BookDatabaseHelper bookItems];
    for (NSNumber *sub in dict[self.codeString][[NSString stringWithFormat:@"%d", volume]]) {
        NSArray *pages = dict[self.codeString][[NSString stringWithFormat:@"%d", volume]][sub][[NSString stringWithFormat:@"%d", *item]];
        if ([pages containsObject:[NSNumber numberWithInteger:page]]) {
            return sub.integerValue;
        }
    }
    return 1;
}

- (NSInteger) totalVolumesInSection:(NSInteger)section
{
    switch (section) {
        case 1:
            return 8;
        case 2:
            return 25;
        case 3:
            return 12;
        default:
            return 0;
    }
}

- (NSInteger) convertVolume:(NSInteger)volume inSection:(NSInteger)section
{
    if (section == 1) return volume;
    if (section == 2) return volume + [self totalVolumesInSection:1];
    if (section == 3) return volume + [self totalVolumesInSection:1] + [self totalVolumesInSection:2];
    return volume;
}

- (NSInteger) convertVolume:(NSInteger)volume item:(NSInteger *)item subItem:(NSInteger)subItem
{
    return volume;
}

- (NSInteger) comparingVolume:(NSInteger)volume page:(NSInteger)page
{
    return volume;
}

- (NSArray *)convertToPivot:(NSInteger)volume page:(NSInteger)page item:(NSInteger)item
{
    NSInteger subItem = [self getSubItemInVolume:volume page:page item:&item];
    return @[[NSNumber numberWithInteger:[self comparingVolume:volume page:page]], [NSNumber numberWithInteger:item], [NSNumber numberWithInteger:subItem]];
}

- (NSArray *)convertFromPivot:(NSInteger)volume item:(NSInteger)item subItem:(NSInteger)subItem
{
    volume = [self convertVolume:volume item:&item subItem:subItem];
    NSInteger page = [BookDatabaseHelper pageOfItem:item subItem:subItem inVolume:volume withCodeString:self.codeString];
    return @[[NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page]];
}

- (BOOL)hasDifferentItemIndex
{
    return NO;
}

- (NSString *)itemIndexingSystemName
{
    return NSLocalizedString(@"PaliSiam", nil);
}

- (BOOL) isTipitaka
{
    return YES;
}

- (NSInteger)startPageOfVolume:(NSInteger)volume
{
    return 1;
}

@end
