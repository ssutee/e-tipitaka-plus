//
//  ETThaiMahaChula2DataModel.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/2/19.
//  Copyright © 2019 Watnapahpong. All rights reserved.
//

#import "ETThaiMahaChulaDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ETThaiMahaChula2DataModel : ETThaiMahaChulaDataModel

@end

NS_ASSUME_NONNULL_END
