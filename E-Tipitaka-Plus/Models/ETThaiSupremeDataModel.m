//
//  ETThaiSupremeDataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 18/4/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "ETThaiSupremeDataModel.h"
#import "BookDatabaseHelper.h"

@implementation ETThaiSupremeDataModel

- (LanguageCode)code
{
    return LanguageCodeThaiSupreme;
}

- (NSString *)codeString
{
    return @"thaims";
}

- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka Thai Supreme", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Thai Supreme", nil);
}

- (NSString *)abbrTitle
{
    return NSLocalizedString(@"thaims", nil);
}

- (NSString *)placeholder
{
    return NSLocalizedString(@"Enter Thai word", nil);
}

- (BOOL)hasDifferentItemIndex
{
    return YES;
}

- (BOOL)isValidVolume:(NSInteger)volume
{
    return volume >= 1 && volume <= 45;
}

- (NSString *)itemIndexingSystemName
{
    return NSLocalizedString(@"Supreme", nil);
}

- (NSInteger)totalItemsOfVolume:(NSInteger)volume useDefaultItemIndexing:(BOOL)defaultItemIndexing
{
    return defaultItemIndexing ? [BookDatabaseHelper totalItemsOfVolume:volume withCode:LanguageCodeThaiSiam] : [BookDatabaseHelper totalItemsOfVolume:volume withCodeString:self.codeString];
}

- (NSDictionary *)itemMappingTable
{
    return [BookDatabaseHelper itemMappingTableMs];
}

@end
