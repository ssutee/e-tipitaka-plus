//
//  HighlightColorList.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/7/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "HighlightColorList.h"
#import "HighlightColor.h"

@interface HighlightColorList()

@end


@implementation HighlightColorList

+ (HighlightColor *)getCurrentHighlightColor
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger selectedId = [defaults objectForKey:@"selected color id"] ? [[defaults objectForKey:@"selected color id"] integerValue] : 1;
    return [UserDatabaseHelper getHighlightColorByRowId:selectedId];
}

+ (void)setCurrentHighlightColor:(NSInteger)rowId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInteger:rowId] forKey:@"selected color id"];
    [defaults synchronize];
}

- (HighlightColor *)highlightColorAtIndex:(NSInteger)index
{
    return [[self list] objectAtIndex:index];
}

- (void)addHighlightColor:(HighlightColor *)color
{
    [UserDatabaseHelper addHighlightColor:color];
    
    [self.delegate highlightColorListDidChange:self];
}

- (void)removeHighlightColor:(HighlightColor *)color
{
    [UserDatabaseHelper deleteHighlightColorByRowId:color.rowId];
    [self.delegate highlightColorListDidChange:self];
}

- (void)updateHighlightColor:(HighlightColor *)color
{
    [UserDatabaseHelper updateHighlightColor:color];
    [self.delegate highlightColorListDidChange:self];
}

- (NSInteger)size
{
    return [[self list] count];
}

- (NSArray *)list
{
    return [UserDatabaseHelper queryHighlightColor];
}

@end
