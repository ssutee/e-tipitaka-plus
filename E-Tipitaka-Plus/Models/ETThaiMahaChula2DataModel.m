//
//  ETThaiMahaChula2DataModel.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 17/2/19.
//  Copyright © 2019 Watnapahpong. All rights reserved.
//

#import "ETThaiMahaChula2DataModel.h"

@implementation ETThaiMahaChula2DataModel

- (LanguageCode)code
{
    return LanguageCodeThaiMahaChula2;
}

- (NSString *)codeStringForResource
{
    return @"thaimc";
}


- (NSString *)title
{
    return NSLocalizedString(@"Tipitaka Thai Maha Chula 2", nil);
}

- (NSString *)shortTitle
{
    return NSLocalizedString(@"Thai Maha Chula 2", nil);
}



@end
