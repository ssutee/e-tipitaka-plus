//
//  History.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "History.h"

@implementation History

@synthesize keywords = _keywords;
@synthesize code = _code;
@synthesize detail = _detail;
@synthesize starred = _starred;
@synthesize rowId = _rowId;
@synthesize created = _created;
@synthesize type = _type;
@synthesize note = _note;
@synthesize buddhawaj = _buddhawaj;
@synthesize noteState = _noteState;
@synthesize priority = _priority;

- (NSDictionary *)dictionaryValue
{
    return @{
        @"rowid": [NSNumber numberWithInteger:self.rowId],
         @"keywords": self.keywords,
         @"type": [NSNumber numberWithInteger:self.type],
         @"code": [NSNumber numberWithInt:self.code],
         @"detail": self.detail,
         @"starred": [NSNumber numberWithBool:self.starred],
         @"state": [NSNumber numberWithInteger:self.state],
         @"note" : self.note ? self.note : @"",
         @"buddhawaj": [NSNumber numberWithBool:self.buddhawaj],
         @"note_state": [NSNumber numberWithUnsignedInteger:self.noteState],
         @"priority": [NSNumber numberWithInteger:self.priority],
         @"search_all": [NSNumber numberWithBool:self.searchAll],
         @"created":[NSNumber numberWithDouble:self.created.timeIntervalSince1970]        
    };
}

@end
