//
//  PageResult.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 30/10/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "PageResult.h"

@implementation PageResult

@synthesize volume = _volume;
@synthesize page = _page;
@synthesize items = _items;
@synthesize code = _code;
@synthesize header = _header;
@synthesize footer = _footer;
@synthesize content = _content;
@synthesize htmlContent = _htmlContent;

- (NSString *)description
{
    return [NSString stringWithFormat:@"%d : %@ %@", self.code, self.volume, self.page];
}

@end
