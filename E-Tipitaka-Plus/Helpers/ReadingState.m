//
//  ReadingState.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 12/23/16.
//  Copyright © 2016 Watnapahpong. All rights reserved.
//

#import "ReadingState.h"

@implementation ReadingState

- (NSDictionary *)dictionaryValue
{
    return @{@"code":[NSNumber numberWithInt:self.code],
             @"volume":[NSNumber numberWithInteger:self.volume],
             @"page":[NSNumber numberWithInteger:self.page],
             @"state":[NSNumber numberWithInt:self.state],
             @"count":[NSNumber numberWithInteger:self.count]};
}

@end
