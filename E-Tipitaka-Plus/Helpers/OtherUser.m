//
//  OtherUser.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "OtherUser.h"

@implementation OtherUser

- (NSString *)historyDatabasePath
{
    return [[DOCUMENT_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.pk]]] stringByAppendingPathComponent:@"history.sqlite"];
}

- (NSString *)bookmarkDatabasePath
{
    return [[DOCUMENT_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.pk]]] stringByAppendingPathComponent:@"bookmark.sqlite"];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.pk = [dict[@"pk"] integerValue];
        self.firstName = dict[@"first_name"];
        self.lastName = dict[@"last_name"];
        self.username = dict[@"username"];
    }
    return self;
}

@end
