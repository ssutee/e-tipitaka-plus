//
//  OtherUser.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 2/3/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OtherUser : NSObject

@property (nonatomic, assign) NSInteger pk;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, readonly) NSString *historyDatabasePath;
@property (nonatomic, readonly) NSString *bookmarkDatabasePath;

- (instancetype)initWithDictionary:(NSDictionary *)dict;


@end
