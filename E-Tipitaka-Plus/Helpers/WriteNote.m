//
//  WriteNote.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 20/8/18.
//  Copyright © 2018 Watnapahpong. All rights reserved.
//

#import "WriteNote.h"

@implementation WriteNote

- (NSDictionary *)dictionaryValue
{
    return @{@"rowid":[NSNumber numberWithInteger:self.rowId],
             @"note":self.note ? self.note : @"",
             @"created":[NSNumber numberWithDouble:self.created.timeIntervalSince1970],
             @"important":[NSNumber numberWithBool:self.starred],
             @"main":[NSNumber numberWithBool:self.main],
             @"rank": [NSNumber numberWithInteger:self.rank]};
}

@end
