//
//  History.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface History : NSObject

@property (nonatomic, strong) NSString *keywords;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, assign) NSInteger state;
@property (nonatomic, assign) NSInteger rowId;
@property (nonatomic, assign) BOOL starred;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) BOOL buddhawaj;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, assign) NSUInteger noteState;
@property (nonatomic, assign) NSInteger priority;
@property (nonatomic, assign) BOOL searchAll;

- (NSDictionary *)dictionaryValue;

@end
