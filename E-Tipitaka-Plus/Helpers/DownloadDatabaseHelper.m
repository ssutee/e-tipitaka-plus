//
//  DownloadDatabaseHelper.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 10/12/17.
//  Copyright © 2017 Watnapahpong. All rights reserved.
//

#import "DownloadDatabaseHelper.h"
#import "UserDatabaseHelper.h"

#import <JGProgressHUD/JGProgressHUD.h>
#import <SSZipArchive/SSZipArchive.h>
#import <AFNetworking/AFNetworking.h>

#include <sys/param.h>
#include <sys/mount.h>

#define ALERT_VIEW_EXIT 10001
#define ALERT_VIEW_HOST 10002


@interface DownloadDatabaseHelper () <UIAlertViewDelegate>
{
    BOOL _isInter;
}

@property (nonatomic, strong) JGProgressHUD *HUD;

@end

@implementation DownloadDatabaseHelper

@synthesize HUD = _HUD;

- (JGProgressHUD *)HUD
{
    if (!_HUD) {
        _HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        _HUD.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    }
    return _HUD;
}

+ (instancetype)sharedInstance
{
    static DownloadDatabaseHelper *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[DownloadDatabaseHelper alloc] init];
    });
    return __sharedInstance;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

+ (float) diskSpaceLeft
{
    NSArray* thePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    struct statfs tStats;
    const char *path = [[NSFileManager defaultManager] fileSystemRepresentationWithPath:[thePath lastObject]];
    statfs(path, &tStats);
    float availableSpace = (float)(tStats.f_bavail * tStats.f_bsize);
    
    return availableSpace;
}

- (void)downloadDatabases
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:DB_ZIP_PATH]) {
        [self unzipDatabase];
        return;
    }
    
    if ([DownloadDatabaseHelper diskSpaceLeft] < 700000000) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Need more disk space", nil) message:NSLocalizedString(@"The program needs at least 700MB for downloading databases", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"Exit", nil];
        alertView.tag = ALERT_VIEW_EXIT;
        [alertView show];
    } else {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose download server", nil) message:NSLocalizedString(@"Which is the server that you want to download the database from?", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* thailandButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Thailand", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            _isInter = NO;
            [self startDownload];
        }];
        
        UIAlertAction* otherPlacesButton = [UIAlertAction actionWithTitle:NSLocalizedString(NSLocalizedString(@"Other place", nil), nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            _isInter = YES;
            [self startDownload];
        }];
        
        [alert addAction:thailandButton];
        [alert addAction:otherPlacesButton];
        
        [self.viewController presentViewController:alert animated:YES completion:^{
        }];
    }
}

- (void)unzipDatabase
{
    if ([self checkExistingDatabases]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self start];
        });
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.HUD.textLabel.text = NSLocalizedString(@"Uncompressing database file", nil);
        self.HUD.indicatorView = [JGProgressHUDIndeterminateIndicatorView new];
    });
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        BOOL success = [SSZipArchive unzipFileAtPath:DB_ZIP_PATH toDestination:DOCUMENT_PATH];
        if (!success) {
            [[NSFileManager defaultManager] removeItemAtPath:DB_ZIP_PATH error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.HUD.textLabel.text = NSLocalizedString(@"Error", nil);
                [self.HUD dismiss];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Need more disk space", nil) message:NSLocalizedString(@"The program needs at least 700MB for storing databases", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"Exit", nil];
                [alertView show];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UserDatabaseHelper updateHistoryDatabase];
                self.HUD.indicatorView = [JGProgressHUDSuccessIndicatorView new];
                self.HUD.textLabel.text = NSLocalizedString(@"Complete", nil);
                [self start];
            });
        }
    });
}

- (BOOL)checkExistingDatabases
{
    return [[NSFileManager defaultManager] fileExistsAtPath:THAI_DB_PATH] && [[NSFileManager defaultManager] fileExistsAtPath:PALI_DB_PATH];
}

- (void)start
{
    _isFirstTime = NO;
    
    NSArray *codes = @[@"thai", @"pali", @"thaiwn", @"thaimm", @"thaimc", @"thaibt", @"thaipb", @"romanct", @"engdict", @"thaidict", @"pali_thai", @"palinew", @"thaims", @"thaimc2"];
    
    for (NSString *code in codes) {
        NSString *dbFile = [NSString stringWithFormat:@"%@.sqlite", code];
        NSString *dbPath = [DOCUMENT_PATH stringByAppendingPathComponent:dbFile];
        if ([[NSFileManager defaultManager] fileExistsAtPath:dbPath]) {
            [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:dbPath]];
        }
    }
    
    [[self countryCode] continueWithBlock:^id(BFTask *task) {
        _isInter = ![task.result isEqualToString:@"TH"];
        return [[self fetchUpdate] continueWithBlock:^id(BFTask *task) {
            [self databasesUpdate];
            return nil;
        }];
    }];
}

- (BFTask *)countryCode
{
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [task setResult:@"TH"];
        return task.task;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/plain", @"text/html"]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:kGeoServiceURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *code = [responseObject valueForKeyPath:@"geoplugin_countryCode"];
        [task setResult:code];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [task setResult:@"TH"];
    }];
    return task.task;
}

- (BFTask *)fetchUpdate
{
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [task setError:nil];
        return task.task;
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:UPDATE_URL]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:responseObject forKey:@"update"];
        [defaults synchronize];
        [task setResult:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error.localizedDescription);
        [task setError:error];
    }];
    [operation start];
    return task.task;
}

- (NSInteger)currentDatabaseVersion:(NSString *)stringCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id updateInfo = [defaults objectForKey:@"update"];
    return updateInfo ? [[[updateInfo objectForKey:stringCode] objectForKey:@"version"] integerValue] : 0;
}

- (NSString *)currentDatabaseFile:(NSString *)stringCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id updateInfo = [defaults objectForKey:@"update"];
    return updateInfo ? [[updateInfo objectForKey:stringCode] objectForKey:@"file"] : nil;
}

- (BFTask *)downloadDatabaseWithCode:(NSString *)stringCode
{
    if (self.HUD.hidden) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.textLabel.text = NSLocalizedString(@"Downloading database file", nil);
            [self.HUD showInView:self.viewController.view];
        });
    }
    
    NSString *zipFile = [self currentDatabaseFile:stringCode];
    NSString *url = [[self getHost:_isInter] stringByAppendingPathComponent:zipFile];
    return [self downloadDatabaseAtURL:url toPath:[DOCUMENT_PATH stringByAppendingPathComponent:zipFile]];
}

- (BFTask *)updateDatabaseWithCode:(NSString *)stringCode
{
    BFTaskCompletionSource *successful = [BFTaskCompletionSource taskCompletionSource];
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [successful setError:nil];
        return successful.task;
    }
    
    [[[[UserDatabaseHelper getDatabaseVersionWithCode:stringCode] continueWithSuccessBlock:^id(BFTask *task) {
        NSLog(@"%@ : %ld - %ld", stringCode, [task.result integerValue], [self currentDatabaseVersion:stringCode]);
        if ([task.result integerValue] != [self currentDatabaseVersion:stringCode]) {
            return [self downloadDatabaseWithCode:stringCode];
        } else {
            return nil;
        }
    }] continueWithSuccessBlock:^id(BFTask *task) {
        if (task.result) {
            return [self unzipDatabaseWithCode:stringCode];
        } else {
            return nil;
        }
    }] continueWithSuccessBlock:^id(BFTask *task) {
        [successful setResult:@"success"];
        return nil;
    }];
    return successful.task;
}

- (void)databasesUpdate
{
    [[[[[[[[[[[[[[[[[self updateDatabaseWithCode:@"thai"] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"pali"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaibt"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaimc"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaimm"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaiwn"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaipb"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"romanct"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"engdict"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaidict"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"pali_thai"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"suggest"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaivn"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaims"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"palinew"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        return [self updateDatabaseWithCode:@"thaimc2"];
    }] continueWithSuccessBlock:^id(BFTask *task) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:DatabaseCompleteNotification object:nil];
            [self.HUD dismissAfterDelay:1.0];
        });
        return nil;
    }];
}

- (void) forceDismissHUD
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.HUD dismiss];
    });    
}

- (BFTask *)unzipDatabaseAt:(NSString *)zipFile toPath:(NSString *)path
{
    NSError *error = nil;
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    }
    if (error || ![[NSFileManager defaultManager] fileExistsAtPath:zipFile]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.indicatorView = [JGProgressHUDErrorIndicatorView new];
            self.HUD.textLabel.text = NSLocalizedString(@"Error", nil);
            [self.HUD dismiss];
        });
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Database error", nil) message:NSLocalizedString(@"Cannot write database file", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"Exit", nil];
        [alertView show];
        [task setError:error];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.textLabel.text = NSLocalizedString(@"Uncompressing database file", nil);
            self.HUD.indicatorView = [JGProgressHUDIndeterminateIndicatorView new];
        });
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            BOOL success = [SSZipArchive unzipFileAtPath:zipFile toDestination:DOCUMENT_PATH];
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.HUD.textLabel.text = NSLocalizedString(@"Complete", nil);
                    self.HUD.indicatorView = [JGProgressHUDSuccessIndicatorView new];
                });
                [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:path]];
                [task setResult:@"success"];
            } else {
                NSError *error = nil;
                [[NSFileManager defaultManager] removeItemAtPath:zipFile error:&error];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.HUD.indicatorView = [JGProgressHUDErrorIndicatorView new];
                    self.HUD.textLabel.text = NSLocalizedString(@"Error", nil);
                    [self.HUD dismiss];
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot uncompress database file", nil) message:NSLocalizedString(@"Please check your disk space and try to download the database file again", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"Exit", nil];
                    [alertView show];
                });
                [task setError:error];
            }
        });
    }
    
    return task.task;
}

- (BFTask *)unzipDatabaseWithCode:(NSString *)stringCode
{
    NSString *zipFile = [self currentDatabaseFile:stringCode];
    NSString *dbFile = [NSString stringWithFormat:@"%@.sqlite", stringCode];
    return [self unzipDatabaseAt:[DOCUMENT_PATH stringByAppendingPathComponent:zipFile] toPath:[DOCUMENT_PATH stringByAppendingPathComponent:dbFile]];
}


- (BFTask *)downloadDatabaseAtURL:(NSString *)url toPath:(NSString *)path
{
    NSLog(@"%@", url);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.HUD.textLabel.text = NSLocalizedString(@"Downloading database file", nil);
        self.HUD.indicatorView = [JGProgressHUDIndeterminateIndicatorView new];
    });
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.textLabel.text = NSLocalizedString(@"Complete", nil);
            self.HUD.detailTextLabel.text = nil;
            self.HUD.indicatorView = [JGProgressHUDSuccessIndicatorView new];
        });
        [task setResult:@"success"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", url);
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.indicatorView = [JGProgressHUDErrorIndicatorView new];
            self.HUD.textLabel.text = NSLocalizedString(@"Error", nil);
            self.HUD.detailTextLabel.text = nil;
            [self.HUD dismiss];
        });
        [task setError:error];
    }];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float progress = (float)totalBytesRead/totalBytesExpectedToRead;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.indicatorView = [[JGProgressHUDPieIndicatorView alloc] init];
            self.HUD.progress = progress;
            self.HUD.detailTextLabel.text = [NSString stringWithFormat:@"%.2f%%", progress*100];
        });
    }];
    
    [operation start];
    return task.task;
}

- (NSString *)getHost:(BOOL)isInter
{
    return isInter ? S3_HOST : THAI_HOST;
}

- (void)startDownload
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.HUD.textLabel.text = NSLocalizedString(@"Downloading database file", nil);
        [self.HUD showInView:self.viewController.view];
    });
    
    NSString *url = [[self getHost:_isInter] stringByAppendingPathComponent:DB_ZIP];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:DB_ZIP_PATH append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.textLabel.text = NSLocalizedString(@"Complete", nil);
            self.HUD.detailTextLabel.text = nil;
        });
        [self unzipDatabase];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.textLabel.text = NSLocalizedString(@"Error", nil);
            self.HUD.indicatorView = [JGProgressHUDErrorIndicatorView new];
            self.HUD.detailTextLabel.text = nil;
            [self.HUD dismissAfterDelay:3.0];
        });
    }];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float progress = (float)totalBytesRead/totalBytesExpectedToRead;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.HUD.indicatorView = [[JGProgressHUDPieIndicatorView alloc] init];
            self.HUD.progress = progress;
            self.HUD.detailTextLabel.text = [NSString stringWithFormat:@"%.2f%%", progress*100];
        });
    }];
    
    [operation start];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == ALERT_VIEW_EXIT && buttonIndex == 0) {
        exit(1);
    } else if (alertView.tag == ALERT_VIEW_HOST && buttonIndex == 0) {
        _isInter = NO;
        [self startDownload];
    } else if (alertView.tag == ALERT_VIEW_HOST && buttonIndex == 1) {
        _isInter = YES;
        [self startDownload];
    }
}

@end


