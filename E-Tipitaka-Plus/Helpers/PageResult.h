//
//  PageResult.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 30/10/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageResult : NSObject

@property (nonatomic, strong) NSNumber *volume;
@property (nonatomic, strong) NSNumber *page;
@property (nonatomic, assign) LanguageCode code;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *header;
@property (nonatomic, strong) NSString *footer;
@property (nonatomic, strong) NSString *htmlContent;

@end
