//
//  UserDatabaseHelper.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "UserDatabaseHelper.h"
#import "BookDatabaseHelper.h"
#import "PageResult.h"
#import "NSString+Thai.h"
#import <OCTotallyLazy/OCTotallyLazy.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "ZipFile.h"
#import "FileInZipInfo.h"
#import "ZipReadStream.h"
#import "Highlight.h"
#import "HighlightNote.h"
#import "Lexicon.h"
#import "NoteItem.h"
#import <TouchXML/TouchXML.h>
#import "UserData.h"
#import "Tag.h"
#import "SyncDatabaseManager.h"
#import "HighlightColor.h"
#import "HighlightColorList.h"
#import "ReadingState.h"
#import "OtherUser.h"
#import "WriteNote.h"
#import "SearchAllHistory.h"

#define DB_DEBUG YES

@implementation UserDatabaseHelper

+ (FMDatabase *)noteDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:NOTE_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:NOTE_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)othersDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:OTHERS_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:OTHERS_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)bookmarkDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:BOOKMARK_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:BOOKMARK_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)bookmarkDatabaseOfUser:(OtherUser *)user
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:user.bookmarkDatabasePath]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:user.bookmarkDatabasePath];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}


+ (FMDatabase *)historyDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:HISTORY_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:HISTORY_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)historyDatabaseOfUser:(OtherUser *)user
{
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:user.historyDatabasePath]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:user.historyDatabasePath];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)highlightDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:HIGHLIGHT_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:HIGHLIGHT_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)highlightColorDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:HIGHLIGHT_COLOR_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:HIGHLIGHT_COLOR_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)highlightNoteDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:HIGHLIGHT_NOTE_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:HIGHLIGHT_NOTE_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}


+ (FMDatabase *)savedLexiconDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:SAVED_LEXICON_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:SAVED_LEXICON_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)userDataDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:USER_DATA_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:USER_DATA_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)tagDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:TAG_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:TAG_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)suggestDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:SUGGEST_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:SUGGEST_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}

+ (FMDatabase *)readingStateDatabase
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:READING_STATE_DB_PATH]){
        return nil;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:READING_STATE_DB_PATH];
    [db setLogsErrors:DB_DEBUG];
    [db setTraceExecution:DB_DEBUG];
    return db;
}


+ (BFTask *)getDatabaseVersion:(NSString *)dbPath
{
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if (![[NSFileManager defaultManager] fileExistsAtPath:dbPath]) {
            [task setResult:[NSNumber numberWithInteger:0]];
        } else {
            FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
            if (![db open]) {
                NSError *error = [NSError errorWithDomain:@"SQLite Error" code:0 userInfo:nil];
                [task setError:error];
            } else {
                FMResultSet *s1 = [db executeQuery:@"SELECT name FROM sqlite_master WHERE type='table'"];
                NSInteger version = 0;
                if ([s1 next]) {
                    FMResultSet *s2 = [db executeQuery:@"pragma user_version"];
                    version = [s2 next] ? [[s2 stringForColumnIndex:0] integerValue] : 1;
                    version = version ? version : 1;
                }
                [db close];
                [task setResult:[NSNumber numberWithInteger:version]];
            }
        }
    });
    
    return task.task;
}

+ (BFTask *)getThaiMMDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:THAIMM_DB_PATH];
}

+ (BFTask *)getThaiWNDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:THAIWN_DB_PATH];
}

+ (BFTask *)getThaiPBDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:THAIPB_DB_PATH];
}

+ (BFTask *)getRomanCTDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:ROMANCT_DB_PATH];
}

+ (BFTask *)getThaiMCDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:THAIMC_DB_PATH];
}

+ (BFTask *)getPaliDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:PALI_DB_PATH];
}

+ (BFTask *)getThaiBTDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:THAIBT_DB_PATH];
}

+ (BFTask *)getEnglishDictionaryDatabaseVersion
{
    return [UserDatabaseHelper getDatabaseVersion:ENG_DICT_DB_PATH];
}

+ (BFTask *)getDatabaseVersionWithCode:(NSString *)stringCode
{
    NSString *dbFile = [NSString stringWithFormat:@"%@.sqlite", stringCode];
    return [UserDatabaseHelper getDatabaseVersion:[DOCUMENT_PATH stringByAppendingPathComponent:dbFile]];
}

+ (void)updateReadingStateDatabase
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"pragma user_version"];
    
    if ([s next]) {
        NSString *version = [s stringForColumnIndex:0];
        if ([version isEqualToString:@"0"] || [version isEqualToString:@"1"]) {
            [db executeUpdate:@"ALTER TABLE reading_state ADD COLUMN count INTEGER DEFAULT 1"];
            [db executeUpdate:@"pragma user_version=2"];
        }
    }
    
    [db close];
}

+ (void)updateNoteDatabase
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"pragma user_version"];
    
    if ([s next]) {
        NSString *version = [s stringForColumnIndex:0];
        if ([version isEqualToString:@"0"] || [version isEqualToString:@"1"]) {
            [db executeUpdate:@"ALTER TABLE note ADD COLUMN main BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"pragma user_version=2"];
        }
    }
}

+ (void)updateTagDatabase
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"pragma user_version"];
    
    if ([s next]) {
        NSString *version = [s stringForColumnIndex:0];
        if ([version isEqualToString:@"0"]) {
            [db executeUpdate:@"ALTER TABLE tag ADD COLUMN search_all TEXT"];
            [db executeUpdate:@"pragma user_version=2"];
        }
    }
    
    [db close];
}

+ (void)updateHistoryDatabase
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"pragma user_version"];
    
    if ([s next]) {
        NSString *version = [s stringForColumnIndex:0];
        if ([version isEqualToString:@"0"]) {
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN marked TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN type INTEGER DEFAULT 1"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN buddhawaj BOOLEAN DEFAULT 1"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_items TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_state INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN priority INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN search_all BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"CREATE TABLE IF NOT EXISTS search_all (keywords TEXT, detail TEXT, state INTEGER DEFAULT 0, note TEXT, note_state INTEGER DEFAULT 0, type INTEGER DEFAULT 1, priority INTEGER DEFAULT 0, created FLOAT)"];
            [db executeUpdate:@"pragma user_version=8"];
        } else if ([version isEqualToString:@"2"]) {
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN type INTEGER DEFAULT 1"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN buddhawaj BOOLEAN DEFAULT 1"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_items TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_state INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN priority INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN search_all BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"CREATE TABLE IF NOT EXISTS search_all (keywords TEXT, detail TEXT, state INTEGER DEFAULT 0, note TEXT, note_state INTEGER DEFAULT 0, type INTEGER DEFAULT 1, priority INTEGER DEFAULT 0, created FLOAT)"];
            [db executeUpdate:@"pragma user_version=8"];
        } else if ([version isEqualToString:@"3"]) {
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN buddhawaj BOOLEAN DEFAULT 1"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_items TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_state INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN priority INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN search_all BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"CREATE TABLE IF NOT EXISTS search_all (keywords TEXT, detail TEXT, state INTEGER DEFAULT 0, note TEXT, note_state INTEGER DEFAULT 0, type INTEGER DEFAULT 1, priority INTEGER DEFAULT 0, created FLOAT)"];
            [db executeUpdate:@"pragma user_version=8"];
        } else if ([version isEqualToString:@"4"]) {
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_items TEXT"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_state INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN priority INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN search_all BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"CREATE TABLE IF NOT EXISTS search_all (keywords TEXT, detail TEXT, state INTEGER DEFAULT 0, note TEXT, note_state INTEGER DEFAULT 0, type INTEGER DEFAULT 1, priority INTEGER DEFAULT 0, created FLOAT)"];
            [db executeUpdate:@"pragma user_version=8"];
        } else if ([version isEqualToString:@"5"]) {
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN note_state INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN priority INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN search_all BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"CREATE TABLE IF NOT EXISTS search_all (keywords TEXT, detail TEXT, state INTEGER DEFAULT 0, note TEXT, note_state INTEGER DEFAULT 0, type INTEGER DEFAULT 1, priority INTEGER DEFAULT 0, created FLOAT)"];
            [db executeUpdate:@"pragma user_version=8"];
        } else if ([version isEqualToString:@"6"]) {
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN priority INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN search_all BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"CREATE TABLE IF NOT EXISTS search_all (keywords TEXT, detail TEXT, state INTEGER DEFAULT 0, note TEXT, note_state INTEGER DEFAULT 0, type INTEGER DEFAULT 1, priority INTEGER DEFAULT 0, created FLOAT)"];
            [db executeUpdate:@"pragma user_version=8"];
        } else if ([version isEqualToString:@"7"]) {
            [db executeUpdate:@"ALTER TABLE history ADD COLUMN search_all BOOLEAN DEFAULT 0"];
            [db executeUpdate:@"CREATE TABLE IF NOT EXISTS search_all (keywords TEXT, detail TEXT, state INTEGER DEFAULT 0, note TEXT, note_state INTEGER DEFAULT 0, type INTEGER DEFAULT 1, priority INTEGER DEFAULT 0, created FLOAT)"];
            [db executeUpdate:@"pragma user_version=8"];
        }
    }
    [db close];
}


+ (void)updateHighlightDatabase
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];

    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"pragma user_version"];
    
    if ([s next]) {
        NSString *version = [s stringForColumnIndex:0];
        if ([version isEqualToString:@"0"]) {
            [db executeUpdate:@"ALTER TABLE highlight ADD COLUMN position INTEGER DEFAULT 0"];
            [db executeUpdate:@"ALTER TABLE highlight ADD COLUMN color INTEGER DEFAULT 1"];
            [db executeUpdate:@"pragma user_version=2"];
        } else if ([version isEqualToString:@"2"]) {
            [db executeUpdate:@"ALTER TABLE highlight ADD COLUMN color INTEGER DEFAULT 1"];
            [db executeUpdate:@"pragma user_version=3"];
        }
    }
    
    [db close];
}

+ (void)createHighlightColorDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:HIGHLIGHT_COLOR_DB_PATH];
    
    if (![db open]) return;
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS highlightcolor (name TEXT, highlight1 TEXT, highlight2 TEXT)"];
    
    if (![[UserDatabaseHelper queryHighlightColor] count]) {
        [db executeUpdate:@"INSERT INTO highlightcolor VALUES (?,?,?)", NSLocalizedString(@"Standard Template", nil), DEFAULT_HIGHLIGHT_COLOR_1, DEFAULT_HIGHLIGHT_COLOR_2];
        [HighlightColorList setCurrentHighlightColor:1];
    }
    
    [db close];
}

+ (void)updateHighlightColorDatabase
{
    FMDatabase *db = [UserDatabaseHelper highlightColorDatabase];

    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"pragma user_version"];
    
    if ([s next]) {
        NSString *version = [s stringForColumnIndex:0];
        if ([version isEqualToString:@"0"]) {
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight3 TEXT"];
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight4 TEXT"];
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight5 TEXT"];
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight6 TEXT"];
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight7 TEXT"];
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight8 TEXT"];
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight9 TEXT"];
            [db executeUpdate:@"ALTER TABLE highlightcolor ADD COLUMN highlight10 TEXT"];
            [db executeUpdate:@"pragma user_version=2"];
        }
    }
    
    [db close];
}

+ (void)createHighlightDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:HIGHLIGHT_DB_PATH];
    
    if (![db open]) return;
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS highlight (selection TEXT, type INTEGER, note TEXT, start INTEGER, end INTEGER, volume INTEGER, page INTEGER, code INTEGER)"];
    
    [db close];
}

+ (void)createOthersDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:OTHERS_DB_PATH];
    
    if (![db open]) return;
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS others (pk INTEGER, username TEXT, first_name TEXT, last_name TEXT)"];
    
    [db close];
}


+ (void)createHighlightNoteDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:HIGHLIGHT_NOTE_DB_PATH];
    
    if (![db open]) return;
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS highlight_note (highlight_id INTEGER, selection TEXT, start INTEGER, end INTEGER)"];
    
    [db close];
}

+ (void)createReadingStateDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:READING_STATE_DB_PATH];
    
    if (![db open]) return;
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS reading_state (code INTEGER, volume INTEGER, page INTEGER, state INTEGER)"];
    
    [db close];
}

+ (void)createSavedLexiconDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:SAVED_LEXICON_DB_PATH];

    if (![db open]) return;

    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS lexicon (type INTEGER, head TEXT, translation TEXT)"];
    
    [db close];
}

+ (void)createTagDatabaseTable
{

    FMDatabase *db = [FMDatabase databaseWithPath:TAG_DB_PATH];
    
    if (![db open]) return;
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS tag (name TEXT, history TEXT, note TEXT, highlight TEXT, priority INTEGER, code INTEGER)"];
    
    [db close];
}

+ (void)createNoteDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:NOTE_DB_PATH];
    if (![db open]) return;
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS note (note TEXT, important INTEGER, created FLOAT, rank INTEGER)"];
    
    [db close];
}

+ (void)updateSearchAllHistoryAtColumn:(NSString *)column setValue:(id)value byRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:[NSString stringWithFormat:@"UPDATE search_all SET %@=? WHERE ROWID = ?", column], value, [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (void)deleteSearchAllHistoryByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"DELETE FROM search_all WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
    
    for (Tag *tag in [UserDatabaseHelper queryTags]) {
        if ([tag.searchAllhistory containsObject:[NSNumber numberWithInteger:rowId]]) {
            [tag.searchAllhistory removeObject:[NSNumber numberWithInteger:rowId]];
            [UserDatabaseHelper updateTagItems:@"search_all" items:tag.searchAllhistory ofTag:tag.name withCode:0];
        }
    }
}

+ (NSArray *)querySearchAllHistoriesInRowIds:(NSArray *)rowIds
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    NSString *statement = [NSString stringWithFormat:@"SELECT ROWID,keywords,type,detail,state,note,note_state,priority,created FROM search_all WHERE ROWID in %@", [UserDatabaseHelper convertArrayParameter:rowIds]];
    
    FMResultSet *s = [db executeQuery:statement];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        SearchAllHistory *history = [[SearchAllHistory alloc] init];
        history.rowId = [s intForColumn:@"ROWID"];
        history.keywords = [s stringForColumn:@"keywords"];
        history.type = [s intForColumn:@"type"];
        history.detail = [s stringForColumn:@"detail"];
        history.state = [s intForColumn:@"state"];
        history.note = [s stringForColumn:@"note"];
        history.noteState = [s intForColumn:@"note_state"];
        history.priority = [s intForColumn:@"priority"];
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumn:@"created"]];
        [results addObject:history];
    }
    
    [db close];
    
    [results sortUsingComparator:^NSComparisonResult(SearchAllHistory *h1, SearchAllHistory *h2) {
        NSNumber *idx1 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:h1.rowId]]];
        NSNumber *idx2 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:h2.rowId]]];
        return [idx1 compare:idx2];
    }];
    
    return results;
}

+ (void)updateSearchAllHistoryNote:(NSInteger)rowId note:(NSString *)note state:(NSUInteger)state
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"UPDATE search_all SET note=?, note_state=? WHERE ROWID = ?", note, [NSNumber numberWithInteger:state], [NSNumber numberWithInteger:rowId]];
    [db close];
    
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (SearchAllHistory *)getSearchAllHistoryByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }

    FMResultSet *s = [db executeQuery:@"SELECT ROWID,keywords,type,detail,state,note,note_state,priority,created FROM search_all WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    SearchAllHistory *history;
    if ([s next]) {
        history = [[SearchAllHistory alloc] init];
        history.rowId = [s intForColumn:@"ROWID"];
        history.keywords = [s stringForColumn:@"keywords"];
        history.type = [s intForColumn:@"type"];
        history.detail = [s stringForColumn:@"detail"];
        history.state = [s intForColumn:@"state"];
        history.note = [s stringForColumn:@"note"];
        history.noteState = [s intForColumn:@"note_state"];
        history.priority = [s intForColumn:@"priority"];
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumn:@"created"]];
    }
    
    [db close];
    
    return history;
}

+ (void)saveSearchAllHistory:(NSString *)keywords withDetail:(NSString *)detail
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    if (![db open]) return;
    
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM search_all WHERE keywords=?", keywords];
    if (![s next]) {
        [db executeUpdate:@"INSERT INTO search_all (keywords, note, detail, created) VALUES (?,?,?,?)", keywords, @"", detail, [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]]];
    }
    
    [db close];
}

+ (NSArray *)querySearchAllHistories
{
    return [UserDatabaseHelper querySearchAllHistories:@""];
}

+ (NSArray *)querySearchAllHistories:(NSString *)keywords
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    if (![db open]) return nil;
    
    FMResultSet *s;
    if (keywords.length > 0) {
        s = [db executeQuery:@"SELECT ROWID,keywords,type,detail,state,note,note_state,priority,created FROM search_all WHERE keywords GLOB ?", [NSString stringWithFormat:@"*%@*", keywords]];
    } else {
        s = [db executeQuery:@"SELECT ROWID,keywords,type,detail,state,note,note_state,priority,created FROM search_all"];
    }
    
    NSMutableArray *result = [NSMutableArray new];
    
    while([s next]) {
        SearchAllHistory *history = [SearchAllHistory new];
        history.rowId = [s intForColumn:@"ROWID"];
        history.keywords = [s stringForColumn:@"keywords"];
        history.type = [s intForColumn:@"type"];
        history.detail = [s stringForColumn:@"detail"];
        history.state = [s intForColumn:@"state"];
        history.note = [s stringForColumn:@"note"];
        history.noteState = [s intForColumn:@"note_state"];
        history.priority = [s intForColumn:@"priority"];
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumn:@"created"]];
        [result addObject:history];
    }
    
    [db close];
    return result;
}

+ (NSInteger)countOthers
{
    FMDatabase *db = [UserDatabaseHelper othersDatabase];
    
    if (![db open]) return 0;
    
    FMResultSet *s = [db executeQuery:@"SELECT COUNT(*) FROM others"];
    NSInteger count = 0;
    if ([s next]) {
        count = [s intForColumnIndex:0];
    }
    
    [db close];
    return count;
}

+ (void)deleteOther:(OtherUser *)user
{
    FMDatabase *db = [UserDatabaseHelper othersDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"DELETE FROM others WHERE username=?", user.username];
}

+ (NSArray *)queryOthers
{
    FMDatabase *db = [UserDatabaseHelper othersDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM others ORDER BY pk"];
    
    NSMutableArray *result = [NSMutableArray new];
    while([s next]) {
        OtherUser *user = [OtherUser new];
        user.pk = [s intForColumn:@"pk"];
        user.username = [s stringForColumn:@"username"];
        user.firstName = [s stringForColumn:@"first_name"];
        user.lastName = [s stringForColumn:@"last_name"];
        [result addObject:user];
    }
    [db close];
    return result;
}

+ (void)addOtherUser:(OtherUser *)user
{
    FMDatabase *db = [UserDatabaseHelper othersDatabase];
        
    if (![db open]) return;
    
    if(![UserDatabaseHelper getOtherUserByUsername:user.username]) {
        [db executeUpdate:@"INSERT INTO others VALUES (?,?,?,?)", [NSNumber numberWithInteger:user.pk], user.username, user.firstName, user.lastName];
    }
    
    [db close];
}

+ (OtherUser *)getOtherUserByUsername:(NSString *)username
{
    FMDatabase *db = [UserDatabaseHelper othersDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM others WHERE username=?", username];
    
    OtherUser *user = nil;
    if ([s next]) {
        user = [OtherUser new];
        user.pk = [s intForColumn:@"pk"];
        user.username = [s stringForColumn:@"username"];
        user.firstName = [s stringForColumn:@"first_name"];
        user.lastName = [s stringForColumn:@"last_name"];
    }
    [db close];
    return user;
}

+ (void)initReadingStateDatabase
{
    if ([UserDatabaseHelper countReadingStates]) {
        return;
    }
    
    for (Highlight *highlight in [UserDatabaseHelper queryHighlights]) {
        ReadingState *readingState = [ReadingState new];
        readingState.code = highlight.code;
        readingState.volume = highlight.volume;
        readingState.page = highlight.page;
        readingState.state = ReadingStateTypeRead;
        readingState.count = 1;
        [UserDatabaseHelper addReadingState:readingState];
    }
    
    for (History *history in [UserDatabaseHelper queryHistories]) {
        NSArray *items = [UserDatabaseHelper historyItemsById:history.rowId];
        for(NSNumber *index in [UserDatabaseHelper getHistoryReadItemsByRowId:history.rowId]) {
            if (index.integerValue >= [items count]) { continue; }
            NSDictionary *item = items[index.integerValue];
            ReadingState *readingState = [ReadingState new];
            readingState.code = history.code;
            readingState.volume = [item[@"volume"] integerValue];
            readingState.page = [item[@"page"] integerValue];
            readingState.state = ReadingStateTypeRead;
            readingState.count = 1;
            [UserDatabaseHelper addReadingState:readingState];
        }
    }
    
    for (Bookmark *bookmark in [UserDatabaseHelper queryBookmarks]) {
        ReadingState *readingState = [ReadingState new];
        readingState.code = bookmark.code;
        readingState.volume = bookmark.volume;
        readingState.page = bookmark.page;
        readingState.state = ReadingStateTypeRead;
        readingState.count = 1;
        [UserDatabaseHelper addReadingState:readingState];
    }
}

+ (NSInteger)countReadingStates
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return 0;
    
    FMResultSet *s = [db executeQuery:@"SELECT COUNT(*) FROM reading_state"];
    NSInteger count = 0;
    if ([s next]) {
        count = [s intForColumnIndex:0];
    }
    
    [db close];
    return count;
}

+ (NSArray *)queryReadingStates
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM reading_state ORDER BY volume ASC, page ASC"];
    
    NSMutableArray *result = [NSMutableArray new];
    while([s next]) {
        ReadingState *readingState = [ReadingState new];
        readingState.code = [s intForColumn:@"code"];
        readingState.page = [s intForColumn:@"page"];
        readingState.volume = [s intForColumn:@"volume"];
        readingState.state = [s intForColumn:@"state"];
        readingState.count = [s intForColumn:@"count"];
        [result addObject:readingState];
    }
    [db close];
    return result;
}

+ (NSArray *)queryReadingStatesWithCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM reading_state WHERE code=? ORDER BY volume ASC, page ASC", [NSNumber numberWithInt:code]];
    
    NSMutableArray *result = [NSMutableArray new];
    while([s next]) {
        ReadingState *readingState = [ReadingState new];
        readingState.code = code;
        readingState.volume = [s intForColumn:@"volume"];        
        readingState.page = [s intForColumn:@"page"];
        readingState.state = [s intForColumn:@"state"];
        readingState.count = [s intForColumn:@"count"];
        [result addObject:readingState];
    }
    [db close];
    return result;
}

+ (NSArray *)queryReadingStates:(ReadingStateType)state code:(LanguageCode)code volume:(NSInteger)volume
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM reading_state WHERE code=? AND volume=? AND state=? ORDER BY volume ASC, page ASC", [NSNumber numberWithInt:code], [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:state]];
    
    NSMutableArray *result = [NSMutableArray new];
    while([s next]) {
        ReadingState *readingState = [ReadingState new];
        readingState.code = code;
        readingState.volume = volume;
        readingState.page = [s intForColumn:@"page"];
        readingState.state = state;
        readingState.count = [s intForColumn:@"count"];
        [result addObject:readingState];
    }
    [db close];
    return result;

}

+ (NSArray *)queryReadingStatesWithCode:(LanguageCode)code andVolume:(NSInteger)volume
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM reading_state WHERE code=? AND volume=? ORDER BY volume ASC, page ASC", [NSNumber numberWithInt:code], [NSNumber numberWithInteger:volume]];
    
    NSMutableArray *result = [NSMutableArray new];
    while([s next]) {
        ReadingState *readingState = [ReadingState new];
        readingState.code = code;
        readingState.volume = volume;
        readingState.page = [s intForColumn:@"page"];
        readingState.state = [s intForColumn:@"state"];
        readingState.count = [s intForColumn:@"count"];
        [result addObject:readingState];
    }
    [db close];
    return result;
}

+ (NSInteger)countReadingStatesWithCode:(LanguageCode)code andVolume:(NSInteger)volume
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return 0;
    
    FMResultSet *s = [db executeQuery:@"SELECT COUNT(*) FROM reading_state WHERE code=? AND volume=?", [NSNumber numberWithInt:code], [NSNumber numberWithInteger:volume]];
    
    NSInteger count = 0;
    if ([s next]) {
        count = [s intForColumnIndex:0];
    }
    
    [db close];
    
    return count;
}


+ (NSInteger)countReadingStates:(ReadingStateType)state code:(LanguageCode)code volume:(NSInteger)volume
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return 0;
    
    FMResultSet *s = [db executeQuery:@"SELECT COUNT(*) FROM reading_state WHERE code=? AND volume=? AND state=?", [NSNumber numberWithInt:code], [NSNumber numberWithInteger:volume], [NSNumber numberWithInt:state]];
    
    NSInteger count = 0;
    if ([s next]) {
        count = [s intForColumnIndex:0];
    }
    
    [db close];
    
    return count;
}

+ (NSInteger)countReadingStates:(ReadingStateType)state code:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return 0;
    
    FMResultSet *s = [db executeQuery:@"SELECT COUNT(*) FROM reading_state WHERE code=? AND state=?", [NSNumber numberWithInt:code], [NSNumber numberWithInt:state]];
    
    NSInteger count = 0;
    if ([s next]) {
        count = [s intForColumnIndex:0];
    }
    
    [db close];
    
    return count;
}

+ (NSInteger)countReadingStates:(ReadingStateType)state code:(LanguageCode)code fromVolume:(NSInteger)start toVolume:(NSInteger)end
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return 0;
    
    FMResultSet *s = [db executeQuery:@"SELECT COUNT(*) FROM reading_state WHERE code=? AND state=? AND volume>=? AND volume<=?", [NSNumber numberWithInt:code], [NSNumber numberWithInt:state], [NSNumber numberWithInteger:start], [NSNumber numberWithInteger:end]];
    
    NSInteger count = 0;
    if ([s next]) {
        count = [s intForColumnIndex:0];
    }
    
    [db close];
    
    return count;
}



+ (ReadingState *)getReadingState:(LanguageCode)code volume:(NSInteger)volume page:(NSInteger)page
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM reading_state WHERE code=? AND volume=? AND page=?", [NSNumber numberWithInteger:code], [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page]];
    
    ReadingState *readingState = [ReadingState new];
    readingState.code = code;
    readingState.volume = volume;
    readingState.page = page;
    if ([s next]) {
        readingState.state = [s intForColumn:@"state"];
        readingState.count = [s intForColumn:@"count"];
    } else {
        readingState.state = ReadingStateTypeUnread;
    }
    
    [db close];
    
    return readingState;
}

+ (ReadingState *)addReadingState:(ReadingState *)readingState
{
    FMDatabase *db = [UserDatabaseHelper readingStateDatabase];
    
    if (![db open]) return nil;
    
    ReadingState *oldReadingState = [UserDatabaseHelper getReadingState:readingState.code volume:readingState.volume page:readingState.page];
    
    ReadingState *result = oldReadingState;
    
    if (oldReadingState.state == ReadingStateTypeSkimmed && oldReadingState.state != readingState.state && readingState.state != ReadingStateTypeUnread) {
        [db executeUpdate:@"UPDATE reading_state SET state=?,count=? WHERE code=? AND volume=? AND page=?", [NSNumber numberWithInteger:readingState.state], @1, [NSNumber numberWithInteger:readingState.code], [NSNumber numberWithInteger:readingState.volume], [NSNumber numberWithInteger:readingState.page]];
        result = readingState;
        [[SyncDatabaseManager sharedInstance] delayPush:READING_STATE_DB_PATH];
    } else if (oldReadingState.state == ReadingStateTypeUnread && readingState.state != ReadingStateTypeUnread) {
        [db executeUpdate:@"INSERT INTO reading_state VALUES (?,?,?,?,?)", [NSNumber numberWithInteger:readingState.code], [NSNumber numberWithInteger:readingState.volume], [NSNumber numberWithInteger:readingState.page], [NSNumber numberWithInteger:readingState.state], [NSNumber numberWithInteger:1]];
        result = readingState;
        [[SyncDatabaseManager sharedInstance] delayPush:READING_STATE_DB_PATH];
    } else if (oldReadingState.state == readingState.state && oldReadingState.state != ReadingStateTypeUnread) {
        [db executeUpdate:@"UPDATE reading_state SET count=? WHERE code=? AND volume=? AND page=?", [NSNumber numberWithInteger:oldReadingState.count+1], [NSNumber numberWithInteger:readingState.code], [NSNumber numberWithInteger:readingState.volume], [NSNumber numberWithInteger:readingState.page]];
        readingState.count = (oldReadingState.count ? oldReadingState.count : 1) + 1;
        result = readingState;
        [[SyncDatabaseManager sharedInstance] delayPush:READING_STATE_DB_PATH];
    }
    
    [db close];
    
    return result;
}

+ (NSString *)createHtmlHighlightNotes:(Highlight *)highlight withBlock:(NSString *(^)(NSString *, NSInteger))block
{
    NSArray *highlightNotes = [UserDatabaseHelper queryHighlightNoteOfHighlight:highlight];
    
    NSString *text = @"";
    
    text = highlightNotes.count ? [text stringByAppendingString:[highlight.note substringToIndex:((HighlightNote *)highlightNotes[0]).range.location]] : highlight.note;
    
    NSInteger totalLength = text.length;
    
    for (int i=0; i<highlightNotes.count; ++i) {
        HighlightNote *highlightNote = highlightNotes[i];
        HighlightNote *next = i < highlightNotes.count-1 ? highlightNotes[i+1] : nil;
        
        NSString *selection = @"";
        if (highlightNote.range.location >= totalLength) {
            selection = [highlight.note substringWithRange:highlightNote.range];
        } else if (highlightNote.range.location + highlightNote.range.length > totalLength) {
            selection = [highlight.note substringWithRange:NSMakeRange(totalLength, highlightNote.range.location+highlightNote.range.length-totalLength)];
        }
        
        totalLength += selection.length;
        
        text = [text stringByAppendingString:block(selection, highlightNote.rowId)];
                
        if (next && next.range.location > totalLength) {
            NSInteger location = totalLength;
            NSInteger length = next.range.location-totalLength;
            text = [text stringByAppendingString:[highlight.note substringWithRange:NSMakeRange(location, length)]];
            totalLength += length;
        } else if (!next) {
            text = [text stringByAppendingString:[highlight.note substringFromIndex:highlightNote.range.location+highlightNote.range.length]];
        }
    }
    
    return text;
}

+ (NSArray *)queryHighlightNoteOfHighlight:(Highlight *)highlight
{
    FMDatabase *db = [UserDatabaseHelper highlightNoteDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID,highlight_id,selection,start,end FROM highlight_note WHERE highlight_id=? ORDER BY start ASC", [NSNumber numberWithInteger:highlight.rowId]];
    
    NSMutableArray *results = [NSMutableArray new];
    while ([s next]) {
        HighlightNote *highlightNote = [HighlightNote new];
        highlightNote.highlight = highlight;
        highlightNote.selection = [s stringForColumn:@"selection"];
        highlightNote.range = NSMakeRange([s intForColumn:@"start"], [s intForColumn:@"end"] - [s intForColumn:@"start"]);
        highlightNote.rowId = [s intForColumn:@"ROWID"];
        [results addObject:highlightNote];
    }
    
    return results;
}

+ (HighlightNote *)getHighlightNoteByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper highlightNoteDatabase];
    
    if (![db open]) return nil;
    
    HighlightNote *highlightNote = nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID,highlight_id,selection,start,end FROM highlight_note WHERE ROWID=?", [NSNumber numberWithInteger:rowId]];
    
    if ([s next]) {
        highlightNote = [HighlightNote new];
        highlightNote.highlight = [UserDatabaseHelper getHighlightByRowId:[s intForColumn:@"highlight_id"]];
        highlightNote.selection = [s stringForColumn:@"selection"];
        highlightNote.range = NSMakeRange([s intForColumn:@"start"], [s intForColumn:@"end"] - [s intForColumn:@"start"]);
        highlightNote.rowId = [s intForColumn:@"ROWID"];
    }
    
    return highlightNote;
}

+ (BOOL)checkOverlapHighlightNotesInRange:(NSRange)range ofHighlight:(Highlight *)highlight
{
    FMDatabase *db = [UserDatabaseHelper highlightNoteDatabase];
    
    if (![db open]) return YES;
    
    BOOL overlap = NO;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM highlight_note WHERE highlight_id=? AND ((start < ? AND end > ?) OR (start < ? AND end > ?) OR (start > ? AND end < ?))", [NSNumber numberWithInt:highlight.rowId], [NSNumber numberWithInteger:range.location], [NSNumber numberWithInteger:range.location], [NSNumber numberWithInteger:range.location+range.length], [NSNumber numberWithInteger:range.location+range.length], [NSNumber numberWithInteger:range.location], [NSNumber numberWithInteger:range.location+range.length]];
    
    overlap = [s next];
    
    [db close];
    
    return overlap;
}

+ (void)addHighlightNote:(HighlightNote *)highlightNote
{
    FMDatabase *db = [UserDatabaseHelper highlightNoteDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"INSERT INTO highlight_note VALUES (?,?,?,?)", [NSNumber numberWithInteger:highlightNote.highlight.rowId], highlightNote.selection, [NSNumber numberWithInteger:highlightNote.range.location], [NSNumber numberWithInteger:highlightNote.range.location+highlightNote.range.length]];
    
    [db close];
    
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_NOTE_DB_PATH];
}

+ (BOOL)deleteHighlightNote:(HighlightNote *)highlightNote
{
    FMDatabase *db = [UserDatabaseHelper highlightNoteDatabase];
    
    if (![db open]) return NO;
    
    BOOL ret = [db executeUpdate:@"DELETE FROM highlight_note WHERE ROWID=?", [NSNumber numberWithInt:highlightNote.rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_NOTE_DB_PATH];
    return ret;
}

+ (HighlightColor *)getHighlightColorByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper highlightColorDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlightcolor WHERE ROWID=?", [NSNumber numberWithInteger:rowId]];
    
    HighlightColor *color = nil;
    if ([s next]) {
        NSString *name = [s stringForColumn:@"name"];
        NSString *highlight1 = [s stringForColumn:@"highlight1"] ? [s stringForColumn:@"highlight1"] : DEFAULT_HIGHLIGHT_COLOR_1;
        NSString *highlight2 = [s stringForColumn:@"highlight2"] ? [s stringForColumn:@"highlight2"] : DEFAULT_HIGHLIGHT_COLOR_2;
        NSString *highlight3 = [s stringForColumn:@"highlight3"] ? [s stringForColumn:@"highlight3"] : DEFAULT_HIGHLIGHT_COLOR_3;
        NSString *highlight4 = [s stringForColumn:@"highlight4"] ? [s stringForColumn:@"highlight4"] : DEFAULT_HIGHLIGHT_COLOR_4;
        NSString *highlight5 = [s stringForColumn:@"highlight5"] ? [s stringForColumn:@"highlight5"] : DEFAULT_HIGHLIGHT_COLOR_5;
        NSString *highlight6 = [s stringForColumn:@"highlight6"] ? [s stringForColumn:@"highlight6"] : DEFAULT_HIGHLIGHT_COLOR_6;
        NSString *highlight7 = [s stringForColumn:@"highlight7"] ? [s stringForColumn:@"highlight7"] : DEFAULT_HIGHLIGHT_COLOR_7;
        NSString *highlight8 = [s stringForColumn:@"highlight8"] ? [s stringForColumn:@"highlight8"] : DEFAULT_HIGHLIGHT_COLOR_8;
        NSString *highlight9 = [s stringForColumn:@"highlight9"] ? [s stringForColumn:@"highlight9"] : DEFAULT_HIGHLIGHT_COLOR_9;
        NSString *highlight10 = [s stringForColumn:@"highlight10"] ? [s stringForColumn:@"highlight10"] : DEFAULT_HIGHLIGHT_COLOR_10;
        NSInteger rowId = [s intForColumn:@"ROWID"];
        color = [HighlightColor new];
        color.rowId = rowId;
        color.name = name;
        color.inlineColors = [@[highlight1,highlight2,highlight3,highlight4,highlight5] mutableCopy];
        color.footerColors = [@[highlight6,highlight7,highlight8,highlight9,highlight10] mutableCopy];
    }
    [db close];
    
    return color;
}

+ (NSArray *)queryHighlightColor
{
    FMDatabase *db = [UserDatabaseHelper highlightColorDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlightcolor"];
    
    NSMutableArray *results = [NSMutableArray new];
    while ([s next]) {
        NSString *name = [s stringForColumn:@"name"];
        NSString *highlight1 = [s stringForColumn:@"highlight1"] ? [s stringForColumn:@"highlight1"] : DEFAULT_HIGHLIGHT_COLOR_1;
        NSString *highlight2 = [s stringForColumn:@"highlight2"] ? [s stringForColumn:@"highlight2"] : DEFAULT_HIGHLIGHT_COLOR_2;
        NSString *highlight3 = [s stringForColumn:@"highlight3"] ? [s stringForColumn:@"highlight3"] : DEFAULT_HIGHLIGHT_COLOR_3;
        NSString *highlight4 = [s stringForColumn:@"highlight4"] ? [s stringForColumn:@"highlight4"] : DEFAULT_HIGHLIGHT_COLOR_4;
        NSString *highlight5 = [s stringForColumn:@"highlight5"] ? [s stringForColumn:@"highlight5"] : DEFAULT_HIGHLIGHT_COLOR_5;
        NSString *highlight6 = [s stringForColumn:@"highlight6"] ? [s stringForColumn:@"highlight6"] : DEFAULT_HIGHLIGHT_COLOR_6;
        NSString *highlight7 = [s stringForColumn:@"highlight7"] ? [s stringForColumn:@"highlight7"] : DEFAULT_HIGHLIGHT_COLOR_7;
        NSString *highlight8 = [s stringForColumn:@"highlight8"] ? [s stringForColumn:@"highlight8"] : DEFAULT_HIGHLIGHT_COLOR_8;
        NSString *highlight9 = [s stringForColumn:@"highlight9"] ? [s stringForColumn:@"highlight9"] : DEFAULT_HIGHLIGHT_COLOR_9;
        NSString *highlight10 = [s stringForColumn:@"highlight10"] ? [s stringForColumn:@"highlight10"] : DEFAULT_HIGHLIGHT_COLOR_10;
        NSInteger rowId = [s intForColumn:@"ROWID"];
        HighlightColor *color = [HighlightColor new];
        color.rowId = rowId;
        color.name = name;
        color.inlineColors = [@[highlight1,highlight2,highlight3,highlight4,highlight5] mutableCopy];
        color.footerColors = [@[highlight6,highlight7,highlight8,highlight9,highlight10] mutableCopy];
        [results addObject:color];
    }
    return results;
}

+ (BOOL)deleteHighlightColorByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper highlightColorDatabase];
    
    if (![db open]) return NO;
    
    BOOL ret = [db executeUpdate:@"DELETE FROM highlightcolor WHERE ROWID=?", [NSNumber numberWithInt:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_COLOR_DB_PATH];
    return ret;
    
}

+ (void)addHighlightColor:(HighlightColor *)color
{
    FMDatabase *db = [UserDatabaseHelper highlightColorDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"INSERT INTO highlightcolor VALUES (?,?,?,?,?,?,?,?,?,?,?)", color.name,
        color.inlineColors[0], color.inlineColors[1], color.inlineColors[2], color.inlineColors[3], color.inlineColors[4],
        color.footerColors[0], color.inlineColors[1], color.inlineColors[2], color.inlineColors[3], color.inlineColors[4]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_COLOR_DB_PATH];

}

+ (void)updateHighlightColor:(HighlightColor *)color
{
    FMDatabase *db = [UserDatabaseHelper highlightColorDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"UPDATE highlightcolor SET name=?,highlight1=?,highlight2=?,highlight3=?,highlight4=?,highlight5=?,highlight6=?,highlight7=?,highlight8=?,highlight9=?,highlight10=? WHERE ROWID=?", color.name,
        color.inlineColors[0], color.inlineColors[1], color.inlineColors[2], color.inlineColors[3], color.inlineColors[4],
        color.footerColors[0], color.footerColors[1], color.footerColors[2], color.footerColors[3], color.footerColors[4],
        [NSNumber numberWithInteger:color.rowId]];
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_COLOR_DB_PATH];
}

+ (void)saveLexicon:(Lexicon *)lexicon type:(DictionaryType)type
{
    FMDatabase *db = [UserDatabaseHelper savedLexiconDatabase];
    
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM lexicon WHERE type=? AND head=?", [NSNumber numberWithInt:type], lexicon.head];

    if (![s next]) {
        [db executeUpdate:@"INSERT INTO lexicon VALUES (?,?,?)", [NSNumber numberWithInt:type], lexicon.head, lexicon.translation];
    }
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:SAVED_LEXICON_DB_PATH];
}

+ (BOOL)deleteLexicon:(Lexicon *)lexicon type:(DictionaryType)type
{
    FMDatabase *db = [UserDatabaseHelper savedLexiconDatabase];
    
    if (![db open]) return NO;
    
    BOOL ret = [db executeUpdate:@"DELETE FROM lexicon WHERE head=? AND translation=? AND type=?", lexicon.head, lexicon.translation, [NSNumber numberWithInt:lexicon.type]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:SAVED_LEXICON_DB_PATH];
    return ret;
}

+ (NSArray *)querySavedLexicon:(DictionaryType)type
{
    FMDatabase *db = [UserDatabaseHelper savedLexiconDatabase];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    if (![db open]) {
        return items;
    }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM lexicon WHERE type=? ORDER BY head", [NSNumber numberWithInt:type]];
    
    while ([s next]) {
        Lexicon *lexicon = [[Lexicon alloc] initWithHead:[s stringForColumn:@"head"] andTranslation:[s stringForColumn:@"translation"]];
        lexicon.type = type;
        [items addObject:lexicon];
    }
    
    [db close];
    
    return items;
}

+ (NSArray *)querySavedLexicon
{
    FMDatabase *db = [UserDatabaseHelper savedLexiconDatabase];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    if (![db open]) {
        return items;
    }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM lexicon ORDER BY head"];
    
    while ([s next]) {
        Lexicon *lexicon = [[Lexicon alloc] initWithHead:[s stringForColumn:@"head"] andTranslation:[s stringForColumn:@"translation"]];
        lexicon.type = [s intForColumn:@"type"];
        [items addObject:lexicon];
    }
    
    [db close];
    
    return items;
}


+ (void)saveHighlight:(Highlight *)highlight
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"INSERT INTO highlight VALUES (?,?,?,?,?,?,?,?,?,?)", highlight.selection, [NSNumber numberWithInt:highlight.type], highlight.note, [NSNumber numberWithInteger:highlight.range.location], [NSNumber numberWithInteger:highlight.range.length+highlight.range.location], [NSNumber numberWithInteger:highlight.volume], [NSNumber numberWithInteger:highlight.page], [NSNumber numberWithInt:highlight.code], [NSNumber numberWithInteger:highlight.position], [NSNumber numberWithInteger:highlight.color]];
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_DB_PATH];
}

+ (void)updateHighlight:(Highlight *)highlight
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return;

    [db executeUpdate:@"UPDATE highlight SET selection=?,type=?,note=?,start=?,end=?,position=?,color=? WHERE ROWID=?", highlight.selection, [NSNumber numberWithInt:highlight.type], highlight.note, [NSNumber numberWithInteger:highlight.range.location], [NSNumber numberWithInteger:highlight.range.length+highlight.range.location], [NSNumber numberWithInteger:highlight.position], [NSNumber numberWithInteger:highlight.color], [NSNumber numberWithInteger:highlight.rowId]];
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_DB_PATH];
}

+ (NSInteger)checkExistingHighlight:(Highlight *)highlight
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return -1;
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID FROM highlight WHERE start=? AND end=? AND volume=? AND page=? AND code=?", [NSNumber numberWithInteger:highlight.range.location], [NSNumber numberWithInteger:highlight.range.length+highlight.range.location], [NSNumber numberWithInteger:highlight.volume], [NSNumber numberWithInteger:highlight.page], [NSNumber numberWithInt:highlight.code]];

    NSInteger rowId = -1;
    if ([s next]) {
        rowId = [s intForColumnIndex:0];
    }
    
    [db close];
    
    return rowId;
}

+ (BOOL)checkOverlapHighlightsInVolume:(NSInteger)volume page:(NSInteger)page code:(LanguageCode)code range:(NSRange)range
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return YES;
    
    BOOL overlap = NO;
    FMResultSet *s = [db executeQuery:@"SELECT *, rowid FROM highlight WHERE volume=? AND page=? AND code=? AND ((start < ? AND end > ?) OR (start < ? AND end > ?) OR (start > ? AND end < ?))", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page], [NSNumber numberWithInt:code], [NSNumber numberWithInteger:range.location], [NSNumber numberWithInteger:range.location], [NSNumber numberWithInteger:range.location+range.length], [NSNumber numberWithInteger:range.location+range.length], [NSNumber numberWithInteger:range.location], [NSNumber numberWithInteger:range.location+range.length]];
    
    overlap = [s next];
    
    [db close];
    
    return overlap;
}

+ (BOOL)checkOverlapHighlights:(Highlight *)highlight
{
    return [UserDatabaseHelper checkOverlapHighlightsInVolume:highlight.volume page:highlight.page code:highlight.code range:highlight.range];
}

+ (void)deleteHighlight:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"DELETE FROM highlight WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_DB_PATH];
    
    for (Tag *tag in [UserDatabaseHelper queryTags]) {
        if ([tag.highlight containsObject:[NSNumber numberWithInteger:rowId]]) {
            [tag.highlight removeObject:[NSNumber numberWithInteger:rowId]];
            [UserDatabaseHelper updateTagItems:@"highlight" items:tag.highlight ofTag:tag.name withCode:tag.code];
        }
    }
}

+ (NSArray *)queryHighlights
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlight ORDER BY volume ASC, page ASC"];
    while ([s next]) {
        Highlight *highlight = [[Highlight alloc] initFromResultSet:s];
        [results addObject:highlight];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryHighlightsByKeyword:(NSString *)keyword code:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlight WHERE (selection GLOB ? OR note GLOB ?) AND code = ? ORDER BY volume ASC, page ASC", [NSString stringWithFormat:@"*%@*", keyword], [NSString stringWithFormat:@"*%@*", keyword], [NSNumber numberWithInt:code]];
    while ([s next]) {
        Highlight *highlight = [[Highlight alloc] initFromResultSet:s];
        [results addObject:highlight];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryHighlightsByKeywordOfSelection:(NSString *)keyword code:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;
    
    NSMutableArray *results = [[NSMutableArray alloc] init];

    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlight WHERE selection GLOB ? AND code = ? ORDER BY volume ASC, page ASC", [NSString stringWithFormat:@"*%@*", keyword], [NSNumber numberWithInt:code]];
    while ([s next]) {
        Highlight *highlight = [[Highlight alloc] initFromResultSet:s];
        [results addObject:highlight];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryHighlightsByKeywordOfNote:(NSString *)keyword code:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlight WHERE note GLOB ? AND code = ? ORDER BY volume ASC, page ASC, start ASC", [NSString stringWithFormat:@"*%@*", keyword], [NSNumber numberWithInt:code]];
    while ([s next]) {
        Highlight *highlight = [[Highlight alloc] initFromResultSet:s];
        [results addObject:highlight];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryHighlightsOfCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlight WHERE code = ? ORDER BY volume ASC, page ASC, start ASC", [NSNumber numberWithInt:code]];
    
    while ([s next]) {
        Highlight *highlight = [[Highlight alloc] initFromResultSet:s];
        [results addObject:highlight];
    }
    
    [db close];
    
    return results;
}

+ (NSString *)convertArrayParameter:(NSArray *)array
{
    NSMutableArray *sArray = [NSMutableArray array];
    for (id item in array) {
        [sArray addObject:[NSString stringWithFormat:@"%@", item]];
    }
    NSString *ret =[sArray componentsJoinedByString:@","];
    ret = [NSString stringWithFormat:@"(%@)", ret];
    return ret;
}

+ (NSMutableArray *)queryHighlights:(NSArray *)rowIds withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    NSString *statement = [NSString stringWithFormat:@"SELECT *, ROWID FROM highlight WHERE code = ? AND ROWID IN %@", [UserDatabaseHelper convertArrayParameter:rowIds]];
    
    FMResultSet *s = [db executeQuery:statement, [NSNumber numberWithInt:code]];
    
    while ([s next]) {
        Highlight *highlight = [[Highlight alloc] initFromResultSet:s];
        [results addObject:highlight];
    }
    
    [db close];
    
    [results sortUsingComparator:^NSComparisonResult(Highlight *h1, Highlight *h2) {
        NSNumber *idx1 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:h1.rowId]]];
        NSNumber *idx2 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:h2.rowId]]];
        return [idx1 compare:idx2];
    }];
    
    return results;
}


+ (NSArray *)queryHighlightsInVolume:(NSInteger)volume page:(NSInteger)page code:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;

    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlight WHERE volume=? AND page=? AND code=? ORDER BY start", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page], [NSNumber numberWithInt:code]];
    while ([s next]) {
        Highlight *highlight = [[Highlight alloc] initFromResultSet:s];
        [results addObject:highlight];
    }
    
    [db close];
    
    return results;
}

+ (Highlight *)getHighlightByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];
    
    if (![db open]) return nil;

    FMResultSet *s = [db executeQuery:@"SELECT *, ROWID FROM highlight WHERE ROWID=?", [NSNumber numberWithInteger:rowId]];
    
    Highlight *highlight = nil;
    while ([s next]) {
        highlight = [[Highlight alloc] initFromResultSet:s];
    }
    
    [db close];
    
    return highlight;
}

+ (void)saveSearchResults:(NSArray *)results ofKeywords:(NSString *)keywords withCode:(LanguageCode)code
{
    [UserDatabaseHelper saveSearchResults:results ofKeywords:keywords ofType:SearchTypeAll withCode:code totalSearch:NO];
}

+ (void)saveSearchResults:(NSArray *)results ofKeywords:(NSString *)keywords ofType:(SearchType)type withCode:(LanguageCode)code totalSearch:(BOOL)totalSearch
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM history WHERE keywords LIKE ? AND code = ? AND type = ?", keywords, [NSNumber numberWithInt:code], [NSNumber numberWithInt:type]];
    
    if (![s next]) {
        NSString *items = @"";
        NSInteger section1 = 0, section2 = 0, section3 = 0;
        for (NSDictionary *result in results) {
            items = [items stringByAppendingString:[NSString stringWithFormat:@"%@:%@ ", result[@"volume"], result[@"page"]]];
            switch ([BookDatabaseHelper sectionOfVolume:[result[@"volume"] intValue] withCode:code]) {
                case 1:
                    section1 += 1;
                    break;
                case 2:
                    section2 += 1;
                    break;
                case 3:
                    section3 += 1;
                    break;
            }
        }
        NSString *detail = [UserDatabaseHelper detailByCode:code count:results.count section1:section1 section2:section2 section3:section3];
        items = [items stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [db executeUpdate:@"INSERT INTO history VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", keywords, [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]], detail, [NSNumber numberWithInt:code], [NSNumber numberWithBool:NO], @0, @"", @"", items, @"", [NSNumber numberWithInt:type], @"", @YES, @"", @0, @0, [NSNumber numberWithBool:totalSearch]];        
    }
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (NSString *)detailByCode:(LanguageCode)code count:(NSInteger)count section1:(NSInteger)section1 section2:(NSInteger)section2 section3:(NSInteger)section3
{
    return [UserDatabaseHelper isTipitaka:code] ? [[NSString stringWithFormat:DETAIL_TEMPLATE, count, section1, section2, section3] stringByReplacingThaiNumeric] : [[NSString stringWithFormat:DETAIL2_TEMPLATE, count] stringByReplacingThaiNumeric];
}

+ (BOOL)isTipitaka:(LanguageCode)code
{
    return code != LanguageCodeThaiFiveBooks && code != LanguageCodeThaiPocketBook && code != LanguageCodeThaiWatna;
}

+ (void)addHistory:(History *)history items:(NSArray *)items skimmedItems:(NSArray *)skimmedItems readItems:(NSArray *)readItems markedItems:(NSArray *)markedItems created:(NSDate *)created
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) return;
    
    
    NSLog(@"%@ : %d", history.keywords, history.code);
    FMResultSet *s = [db executeQuery:@"SELECT * FROM history WHERE keywords LIKE ? AND code = ?", history.keywords, [NSNumber numberWithInt:history.code]];
    if (![s next]) {
        NSInteger section1 = 0, section2 = 0, section3 = 0;
        if (!history.detail.length && items.count) {
            if ([UserDatabaseHelper isTipitaka:history.code]) {
                for (NSString *token in items) {
                    NSInteger volume = [[token componentsSeparatedByString:@":"][0] integerValue];
                    switch ([BookDatabaseHelper sectionOfVolume:volume withCode:history.code]) {
                        case 1:
                            section1 += 1;
                            break;
                        case 2:
                            section2 += 1;
                            break;
                        case 3:
                            section3 += 1;
                            break;
                    }
                }
            }
            history.detail = [UserDatabaseHelper detailByCode:history.code count:items.count section1:section1 section2:section2 section3:section3];
        }
        NSLog(@"insert");
        [db executeUpdate:@"INSERT INTO history VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", history.keywords, [NSNumber numberWithDouble:[created timeIntervalSince1970]], history.detail, [NSNumber numberWithInt:history.code], [NSNumber numberWithBool:history.starred], [NSNumber numberWithInteger:history.state], [readItems componentsJoinedByString:@" "], [skimmedItems componentsJoinedByString:@" "], [items componentsJoinedByString:@" "], [markedItems componentsJoinedByString:@" "], [NSNumber numberWithInteger:history.type], @"", @YES, @"", @0, @0];
    }
    
    [db close];
        
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (NSArray *)queryBookmarks
{
    FMDatabase *db = [UserDatabaseHelper bookmarkDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s;
    s = [db executeQuery:@"SELECT ROWID, note, important, volume, page, code, created FROM bookmark"];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        Bookmark *bookmark = [[Bookmark alloc] init];
        bookmark.rowId = [s intForColumnIndex:0];
        bookmark.note = [s stringForColumnIndex:1];
        bookmark.starred = [s boolForColumnIndex:2];
        bookmark.volume = [s intForColumnIndex:3];
        bookmark.page = [s intForColumnIndex:4];
        bookmark.code = [s intForColumnIndex:5];
        bookmark.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:6]];
        [results addObject:bookmark];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryBookmarksInRowIds:(NSArray *)rowIds withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper bookmarkDatabase];
    
    if (![db open]) { return nil; }

    
    NSString *statement = [NSString stringWithFormat:@"SELECT ROWID, note, important, volume, page, created FROM bookmark WHERE ROWID IN %@ AND code = ?", [UserDatabaseHelper convertArrayParameter:rowIds]];
    FMResultSet *s = [db executeQuery:statement, [NSNumber numberWithInt:code]];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        Bookmark *bookmark = [[Bookmark alloc] init];
        bookmark.rowId = [s intForColumnIndex:0];
        bookmark.note = [s stringForColumnIndex:1];
        bookmark.starred = [s boolForColumnIndex:2];
        bookmark.volume = [s intForColumnIndex:3];
        bookmark.page = [s intForColumnIndex:4];
        bookmark.code = code;
        bookmark.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        [results addObject:bookmark];
    }
    
    [db close];

    [results sortUsingComparator:^NSComparisonResult(Bookmark *b1, Bookmark *b2) {
        NSNumber *idx1 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:b1.rowId]]];
        NSNumber *idx2 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:b2.rowId]]];
        return [idx1 compare:idx2];
    }];
    
    return results;
    
}

+ (NSArray *)queryBookmarks:(NSString *)note withCode:(LanguageCode)code ofUser:(OtherUser *)user
{
    FMDatabase *db;
    if (!user) {
        db = [UserDatabaseHelper bookmarkDatabase];
    } else {
        db = [UserDatabaseHelper bookmarkDatabaseOfUser:user];
    }
    
    if (![db open]) { return nil; }
    
    FMResultSet *s;
    if (note.length > 0) {
        s = [db executeQuery:@"SELECT ROWID, note, important, volume, page, created FROM bookmark WHERE note GLOB ? AND code = ?", [NSString stringWithFormat:@"*%@*", note], [NSNumber numberWithInt:code]];
    } else {
        s = [db executeQuery:@"SELECT ROWID, note, important, volume, page, created FROM bookmark WHERE code = ?", [NSNumber numberWithInt:code]];
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        Bookmark *bookmark = [[Bookmark alloc] init];
        bookmark.rowId = [s intForColumnIndex:0];
        bookmark.note = [s stringForColumnIndex:1];
        bookmark.starred = [s boolForColumnIndex:2];
        bookmark.volume = [s intForColumnIndex:3];
        bookmark.page = [s intForColumnIndex:4];
        bookmark.code = code;
        bookmark.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        [results addObject:bookmark];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryBookmarks:(NSString *)note withCode:(LanguageCode)code
{
    return [UserDatabaseHelper queryBookmarks:note withCode:code ofUser:nil];
}

+ (void)clearHistories
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"DELETE FROM history"];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (NSArray *)queryHistories
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s;

    s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, code, created, type, note, buddhawaj, note_state, priority FROM history"];

    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        History *history = [[History alloc] init];
        history.rowId = [s intForColumnIndex:0];
        history.keywords = [s stringForColumnIndex:1];
        history.state = [s intForColumnIndex:2];
        history.starred = [s boolForColumnIndex:3];
        history.detail = [s stringForColumnIndex:4];
        history.code = [s intForColumnIndex:5];
        history.type = [s intForColumnIndex:7];
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:6]];
        history.note = [s stringForColumnIndex:8];
        history.buddhawaj = [s boolForColumnIndex:9];
        history.noteState = [s intForColumnIndex:10];
        history.priority = [s intForColumnIndex:11];
        [results addObject:history];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryHistories:(NSString *)keywords ofUser:(OtherUser *)user
{
    FMDatabase *db;
    if (!user) {
        db = [UserDatabaseHelper historyDatabase];
    } else {
        db = [UserDatabaseHelper historyDatabaseOfUser:user];
    }
    
    if (![db open]) { return nil; }
    
    FMResultSet *s;
    if (keywords.length > 0) {
        s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, created, type, code, note, buddhawaj, note_state, priority FROM history WHERE keywords GLOB ?", [NSString stringWithFormat:@"*%@*", keywords]];
    } else {
        s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, created, type, code, note, buddhawaj, note_state, priority FROM history"];
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        History *history = [[History alloc] init];
        history.rowId = [s intForColumnIndex:0];
        history.keywords = [s stringForColumnIndex:1];
        history.state = [s intForColumnIndex:2];
        history.starred = [s boolForColumnIndex:3];
        history.detail = [s stringForColumnIndex:4];
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        history.type = [s intForColumnIndex:6];
        history.code = [s intForColumnIndex:7];
        history.note = [s stringForColumnIndex:8];
        history.buddhawaj = [s boolForColumnIndex:9];
        history.noteState = [s intForColumnIndex:10];
        history.priority = [s intForColumnIndex:11];
        [results addObject:history];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryHistories:(NSString *)keywords
{
    return [UserDatabaseHelper queryHistories:keywords ofUser:nil];
}

+ (NSArray *)queryHistoriesInRowIds:(NSArray *)rowIds withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    NSString *statement = [NSString stringWithFormat:@"SELECT ROWID, keywords, state, starred, detail, created, type, note, buddhawaj, note_state, priority FROM history WHERE code = ? AND ROWID in %@", [UserDatabaseHelper convertArrayParameter:rowIds]];
    
    FMResultSet *s = [db executeQuery:statement, [NSNumber numberWithInt:code]];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        History *history = [[History alloc] init];
        history.rowId = [s intForColumnIndex:0];
        history.keywords = [s stringForColumnIndex:1];
        history.state = [s intForColumnIndex:2];
        history.starred = [s boolForColumnIndex:3];
        history.detail = [s stringForColumnIndex:4];
        history.type = [s intForColumnIndex:6];
        history.code = code;
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        history.note = [s stringForColumnIndex:7];
        history.buddhawaj = [s boolForColumnIndex:8];
        history.noteState = [s intForColumnIndex:9];
        history.priority = [s intForColumnIndex:10];
        [results addObject:history];
    }
    
    [db close];
    
    [results sortUsingComparator:^NSComparisonResult(History *h1, History *h2) {
        NSNumber *idx1 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:h1.rowId]]];
        NSNumber *idx2 = [NSNumber numberWithInteger:[rowIds indexOfObject:[NSNumber numberWithInteger:h2.rowId]]];
        return [idx1 compare:idx2];
    }];
    
    return results;
}

+ (NSArray *)queryHistories:(NSString *)keywords withCode:(LanguageCode)code ofUser:(OtherUser *)user
{
    FMDatabase *db;
    if (!user) {
        db = [UserDatabaseHelper historyDatabase];
    } else {
        db = [UserDatabaseHelper historyDatabaseOfUser:user];
    }
    
    if (![db open]) { return nil; }
    
    FMResultSet *s;
    if (keywords.length > 0) {
        s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, created, type, note, buddhawaj, note_state, priority FROM history WHERE keywords GLOB ? AND code = ?", [NSString stringWithFormat:@"*%@*", keywords], [NSNumber numberWithInt:code]];
    } else {
        s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, created, type, note, buddhawaj, note_state, priority FROM history WHERE code = ?", [NSNumber numberWithInt:code]];
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        History *history = [[History alloc] init];
        history.rowId = [s intForColumnIndex:0];
        history.keywords = [s stringForColumnIndex:1];
        history.state = [s intForColumnIndex:2];
        history.starred = [s boolForColumnIndex:3];
        history.detail = [s stringForColumnIndex:4];
        history.type = [s intForColumnIndex:6];
        history.code = code;
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        history.note = [s stringForColumnIndex:7];
        history.buddhawaj = [s boolForColumnIndex:8];
        history.noteState = [s intForColumnIndex:9];
        history.priority = [s intForColumnIndex:10];
        [results addObject:history];
    }
    
    [db close];
    
    return results;
}

+ (NSArray *)queryHistories:(NSString *)keywords withCode:(LanguageCode)code
{
    return [UserDatabaseHelper queryHistories:keywords withCode:code ofUser:nil];
}

+ (History *)getHistory:(NSString *)keywords ofType:(SearchType)type withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, created, note, buddhawaj, note_state, priority FROM history WHERE keywords LIKE ? AND code = ? AND type = ?", keywords, [NSNumber numberWithInt:code], [NSNumber numberWithInt:type]];
    
    History *history;
    if ([s next]) {
        history = [[History alloc] init];
        history.rowId = [s intForColumnIndex:0];
        history.keywords = [s stringForColumnIndex:1];
        history.state = [s intForColumnIndex:2];
        history.starred = [s boolForColumnIndex:3];
        history.detail = [s stringForColumnIndex:4];
        history.type = type;
        history.code = code;
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        history.note = [s stringForColumnIndex:6];
        history.buddhawaj = [s boolForColumnIndex:7];
        history.noteState = [s intForColumnIndex:8];
        history.priority = [s intForColumnIndex:9];
    }
    [db close];
    
    return history;
}

+ (History *)getHistoryByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }

    FMResultSet *s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, created, type, code, note, buddhawaj, note_state, priority FROM history WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    History *history;
    if ([s next]) {
        history = [[History alloc] init];
        history.rowId = [s intForColumnIndex:0];
        history.keywords = [s stringForColumnIndex:1];
        history.state = [s intForColumnIndex:2];
        history.starred = [s boolForColumnIndex:3];
        history.detail = [s stringForColumnIndex:4];
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        history.type = [s intForColumnIndex:6];
        history.code = [s intForColumnIndex:7];
        history.note = [s stringForColumnIndex:8];
        history.buddhawaj = [s boolForColumnIndex:9];
        history.noteState = [s intForColumnIndex:10];
        history.priority = [s intForColumnIndex:11];
    }
    
    [db close];
    
    return history;

}

+ (History *)getHistory:(NSString *)keywords withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID, keywords, state, starred, detail, created, type, note, buddhawaj, note_state, priority FROM history WHERE keywords LIKE ? AND code = ?", keywords, [NSNumber numberWithInt:code]];
    
    History *history;
    if ([s next]) {
        history = [[History alloc] init];
        history.rowId = [s intForColumnIndex:0];
        history.keywords = [s stringForColumnIndex:1];
        history.state = [s intForColumnIndex:2];
        history.starred = [s boolForColumnIndex:3];
        history.detail = [s stringForColumnIndex:4];
        history.type = [s intForColumnIndex:6];
        history.code = code;
        history.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:5]];
        history.note = [s stringForColumnIndex:7];
        history.buddhawaj = [s boolForColumnIndex:8];
        history.noteState = [s intForColumnIndex:9];
        history.priority = [s intForColumnIndex:10];
    }
    [db close];
    
    return history;
}

+ (NSDictionary *)historyDataByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s = [db executeQuery:@"SELECT items, read, skimmed, marked, note_items, search_all FROM history WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    NSDictionary *result = nil;
    
    if ([s next]) {
        
        result = @{@"items":[s stringForColumnIndex:0],
                   @"read":[s stringForColumnIndex:1],
                   @"skimmed":[s stringForColumnIndex:2],
                   @"marked":[s stringForColumnIndex:3] ? [s stringForColumnIndex:3] : @"",
                   @"note_items":[s stringForColumnIndex:4] ? [s stringForColumnIndex:4] : @"",
                   @"search_all":[NSNumber numberWithInt:[s intForColumnIndex:5]]
        };
    }
    
    [db close];
    
    return result;
    
}

+ (NSArray *)historyItemsById:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }

    FMResultSet *s = [db executeQuery:@"SELECT items FROM history WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    if ([s next] && [s stringForColumnIndex:0].length) {
        for (NSString *token in [[s stringForColumnIndex:0] componentsSeparatedByString:@" "]) {
            [items addObject:@{@"volume":[NSNumber numberWithInteger:[[token componentsSeparatedByString:@":"][0] integerValue]], @"page":[NSNumber numberWithInteger:[[token componentsSeparatedByString:@":"][1] integerValue]]}];
        }
    }
    
    [db close];
    
    return items;
}

+ (void)addHistoryReadItem:(NSInteger)index rowId:(NSInteger)rowId
{
    NSMutableSet *items = [[NSMutableSet alloc] initWithArray:[UserDatabaseHelper getHistoryReadItemsByRowId:rowId]];
    [items addObject:[NSNumber numberWithInteger:index]];

    NSString *text = @"";
    for (NSNumber *item in items) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@ ", item]];
    }
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"UPDATE history SET read=? WHERE ROWID = ?", text, [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (void)addHistorySkimmedItem:(NSInteger)index rowId:(NSInteger)rowId
{
    NSMutableSet *items = [[NSMutableSet alloc] initWithArray:[UserDatabaseHelper getHistorySkimmedItemsByRowId:rowId]];
    [items addObject:[NSNumber numberWithInteger:index]];
    
    NSString *text = @"";
    for (NSNumber *item in items) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@ ", item]];
    }
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"UPDATE history SET skimmed=? WHERE ROWID = ?", text, [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (void)addHistoryMarkedItem:(NSInteger)index rowId:(NSInteger)rowId
{
    NSMutableSet *items = [[NSMutableSet alloc] initWithArray:[UserDatabaseHelper getHistoryMarkedItemsByRowId:rowId]];
    [items addObject:[NSNumber numberWithInteger:index]];
    
    NSString *text = @"";
    for (NSNumber *item in items) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@ ", item]];
    }
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"UPDATE history SET marked=? WHERE ROWID = ?", text, [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (void)updateHistoryNoteItems:(NSInteger)index note:(NSString *)note state:(NSUInteger)state rowId:(NSInteger)rowId
{
    NSMutableDictionary *noteItems = [UserDatabaseHelper getHistoryNoteItemsByRowId:rowId];
    NoteItem *noteItem = [[NoteItem alloc] initWithNote:note state:state];
    [noteItems setObject:noteItem forKey:[NSNumber numberWithInteger:index]];
    
    NSMutableArray *tmp = [[NSMutableArray alloc] init];
    for (NSNumber *key in noteItems) {
        NoteItem *noteItem = [noteItems objectForKey:key];
        [tmp addObject:[NSString stringWithFormat:@"%@|%@|%d", key, noteItem.note, noteItem.state]];
    }
    NSString *result = [tmp componentsJoinedByString:@"~"];
    NSLog(@"%@", result);

    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"UPDATE history SET note_items=? WHERE ROWID = ?", result, [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (NSArray *)getHistoryItems:(NSString *)name byRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s = [db executeQuery:[NSString stringWithFormat:@"SELECT %@ FROM history WHERE ROWID = ?", name], [NSNumber numberWithInteger:rowId]];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    if ([s next] && [s stringForColumnIndex:0].length) {
        for (NSString *item in [[s stringForColumnIndex:0] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]) {
            NSNumber *index = [NSNumber numberWithInteger:[item integerValue]];
            if (![items containsObject:index]) {
                [items addObject:[NSNumber numberWithInteger:[item integerValue]]];
            }
        }
    }
    
    [db close];
    
    return items;
}

+ (NSArray *)getHistoryReadItemsByRowId:(NSInteger)rowId
{
    return [self getHistoryItems:@"read" byRowId:rowId];
}

+ (NSArray *)getHistorySkimmedItemsByRowId:(NSInteger)rowId
{
    return [self getHistoryItems:@"skimmed" byRowId:rowId];
}

+ (NSArray *)getHistoryMarkedItemsByRowId:(NSInteger)rowId
{
    return [self getHistoryItems:@"marked" byRowId:rowId];
}

+ (NSMutableDictionary *)getHistoryNoteItemsByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    FMResultSet *s = [db executeQuery:@"SELECT note_items FROM history WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    if ([s next] && [s stringForColumnIndex:0].length) {
        for (NSString *token in [[s stringForColumnIndex:0] componentsSeparatedByString:@"~"]) {
            NSArray *items = [token componentsSeparatedByString:@"|"];
            NoteItem *noteItem = [[NoteItem alloc] initWithNote:items[1] state:[items[2] integerValue]];
            [result setObject:noteItem forKey:[NSNumber numberWithInteger:[items[0] integerValue]]];
        }
    }
    
    [db close];
    
    return result;
}

+ (void)updateHistoryReadItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page
{
    NSInteger index = 0;
    for (NSDictionary *item in [UserDatabaseHelper historyItemsById:rowId]) {
        if ([item[@"volume"] integerValue] == volume && [item[@"page"] integerValue] == page) {
            [UserDatabaseHelper addHistoryReadItem:index rowId:rowId];
            break;
        }
        index += 1;
    }
}

+ (void)updateHistorySkimmedItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page
{
    NSInteger index = 0;
    for (NSDictionary *item in [UserDatabaseHelper historyItemsById:rowId]) {
        if ([item[@"volume"] integerValue] == volume && [item[@"page"] integerValue] == page) {
            [UserDatabaseHelper addHistorySkimmedItem:index rowId:rowId];
            break;
        }
        index += 1;
    }    
}

+ (void)updateHistoryMarkedItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page
{
    NSInteger index = 0;
    for (NSDictionary *item in [UserDatabaseHelper historyItemsById:rowId]) {
        if ([item[@"volume"] integerValue] == volume && [item[@"page"] integerValue] == page) {
            [UserDatabaseHelper addHistoryMarkedItem:index rowId:rowId];
            break;
        }
        index += 1;
    }
}

+ (void)updateHistoryNote:(NSInteger)rowId note:(NSString *)note state:(NSUInteger)state
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"UPDATE history SET note=?, note_state=? WHERE ROWID = ?", note, [NSNumber numberWithInteger:state], [NSNumber numberWithInteger:rowId]];
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (void)deleteHistoryMarkedItem:(NSInteger)rowId volume:(NSInteger)volume page:(NSInteger)page
{
    NSMutableSet *items = [[NSMutableSet alloc] initWithArray:[UserDatabaseHelper getHistoryMarkedItemsByRowId:rowId]];
    
    NSInteger index = 0;
    for (NSDictionary *item in [UserDatabaseHelper historyItemsById:rowId]) {
        if ([item[@"volume"] integerValue] == volume && [item[@"page"] integerValue] == page) {
            [items removeObject:[NSNumber numberWithInteger:index]];
            break;
        }
        index += 1;
    }

    NSString *text = @"";
    for (NSNumber *item in items) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"%@ ", item]];
    }
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"UPDATE history SET marked=? WHERE ROWID = ?", text, [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (NSArray *)queryNotes:(NSString *)note
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s;
    if (note.length > 0) {
        s = [db executeQuery:@"SELECT ROWID, note, important, created, main FROM note WHERE note GLOB ?", [NSString stringWithFormat:@"*%@*", note]];
    } else {
        s = [db executeQuery:@"SELECT ROWID, note, important, created, main FROM note"];
    }
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        WriteNote *note = [[WriteNote alloc] init];
        note.rowId = [s intForColumnIndex:0];
        note.note = [s stringForColumnIndex:1];
        note.starred = [s boolForColumnIndex:2];
        note.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:3]];
        note.main = [s boolForColumnIndex:4];
        [results addObject:note];
    }
    
    [db close];
    
    return results;
}

+ (WriteNote *)getMainWriteNote
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    if (![db open]) { return nil; }
    
    WriteNote *writeNote = nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID, note, important, rank, created FROM note WHERE main = ?", @YES];
    
    if ([s next]) {
        writeNote = [[WriteNote alloc] init];
        writeNote.rowId = [s intForColumnIndex:0];
        writeNote.note = [s stringForColumnIndex:1];
        writeNote.starred = [s boolForColumnIndex:2];
        writeNote.rank = [s intForColumnIndex:3];
        writeNote.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:4]];
    }
    
    [db close];
    return writeNote;
}

+ (WriteNote *)getWriteNote:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    if (![db open]) { return nil; }
    
    WriteNote *writeNote = nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT note, important, rank, created FROM note WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    if ([s next]) {
        writeNote = [[WriteNote alloc] init];
        writeNote.rowId = rowId;
        writeNote.note = [s stringForColumnIndex:0];
        writeNote.starred = [s boolForColumnIndex:1];
        writeNote.rank = [s intForColumnIndex:2];
        writeNote.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:3]];
    }
    
    [db close];
    return writeNote;
}

+ (NSInteger)saveWriteNote:(NSString *)text starred:(BOOL)starred created:(NSDate *)created main:(BOOL)main
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    
    if (![db open]) { return -1; }
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID FROM note WHERE note=? AND main=?", text, [NSNumber numberWithBool:main]];
    NSInteger rowId = -1;
    
    if ([s next]) {
        rowId = [s intForColumnIndex:0];        
        [db executeUpdate:@"UPDATE note SET important=?,created=? WHERE ROWID = ?", [NSNumber numberWithBool:starred],[NSNumber numberWithDouble:[created timeIntervalSince1970]],[NSNumber numberWithInteger:rowId]];
    } else {
        [db executeUpdate:@"INSERT INTO note VALUES (?,?,?,?,?)", text, [NSNumber numberWithBool:starred],[NSNumber numberWithDouble:[created timeIntervalSince1970]], @0, [NSNumber numberWithBool:main]];
        rowId = [db lastInsertRowId];
    }
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:NOTE_DB_PATH];
    
    return rowId;
}

+ (void)updateWirteNote:(NSString *)text starred:(BOOL)starred rowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    
    if (![db open]) { return; }
    NSDate *edited = [NSDate date];
    [db executeUpdate:@"UPDATE note SET note=?,important=?,created=? WHERE ROWID = ?", text, [NSNumber numberWithBool:starred], [NSNumber numberWithDouble:[edited timeIntervalSince1970]], [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:NOTE_DB_PATH];
}

+ (void)updateWirteNote:(BOOL)starred rowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    
    if (![db open]) { return; }
    
    NSDate *edited = [NSDate date];
    [db executeUpdate:@"UPDATE note SET important=?,created=? WHERE ROWID = ?", [NSNumber numberWithBool:starred], [NSNumber numberWithDouble:[edited timeIntervalSince1970]], [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:NOTE_DB_PATH];
}

+ (void)deleteWriteNote:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper noteDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"DELETE FROM note WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:NOTE_DB_PATH];
}

+ (void)saveBookmark:(NSString *)text volume:(NSInteger)volume page:(NSInteger)page starred:(BOOL)starred created:(NSDate *)created append:(BOOL)append withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper bookmarkDatabase];
    
    if (![db open]) { return; }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM bookmark WHERE volume=? AND page=? AND code=?", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page], [NSNumber numberWithInt:code]];
    
    if ([s next]) {
        text = append ? [NSString stringWithFormat:@"%@\n\n%@", [s stringForColumn:@"note"], text] : text;
        [db executeUpdate:@"UPDATE bookmark SET note=?,important=?,created=? WHERE volume=? AND page=? AND code=?", text, [NSNumber numberWithBool:starred], [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]], [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page], [NSNumber numberWithInt:code]];
    } else {
        [db executeUpdate:@"INSERT INTO bookmark VALUES (?, ?, ?, ?, ?, ?, ?)", [NSNumber numberWithDouble:[created timeIntervalSince1970]], [NSNumber numberWithBool:starred], text, @0, [NSNumber numberWithInt:code], [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page]];
    }
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:BOOKMARK_DB_PATH];
}

+ (void)saveBookmark:(NSString *)text volume:(NSInteger)volume page:(NSInteger)page starred:(BOOL)starred withCode:(LanguageCode)code
{
    if (text.length) {
        [UserDatabaseHelper saveBookmark:text volume:volume page:page starred:starred created:[NSDate date] append:NO withCode:code];
    } else {
        Bookmark *bookmark = [UserDatabaseHelper getBookmarkByVolume:volume page:page withCode:code];
        [UserDatabaseHelper deleteBookmarkByRowId:bookmark.rowId];
    }
    
}

+ (Bookmark *)getBookmarkByVolume:(NSInteger)volume page:(NSInteger)page withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper bookmarkDatabase];
    
    if (![db open]) { return nil; }

    Bookmark *bookmark = nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID, note, important, rank, created FROM bookmark WHERE volume=? AND page=? AND code=?", [NSNumber numberWithInteger:volume], [NSNumber numberWithInteger:page], [NSNumber numberWithInt:code]];
    
    if ([s next]) {
        bookmark = [[Bookmark alloc] init];
        bookmark.volume = volume;
        bookmark.page = page;
        bookmark.code = code;
        bookmark.rowId = [s intForColumnIndex:0];
        bookmark.note = [s stringForColumnIndex:1];
        bookmark.starred = [s boolForColumnIndex:2];
        bookmark.rank = [s intForColumnIndex:3];
        bookmark.created = [NSDate dateWithTimeIntervalSince1970:[s doubleForColumnIndex:4]];
    }
    
    [db close];
    
    return bookmark;
}

+ (void)updateBookmarkAtColumn:(NSString *)column setValue:(id)value byRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper bookmarkDatabase];

    if (![db open]) { return; }

    NSDate *edited = [NSDate date];
    [db executeUpdate:[NSString stringWithFormat:@"UPDATE bookmark SET %@=?,created=? WHERE ROWID = ?", column], value, [NSNumber numberWithDouble:[edited timeIntervalSince1970]],  [NSNumber numberWithInteger:rowId]];
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:BOOKMARK_DB_PATH];
}

+ (void)updateHistoryAtColumn:(NSString *)column setValue:(id)value byRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:[NSString stringWithFormat:@"UPDATE history SET %@=? WHERE ROWID = ?", column], value, [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
}

+ (void)deleteHistoryByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"DELETE FROM history WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
    
    for (Tag *tag in [UserDatabaseHelper queryTags]) {
        if ([tag.history containsObject:[NSNumber numberWithInteger:rowId]]) {
            [tag.history removeObject:[NSNumber numberWithInteger:rowId]];
            [UserDatabaseHelper updateTagItems:@"history" items:tag.history ofTag:tag.name withCode:tag.code];
        }
    }
}

+ (void)deleteBookmarkByRowId:(NSInteger)rowId
{
    FMDatabase *db = [UserDatabaseHelper bookmarkDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:@"DELETE FROM bookmark WHERE ROWID = ?", [NSNumber numberWithInteger:rowId]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:BOOKMARK_DB_PATH];
    
    for (Tag *tag in [UserDatabaseHelper queryTags]) {
        if ([tag.note containsObject:[NSNumber numberWithInteger:rowId]]) {
            [tag.note removeObject:[NSNumber numberWithInteger:rowId]];
            [UserDatabaseHelper updateTagItems:@"note" items:tag.note ofTag:tag.name withCode:tag.code];
        }
    }
}

+ (NSDictionary *)getHistorySelectionItems:(NSDictionary *)history
{
    NSArray *contents = [history[@"contents"] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSArray *array1 = [obj1 componentsSeparatedByString:@":"];
        NSArray *array2 = [obj2 componentsSeparatedByString:@":"];
        NSNumber *v1 = [NSNumber numberWithInteger:[array1[0] integerValue]];
        NSNumber *p1 = [NSNumber numberWithInteger:[array1[1] integerValue]];
        NSNumber *v2 = [NSNumber numberWithInteger:[array2[0] integerValue]];
        NSNumber *p2 = [NSNumber numberWithInteger:[array2[1] integerValue]];
        return [v1 compare:v2] != NSOrderedSame ? [v1 compare:v2] : [p1 compare:p2];
    }];
    
    NSInteger section = 0;
    NSInteger row = 0;
    NSInteger prevPart = 0;
    NSMutableArray *newContents = [[NSMutableArray alloc] init];
    for (NSString *item in contents) {
        NSArray *array = [item componentsSeparatedByString:@":"];
        NSInteger volume = [array[0] integerValue];
        NSInteger nowPart = 3;
        if (volume <= 8) {
            nowPart = 1;
        } else if (volume <= 33) {
            nowPart = 2;
        }
        if (prevPart != nowPart) {
            section += 1;
            row = 0;
        } else {
            row += 1;
        }
        prevPart = nowPart;
        [newContents addObject:[NSString stringWithFormat:@"%ld:%ld", section, row]];
    }
    
    NSArray *selected = [history[@"selected"] map:^id(NSString *item) {
        return [NSNumber numberWithInteger:[newContents indexOfObject:item]];
    }];
    NSArray *read = [history[@"read"] map:^id(NSString *item) {
        return [NSNumber numberWithInteger:[newContents indexOfObject:item]];
    }];
    NSArray *marked = [history[@"marked"] map:^id(NSString *item) {
        return [NSNumber numberWithInteger:[newContents indexOfObject:item]];
    }];
    
    return @{@"readItems":selected, @"skimmedItems":read, @"marked":marked};
}

+ (void)importOldHistories:(NSArray *)histories
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"th_TH"]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];

    NSInteger count = 0;
    for (NSDictionary *history in histories) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
            [SVProgressHUD showProgress:(count*1.0)/histories.count status:NSLocalizedString(@"Importing histories", nil)];
        });
        NSArray *contents = history[@"contents"];
        if (contents.count) {
            NSDictionary *dict = [UserDatabaseHelper getHistorySelectionItems:history];
            History *h = [[History alloc] init];
            h.code = [UserDatabaseHelper languageCodeByCodeString:history[@"lang"]];
            h.keywords = history[@"keywords"];
            h.state = [history[@"state"] integerValue];
            h.starred = [history[@"star"] boolValue];
            h.detail = history[@"detail"];
            [UserDatabaseHelper addHistory:h items:history[@"contents"] skimmedItems:dict[@"skimmedItems"] readItems:dict[@"readItems"] markedItems:@[] created:[dateFormatter dateFromString:history[@"created"]]];
        }
        count += 1;
    }
}

+ (NSString *)userDatabaseJSONString
{
    NSMutableArray *bookmarks = [[NSMutableArray alloc] init];
    for (Bookmark *bookmark in [UserDatabaseHelper queryBookmarks]) {
        [bookmarks addObject:[bookmark dictionaryValue]];
    }
    
    NSMutableArray *searchAllHistories = [[NSMutableArray alloc] init];
    for (SearchAllHistory *history in [UserDatabaseHelper querySearchAllHistories]) {
        [searchAllHistories addObject:[history dictionaryValue]];
    }
    
    NSMutableArray *histories = [[NSMutableArray alloc] init];
    for (History *history in [UserDatabaseHelper queryHistories]) {
        NSDictionary *data = [UserDatabaseHelper historyDataByRowId:history.rowId];
        NSMutableDictionary *dict = [[history dictionaryValue] mutableCopy];
        [dict setObject:data[@"items"] forKey:@"items"];
        [dict setObject:data[@"read"] forKey:@"read"];
        [dict setObject:data[@"skimmed"] forKey:@"skimmed"];
        [dict setObject:(data[@"marked"] ? data[@"marked"] : @[]) forKey:@"marked"];
        [dict setObject:data[@"note_items"] forKey:@"note_items"];
        [dict setObject:data[@"search_all"] forKey:@"search_all"];
        [histories addObject:dict];
    }
    
    NSMutableArray *highlights = [[NSMutableArray alloc] init];
    for (Highlight *highlight in [UserDatabaseHelper queryHighlights]) {
        [highlights addObject:[highlight dictionaryValue]];
    }
    
    NSMutableArray *lexicons = [[NSMutableArray alloc] init];
    for (Lexicon *lexicon in [UserDatabaseHelper querySavedLexicon]) {
        [lexicons addObject:[lexicon dictionaryValue]];
    }
    
    NSMutableArray *tags = [NSMutableArray array];
    for (Tag *tag in [UserDatabaseHelper queryTags]) {
        [tags addObject:[tag dictionaryValue]];
    }
    
    NSMutableArray *readingStates = [NSMutableArray array];
    for (ReadingState *readingState in [UserDatabaseHelper queryReadingStates]) {
        [readingStates addObject:[readingState dictionaryValue]];
    }
    
    NSMutableArray *notes = [NSMutableArray array];
    for (WriteNote *note in [UserDatabaseHelper queryNotes:nil]) {
        [notes addObject:[note dictionaryValue]];
    }
    
    NSDictionary *data = @{@"bookmarks":bookmarks,
                           @"histories":histories,
                           @"highlights":highlights,
                           @"lexicons":lexicons,
                           @"tags":tags,
                           @"readingStates":readingStates,
                           @"notes":notes,
                           @"search_all":searchAllHistories,
                           @"version":@2};
    
    NSError *error = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];

    if (error) {
        NSLog(@"%@", error);
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", jsonString);
    
    return jsonString;
}

+ (void)exportUserDatabase:(NSURL *)url
{
    NSString *jsonString = [UserDatabaseHelper userDatabaseJSONString];
    
    NSError *error = nil;
    [jsonString writeToFile:url.path atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
}

+ (void)importSavedLexicons:(NSArray *)lexicons
{
    for (NSDictionary *lexicon in lexicons) {
        [UserDatabaseHelper importSavedLexicon:lexicon];
    }
}

+ (void)importSavedLexicon:(NSDictionary *)lexiconDict
{
    FMDatabase *db = [UserDatabaseHelper savedLexiconDatabase];
    
    if (![db open]) { return; }
    
    Lexicon *lexicon = [[Lexicon alloc] initWithHead:lexiconDict[@"head"] andTranslation:lexiconDict[@"translation"]];
    [UserDatabaseHelper saveLexicon:lexicon type:[lexiconDict[@"type"] intValue]];
    
    [db close];
}

+ (void)importReadingState:(NSDictionary *)readingStateDict
{
    ReadingState *readingState = [ReadingState new];
    readingState.code = [readingStateDict[@"code"] intValue];;
    readingState.volume = [readingStateDict[@"volume"] integerValue];
    readingState.page = [readingStateDict[@"page"] integerValue];
    readingState.state = [readingStateDict[@"state"] intValue];
    readingState.count = [readingStateDict[@"count"] integerValue];
    [UserDatabaseHelper addReadingState:readingState];
}

+ (void)importReadingStates:(NSArray *)readingStateArray
{
    for (NSDictionary *readingStateDict in readingStateArray) {
        [UserDatabaseHelper importReadingState:readingStateDict];
    }
}

+ (void)importNote:(NSDictionary *)noteDict
{
    NSString *note = noteDict[@"note"];
    BOOL starred = [noteDict[@"important"] boolValue];
    NSDate *created = [NSDate dateWithTimeIntervalSince1970:[noteDict[@"created"] doubleValue]];
    BOOL main = [noteDict[@"main"] boolValue];
    
    [UserDatabaseHelper saveWriteNote:note starred:starred created:created main:main];
}

+ (void)importNotes:(NSArray *)noteArray
{
    for (NSDictionary *noteDict in noteArray) {
        [UserDatabaseHelper importNote:noteDict];
    }
}

+ (NSArray *)importSearchAllHistory:(NSDictionary *)history
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s = [db executeQuery:@"SELECT ROWID FROM search_all WHERE keywords LIKE ?", history[@"keywords"]];
 
    BOOL existingRecord = [s next];
    if (!existingRecord) {
        [db executeUpdate:@"INSERT INTO search_all (keywords,type,detail,state,note,note_state,priority,created) VALUES (?,?,?,?,?,?,?,?)",
         history[@"keywords"],
         history[@"type"],
         history[@"detail"],
         history[@"state"],
         history[@"note"],
         history[@"note_state"],
         history[@"priority"],
         history[@"created"]];
    } else {
        [db executeUpdate:@"UPDATE search_all SET type=?, detail=?, state=?, note=?, note_state=?, priority=?, created=? WHERE keywords=?",
         history[@"type"],
         history[@"detail"],
         history[@"state"],
         history[@"note"],
         history[@"note_state"],
         history[@"priority"],
         history[@"created"],
         history[@"keywords"]];
    }
    
    s = [db executeQuery:@"SELECT ROWID FROM search_all WHERE keywords LIKE ?", history[@"keywords"]];
    
    if ([s next] && history[@"rowid"]) {
        NSNumber* oldRowId = history[@"rowid"];
        NSInteger newRowId = [s intForColumn:@"ROWID"];
        [db close];
        return @[oldRowId, [NSNumber numberWithInteger:newRowId]];
    }
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
    return nil;
}


+ (NSArray *)importHistory:(NSDictionary *)history
{
    FMDatabase *db = [UserDatabaseHelper historyDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s;
    
    // check existing record
    BOOL existingRecord = NO;
    if (history[@"type"]) {
        s = [db executeQuery:@"SELECT ROWID FROM history WHERE keywords LIKE ? AND code=? AND type=?",history[@"keywords"], history[@"code"], history[@"type"]];
    } else {
        s = [db executeQuery:@"SELECT ROWID FROM history WHERE keywords LIKE ? AND code=?", history[@"keywords"], history[@"code"]];
    }
    if ([s next]) {
        existingRecord = YES;
    }
    
    if (!existingRecord) {
        [db executeUpdate:@"INSERT INTO history VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", history[@"keywords"], history[@"created"], history[@"detail"], history[@"code"], history[@"starred"], history[@"state"], history[@"read"], history[@"skimmed"], history[@"items"], history[@"marked"], history[@"type"] ? history[@"type"] : [NSNumber numberWithInt:SearchTypeAll], history[@"note"], history[@"buddhawaj"], history[@"note_items"], history[@"note_state"], history[@"priority"], history[@"search_all"]];
    } else if (existingRecord && history[@"type"]) {
        [db executeUpdate:@"UPDATE history SET starred=?, state=?, read=?, skimmed=?, items=?, marked=?, note=? WHERE keywords LIKE ? AND code=? AND type=?", history[@"starred"], history[@"state"], history[@"read"], history[@"skimmed"], history[@"items"], history[@"marked"], history[@"note"], history[@"keywords"], history[@"code"], history[@"type"]];
    } else if (existingRecord && !history[@"type"]) {
        [db executeUpdate:@"UPDATE history SET starred=?, state=?, read=?, skimmed=?, items=?, marked=?, note=? WHERE keywords LIKE ? AND code=?", history[@"starred"], history[@"state"], history[@"read"], history[@"skimmed"], history[@"items"], history[@"marked"], history[@"note"], history[@"keywords"], history[@"code"]];
    }
    
    if (history[@"type"]) {
        s = [db executeQuery:@"SELECT ROWID FROM history WHERE keywords LIKE ? AND code=? AND type=?",history[@"keywords"], history[@"code"], history[@"type"]];
    } else {
        s = [db executeQuery:@"SELECT ROWID FROM history WHERE keywords LIKE ? AND code=?", history[@"keywords"], history[@"code"]];
    }
    
    if ([s next] && history[@"rowid"]) {
        NSNumber* oldRowId = history[@"rowid"];
        NSInteger newRowId = [s intForColumn:@"ROWID"];
        [db close];
        return @[oldRowId, [NSNumber numberWithInteger:newRowId]];
    }
    
    [db close];
    
    [[SyncDatabaseManager sharedInstance] delayPush:HISTORY_DB_PATH];
    
    return nil;
}

+ (NSDictionary *)importSearchAllHistories:(NSArray *)histories
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Importing search all histories", nil)];
    });

    int count = 0;
    NSMutableDictionary *mapRowIds = [NSMutableDictionary dictionary];
    for (NSDictionary *dict in histories) {
        NSArray *ret = [UserDatabaseHelper importSearchAllHistory:dict];
        count += 1;
        if (ret) {
            [mapRowIds setObject:ret[1] forKey:ret[0]];
        }
    }
    return mapRowIds;
}


+ (NSDictionary *)importHistories:(NSArray *)histories
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Importing histories", nil)];
    });

    int count = 0;
    NSMutableDictionary *mapRowIds = [NSMutableDictionary dictionary];
    for (NSDictionary *dict in histories) {
        NSArray *ret = [UserDatabaseHelper importHistory:dict];
        count += 1;
        if (ret) {
            [mapRowIds setObject:ret[1] forKey:ret[0]];
        }
    }
    return mapRowIds;
}

+ (void)importTags:(NSArray *)tags mapRowIds:(NSDictionary *)mapRowIds
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Importing tags", nil)];
    });
    
    int count = 0;
    for (NSDictionary *dict in tags) {
        [UserDatabaseHelper importTag:dict mapRowIds:mapRowIds];
        count += 1;
    }
}

+ (void)importTag:(NSDictionary *)tag mapRowIds:(NSDictionary *)mapRowIds
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    if (![db open]) { return; }
    
    NSMutableArray *bookmarkRowIds = [NSMutableArray array];
    for (NSString *token in [tag[@"note"] componentsSeparatedByString:@","]) {
        NSNumber *oldRowId = [NSNumber numberWithInteger:token.integerValue];
        NSNumber *newRowId = [[mapRowIds objectForKey:@"bookmarks"] objectForKey:oldRowId];
        if (newRowId) {
            [bookmarkRowIds addObject:newRowId];
        } else {
            NSLog(@"error");
        }
    }

    NSMutableArray *historyRowIds = [NSMutableArray array];
    for (NSString *token in [tag[@"history"] componentsSeparatedByString:@","]) {
        NSNumber *oldRowId = [NSNumber numberWithInteger:token.integerValue];
        NSNumber *newRowId = [[mapRowIds objectForKey:@"histories"] objectForKey:oldRowId];
        if (newRowId) {
            [historyRowIds addObject:newRowId];
        } else {
            NSLog(@"error");
        }
    }
    
    NSMutableArray *searchAllRowIds = [NSMutableArray array];
    for (NSString *token in [tag[@"search_all"] componentsSeparatedByString:@","]) {
        NSNumber *oldRowId = [NSNumber numberWithInteger:token.integerValue];
        NSNumber *newRowId = [[mapRowIds objectForKey:@"search_all"] objectForKey:oldRowId];
        if (newRowId) {
            [searchAllRowIds addObject:newRowId];
        } else {
            NSLog(@"error");
        }
    }
    
    NSMutableArray *highlightRowIds = [NSMutableArray array];
    for (NSString *token in [tag[@"highlight"] componentsSeparatedByString:@","]) {
        NSNumber *oldRowId = [NSNumber numberWithInteger:token.integerValue];
        NSNumber *newRowId = [[mapRowIds objectForKey:@"highlights"] objectForKey:oldRowId];
        if (newRowId) {
            [highlightRowIds addObject:newRowId];
        } else {
            NSLog(@"error");
        }
    }

    [db executeUpdate:@"DELETE FROM tag WHERE name=? AND code=?", tag[@"name"], tag[@"code"]];
    
    [db executeUpdate:@"INSERT INTO tag (name, note, history, highlight, search_all, priority, code) VALUES (?,?,?,?,?,?,?)",
     tag[@"name"],
     [bookmarkRowIds componentsJoinedByString:@","],
     [historyRowIds componentsJoinedByString:@","],
     [highlightRowIds componentsJoinedByString:@","],
     [searchAllRowIds componentsJoinedByString:@","],
     tag[@"priority"], tag[@"code"]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:TAG_DB_PATH];
}

+ (NSArray *)importHighlight:(NSDictionary *)highlight
{
    FMDatabase *db = [UserDatabaseHelper highlightDatabase];

    if (![db open]) { return nil; }

    BOOL existingRecord = NO;
    FMResultSet *s = [db executeQuery:@"SELECT ROWID FROM highlight WHERE volume=? AND page=? AND code=? AND start=? AND end=?", highlight[@"volume"], highlight[@"page"], highlight[@"code"], highlight[@"start"], highlight[@"end"]];
    if ([s next]) {
        existingRecord = YES;
    }

    if (!existingRecord) {
        [db executeUpdate:@"INSERT INTO highlight VALUES (?,?,?,?,?,?,?,?,?,?)", highlight[@"selection"], highlight[@"type"], highlight[@"note"], highlight[@"start"], highlight[@"end"], highlight[@"volume"], highlight[@"page"], highlight[@"code"], highlight[@"position"] ? highlight[@"position"] : @0, highlight[@"color"]];
    } else {
        [db executeUpdate:@"UPDATE highlight SET type=?, note=?, selection=? WHERE volume=? AND page=? AND code=? AND start=? AND end=?", highlight[@"type"], highlight[@"note"], highlight[@"selection"], highlight[@"volume"], highlight[@"page"], highlight[@"code"], highlight[@"start"], highlight[@"end"]];
    }
    
    s = [db executeQuery:@"SELECT ROWID FROM highlight WHERE volume=? AND page=? AND code=? AND start=? AND end=?", highlight[@"volume"], highlight[@"page"], highlight[@"code"], highlight[@"start"], highlight[@"end"]];

    if ([s next] && highlight[@"rowid"]) {
        NSNumber* oldRowId = highlight[@"rowid"];
        NSInteger newRowId = [s intForColumn:@"ROWID"];
        [db close];
        return @[oldRowId, [NSNumber numberWithInteger:newRowId]];
    }
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:HIGHLIGHT_DB_PATH];
    return nil;
}

+ (NSDictionary *)importHighlights:(NSArray *)highlights
{
    int count = 0;
    NSMutableDictionary *mapRowIds = [NSMutableDictionary dictionary];
    for (NSDictionary *dict in highlights) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
            [SVProgressHUD showProgress:(count*1.0)/highlights.count status:NSLocalizedString(@"Importing highlights", nil)];
        });
        NSArray *ret = [UserDatabaseHelper importHighlight:dict];
        count += 1;
        if (ret) {
            [mapRowIds setObject:ret[1] forKey:ret[0]];
        }
    }
    return mapRowIds;
}

+ (void)importOldBookmarks:(NSArray *)bookmarks
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"th_TH"]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    int count = 0;
    for (NSDictionary *bookmark in bookmarks) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
            [SVProgressHUD showProgress:(count*1.0)/bookmarks.count status:NSLocalizedString(@"Importing bookmarks", nil)];
        });
        NSInteger page = [BookDatabaseHelper pageOfItem:[bookmark[@"item_number"] integerValue] subItem:[bookmark[@"item_section"] integerValue] inVolume:[bookmark[@"volume"] integerValue] withCodeString:bookmark[@"lang"]];
        LanguageCode code = 0;
        if ([bookmark[@"lang"] isEqualToString:@"thai"]) {
            code = LanguageCodeThaiSiam;
        } else if ([bookmark[@"lang"] isEqualToString:@"pali"]) {
            code = LanguageCodePaliSiam;
        }
        [UserDatabaseHelper saveBookmark:bookmark[@"text"] volume:[bookmark[@"volume"] integerValue] page:page starred:[bookmark[@"important"] boolValue] created:[dateFormatter dateFromString:[bookmark objectForKey:@"created"]] append:YES withCode:code];
        count += 1;
    }
}

+ (NSArray *)importBookmark:(NSDictionary *)bookmark
{
    FMDatabase *db = [UserDatabaseHelper bookmarkDatabase];
    
    if (![db open]) { return nil; }
    
    BOOL existingRecord = NO;
    FMResultSet *s = [db executeQuery:@"SELECT created FROM bookmark WHERE code=? AND volume=? AND page=?", bookmark[@"code"], bookmark[@"volume"], bookmark[@"page"]];
    
    if ([s next]) {
        existingRecord = YES;
    }
    
    if (!existingRecord) {
        [db executeUpdate:@"INSERT INTO bookmark VALUES (?,?,?,?,?,?,?)", bookmark[@"created"], bookmark[@"important"], bookmark[@"note"], bookmark[@"rank"], bookmark[@"code"], bookmark[@"volume"], bookmark[@"page"]];
    } else {
        [db executeUpdate:@"UPDATE bookmark SET important=?, note=?, rank=? WHERE code=? AND volume=? AND page=?", bookmark[@"important"], bookmark[@"note"], bookmark[@"rank"], bookmark[@"code"], bookmark[@"volume"], bookmark[@"page"]];
    }
    
    s = [db executeQuery:@"SELECT ROWID FROM bookmark WHERE code=? AND volume=? AND page=?", bookmark[@"code"], bookmark[@"volume"], bookmark[@"page"]];
    if ([s next] && bookmark[@"rowid"]) {
        NSNumber* oldRowId = bookmark[@"rowid"];
        NSInteger newRowId = [s intForColumn:@"ROWID"];
        [db close];
        return @[oldRowId, [NSNumber numberWithInteger:newRowId]];
    }
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:BOOKMARK_DB_PATH];
    return nil;
}

+ (NSDictionary *)importBookmarks:(NSArray *)bookmarks
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Importing bookmarks", nil)];
    });

    int count = 0;
    NSMutableDictionary *mapRowIds = [NSMutableDictionary dictionary];
    for (NSDictionary *dict in bookmarks) {
        NSArray *ret = [self importBookmark:dict];
        count += 1;
        if (ret) {
            [mapRowIds setObject:ret[1] forKey:ret[0]];
        }
    }
    
    return mapRowIds;
}

+ (NSString *)readTextFromZipFile:(ZipFile *)zipFile
{
    ZipReadStream *readStream = [zipFile readCurrentFileInZip];
    NSMutableData *buffer = [[NSMutableData alloc] initWithLength:256];
    NSMutableData *data = [[NSMutableData alloc] init];
    do {
        [buffer setLength:256];
        NSUInteger bytesRead = [readStream readDataWithBuffer:buffer];
        if (!bytesRead) { break; }
        [buffer setLength:bytesRead];
        [data appendData:buffer];
    } while (YES);
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

}

+ (LanguageCode)languageCodeByCodeString:(NSString *)code
{
    if ([code isEqualToString:@"thai"]) {
        return LanguageCodeThaiSiam;
    } else if ([code isEqualToString:@"pali"]) {
        return LanguageCodePaliSiam;
    } else if ([code isEqualToString:@"thaimm"]) {
        return LanguageCodeThaiMahaMakut;
    } else if ([code isEqualToString:@"thaimc"]) {
        return LanguageCodeThaiMahaChula;
    } else if ([code isEqualToString:@"thaibt"]) {
        return LanguageCodeThaiFiveBooks;
    } else if ([code isEqualToString:@"thaiwn"]) {
        return LanguageCodeThaiWatna;
    } else if ([code isEqualToString:@"thaipb"]) {
        return LanguageCodeThaiPocketBook;
    } else if ([code isEqualToString:@"romanct"]) {
        return LanguageCodeRomanScript;
    }
    return 0;
}

+ (void)importUserDatabaseFromPC:(ZipFile *)zipFile
{
    while ([zipFile goToNextFileInZip]) {
        FileInZipInfo *info = [zipFile getCurrentFileInZipInfo];
        if ([info.name hasPrefix:@"notes/"]) {
            NSLog(@"%@", info.name);
            NSString *text = [UserDatabaseHelper readTextFromZipFile:zipFile];
            text = [text stringByReplacingOccurrencesOfString:@"xmlns=\"http://www.wxwidgets.org\"" withString:@""];
            CXMLDocument *doc = [[CXMLDocument alloc] initWithXMLString:text options:0 error:nil];
            NSArray *para_nodes = [doc nodesForXPath:@"//paragraph" error:nil];
            NSString *note_text = @"";
            for (CXMLElement *para_node in para_nodes) {
                for (int i=0; i < [para_node childCount]; ++i) {
                    CXMLNode *text_node = [para_node childAtIndex:i];
                    note_text = [note_text stringByAppendingString:[text_node stringValue]];
                }
                note_text = [note_text stringByAppendingString:@"\n"];
            }
            note_text = [note_text stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
            NSLog(@"%@", note_text);
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"/([a-zA-Z]+)/(\\d+)-(\\d+)\\.xml" options:NSRegularExpressionCaseInsensitive error:nil];
            NSArray *matches = [regex matchesInString:info.name options:0 range:NSMakeRange(0, info.name.length)];
            for (NSTextCheckingResult *match in matches) {
                NSNumberFormatter *formatter = [NSNumberFormatter new];
                formatter.numberStyle = NSNumberFormatterDecimalStyle;
                NSString *code = [[info.name substringWithRange:[match rangeAtIndex:1]] lowercaseString];
                NSNumber *volume = [formatter numberFromString:[info.name substringWithRange:[match rangeAtIndex:2]]];
                NSNumber *page = [formatter numberFromString:[info.name substringWithRange:[match rangeAtIndex:3]]];
                
                Bookmark *bookmark = [UserDatabaseHelper getBookmarkByVolume:volume.integerValue page:page.integerValue withCode:[UserDatabaseHelper languageCodeByCodeString:code]];
                if (bookmark && ![bookmark.note containsString:note_text]) {
                    [UserDatabaseHelper saveBookmark:note_text volume:bookmark.volume page:bookmark.page starred:bookmark.starred created:bookmark.created append:YES withCode:bookmark.code];
                } else if (!bookmark) {
                    [UserDatabaseHelper saveBookmark:note_text volume:volume.integerValue page:page.integerValue starred:NO created:[NSDate date] append:NO withCode:[UserDatabaseHelper languageCodeByCodeString:code]];
                }
            }
        }
    }
}

+ (void)importAndroidBookmarks:(NSArray *)items
{
    for (NSDictionary *item in items) {
        NSNumber *volume = [item objectForKey:@"volume_column"];
        NSNumber *page = [item objectForKey:@"page_column"];
        LanguageCode code = [[item objectForKey:@"language_column"] intValue]+1;
        NSString *note = [item objectForKey:@"note_column"];
        Bookmark *bookmark = [UserDatabaseHelper getBookmarkByVolume:volume.integerValue page:page.integerValue withCode:code];
        if (bookmark && ![bookmark.note containsString:note]) {
            [UserDatabaseHelper saveBookmark:note volume:bookmark.volume page:bookmark.page starred:bookmark.starred created:bookmark.created append:YES withCode:bookmark.code];
        } else if (!bookmark) {
            [UserDatabaseHelper saveBookmark:note volume:volume.integerValue page:page.integerValue starred:NO created:[NSDate date] append:NO withCode:code];
        }
    }
}

+ (void)importAndroidHistories:(NSArray *)items
{
    for (NSDictionary *item in items) {
        NSString *content = [item objectForKey:@"content_column"];
        LanguageCode code = [[item objectForKey:@"language_column"] intValue]+1;
        int section1 = [[item objectForKey:@"result1_column"] intValue];
        int section2 = [[item objectForKey:@"result2_column"] intValue];
        int section3 = [[item objectForKey:@"result3_column"] intValue];
        BOOL isBuddhawaj = [[item objectForKey:@"buddhawaj_column"] boolValue];
        History *history = [[History alloc] init];
        history.keywords = [item objectForKey:@"keywords_column"];
        history.detail = [UserDatabaseHelper detailByCode:code count:section1+section2+section3 section1:section1 section2:section2 section3:section3];
        history.state = 0;
        history.starred = NO;
        history.code = code;
        history.type = isBuddhawaj ? SearchTypeBuddhawaj : SearchTypeAll;
        NSMutableArray *readItems = [[NSMutableArray alloc] init];
        NSMutableArray *skimmedItems = [[NSMutableArray alloc] init];
        for (NSDictionary *item_status in [item objectForKey:@"history_item_table"]) {
            NSNumber *volume = [item_status objectForKey:@"volume_column"];
            NSNumber *page = [item_status objectForKey:@"page_column"];
            NSNumber *status = [item_status objectForKey:@"status_column"];
            if (status.intValue == 1) {
                [readItems addObject:[NSString stringWithFormat:@"%@:%@", volume, page]];
            } else if (status.intValue == 2) {
                [skimmedItems addObject:[NSString stringWithFormat:@"%@:%@", volume, page]];
            }
        }
        NSLog(@"%@", history.keywords);
        NSLog(@"%@", history.detail);
        [UserDatabaseHelper addHistory:history items:[content componentsSeparatedByString:@","] skimmedItems:skimmedItems readItems:readItems markedItems:@[] created:[NSDate date]];
    }
}


+ (void)importUserDatabase:(NSURL *)url
{
    NSError *error = nil;
    id obj = nil;
    if ([[url pathExtension] isEqualToString:@"etz"]) {
        ZipFile *zipFile = [[ZipFile alloc] initWithFileName:url.path mode:ZipFileModeUnzip];
        if ([zipFile numFilesInZip] > 1) {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [UserDatabaseHelper importUserDatabaseFromPC:zipFile];
            });
        } else {
            [zipFile goToFirstFileInZip];
            NSString *text = [UserDatabaseHelper readTextFromZipFile:zipFile];
            obj = [text objectFromJSONString];
        }
    } else {
        obj = [[NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error] objectFromJSONString];
        if (error) {
            NSLog(@"%@", error);
        }
    }
    
    if (!obj) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Import data error", nil)];
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if ([obj objectForKey:@"favorite_table"] || [obj objectForKey:@"history_table"]) {
            NSLog(@"Android");
            [UserDatabaseHelper importAndroidBookmarks:[obj objectForKey:@"favorite_table"]];
            [UserDatabaseHelper importAndroidHistories:[obj objectForKey:@"history_table"]];
        } else if (![obj objectForKey:@"version"]) {
            [UserDatabaseHelper importOldBookmarks:[obj objectForKey:@"bookmarks"]];
            [UserDatabaseHelper importOldHistories:[obj objectForKey:@"histories"]];
        } else if (obj) {
            NSMutableDictionary *mapRowIds = [NSMutableDictionary dictionary];
            [mapRowIds setObject:[UserDatabaseHelper importHistories:[obj objectForKey:@"histories"]] forKey:@"histories"];
            [mapRowIds setObject:[UserDatabaseHelper importBookmarks:[obj objectForKey:@"bookmarks"]] forKey:@"bookmarks"];
            [mapRowIds setObject:[UserDatabaseHelper importHighlights:[obj objectForKey:@"highlights"]] forKey:@"highlights"];
            [mapRowIds setObject:[UserDatabaseHelper importSearchAllHistories:[obj objectForKey:@"search_all"]] forKey:@"search_all"];
            
            [UserDatabaseHelper importSavedLexicons:[obj objectForKey:@"lexicons"]];
            [UserDatabaseHelper importReadingStates:[obj objectForKey:@"readingStates"]];
            [UserDatabaseHelper importNotes:[obj objectForKey:@"notes"]];
            [UserDatabaseHelper importTags:[obj objectForKey:@"tags"] mapRowIds:mapRowIds];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:RefreshHistoryListNotification object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:RefreshBookmarkListNotification object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:UpdateCurrentPageNotification object:nil];
            [SVProgressHUD dismiss];
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Import data successful", nil)];
        });
    });
}


+ (void)createUserDataDatabaseTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:USER_DATA_DB_PATH];
    if (![db open]) return;
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS userdata (gid INTEGER, deleting BOOLEAN, adding BOOLEAN, platform TEXT, path TEXT, username TEXT)"];
    
    [db close];
}


+ (void)saveUserData:(NSString *)username path:(NSString *)path platform:(NSString *)platform gid:(NSNumber *)gid
{
    FMDatabase *db = [UserDatabaseHelper userDataDatabase];
    
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM userdata WHERE path=? AND platform=? AND gid=?", path, platform, gid];
    
    if (![s next]) {
        [db executeUpdate:@"INSERT INTO userdata VALUES (?,?,?,?,?,?)", gid, @NO, @NO, platform, path, username];
    }
    
    [db close];
}

+ (NSMutableArray *)getDeletingUserDataItems:(NSString *)username
{
    FMDatabase *db = [UserDatabaseHelper userDataDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM userdata WHERE username = ? AND deleting=?", username, @YES];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    if ([s next]) {
        NSString *path = [s stringForColumn:@"path"];
        NSInteger gid = [s intForColumn:@"gid"];
        NSString *username = [s stringForColumn:@"username"];
        NSString *platform = [s stringForColumn:@"platform"];
        UserData *userData = [[UserData alloc] initWithUsername:username platform:platform path:path gid:[NSNumber numberWithInteger:gid]];
        userData.deleting = [s boolForColumn:@"deleting"];
        userData.adding = [s boolForColumn:@"adding"];
        [items addObject:userData];
    }
    
    [db close];
    
    return items;
}


+ (NSMutableArray *)getUserDataItems:(NSString *)username platform:(NSString *)platform
{
    FMDatabase *db = [UserDatabaseHelper userDataDatabase];
    
    if (![db open]) { return nil; }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM userdata WHERE username = ? AND platform=?", username, platform];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        NSString *path = [s stringForColumn:@"path"];
        NSInteger gid = [s intForColumn:@"gid"];
        UserData *userData = [[UserData alloc] initWithUsername:username platform:platform path:path gid:[NSNumber numberWithInteger:gid]];
        userData.deleting = [s boolForColumn:@"deleting"];
        userData.adding = [s boolForColumn:@"adding"];
        [items addObject:userData];
    }
    
    [db close];
    
    return items;
}

+ (void)updateUserDataAtColumn:(NSString *)column setValue:(id)value byGid:(NSInteger)gid
{
    FMDatabase *db = [UserDatabaseHelper userDataDatabase];
    
    if (![db open]) { return; }
    
    [db executeUpdate:[NSString stringWithFormat:@"UPDATE userdata SET %@=? WHERE gid = ?", column], value, [NSNumber numberWithInteger:gid]];
    
    [db close];
}

+ (BOOL)deleteUserDataByGid:(NSInteger)gid
{
    FMDatabase *db = [UserDatabaseHelper userDataDatabase];
    
    if (![db open]) return NO;
    
    BOOL ret = [db executeUpdate:@"DELETE FROM userdata WHERE gid=?", [NSNumber numberWithInteger:gid]];
    
    [db close];
    return ret;
}

+ (NSMutableArray *)queryTags
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM tag ORDER BY priority"];
    
    NSMutableArray *items = [NSMutableArray array];
    while ([s next]) {
        Tag *tag = [[Tag alloc] initWithName:[s stringForColumn:@"name"]];
        tag.code = [s intForColumn:@"code"];
        tag.priority = [NSNumber numberWithInteger:[s intForColumn:@"priority"]];
        tag.history = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"history"]];
        tag.note = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"note"]];
        tag.highlight = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"highlight"]];
        tag.searchAllhistory = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"search_all"]];
        [items addObject:tag];
    }
    return items;
}

+ (NSArray *)queryTagNamesFrom:(NSString *)column byRowId:(NSInteger)rowId withCode:(LanguageCode)code
{
    NSMutableArray *names = [NSMutableArray array];
    
    for (Tag *tag in [UserDatabaseHelper queryTags:code]) {
        NSArray *items;
        if ([column isEqualToString:@"history"]) {
            items = tag.history;
        } else if ([column isEqualToString:@"note"]) {
            items = tag.note;
        } else if ([column isEqualToString:@"highlight"]) {
            items = tag.highlight;
        } else if ([column isEqualToString:@"search_all"]) {
            items = tag.searchAllhistory;
        }
        for (NSNumber *item in items) {
            if (item.integerValue == rowId) {
                [names addObject:tag.name];
                break;
            }
        }
    }

    return names;
}

+ (Tag *)getTag:(NSString *)name withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM tag WHERE name = ? AND code = ?", name, [NSNumber numberWithInt:code]];
    
    Tag *tag;
    if ([s next]) {
        tag = [[Tag alloc] initWithName:[s stringForColumn:@"name"]];
        tag.code = [s intForColumn:@"code"];
        tag.priority = [NSNumber numberWithInteger:[s intForColumn:@"priority"]];
        tag.history = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"history"]];
        tag.note = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"note"]];
        tag.highlight = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"highlight"]];
        tag.searchAllhistory = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"search_all"]];
    }
    [db close];
    return tag;
}

+ (NSMutableArray *)queryTags:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM tag WHERE code = ? ORDER BY priority", [NSNumber numberWithInt:code]];
    
    NSMutableArray *items = [NSMutableArray array];
    while ([s next]) {
        Tag *tag = [[Tag alloc] initWithName:[s stringForColumn:@"name"]];
        tag.code = code;
        tag.priority = [NSNumber numberWithInteger:[s intForColumn:@"priority"]];
        tag.history = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"history"]];
        tag.note = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"note"]];
        tag.highlight = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"highlight"]];
        tag.searchAllhistory = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:@"search_all"]];
        [items addObject:tag];
    }
    return items;
}

+ (NSMutableArray *)queryTagNames
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM tag ORDER BY name"];
    
    NSMutableArray *items = [NSMutableArray array];
    while ([s next]) {
        NSString *name = [s stringForColumn:@"name"];
        if (![items containsObject:name]) {
            [items addObject:name];
        }
    }
    return items;
}


+ (NSMutableArray *)queryTagNames:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM tag WHERE code = ? ORDER BY name", [NSNumber numberWithInt:code]];
    
    NSMutableArray *items = [NSMutableArray array];
    while ([s next]) {
        [items addObject:[s stringForColumn:@"name"]];
    }
    return items;
}


+ (void)addTag:(NSString *)name withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM tag WHERE name=? AND code=?", name, [NSNumber numberWithInt:code]];
    
    if (![s next]) {
        [db executeUpdate:@"INSERT INTO tag (name, note, history, highlight, priority, code) VALUES (?,?,?,?,?,?)", name, @"", @"", @"", @0, [NSNumber numberWithInt:code]];
    }
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:TAG_DB_PATH];
}

+ (BOOL)deleteTag:(NSString *)name withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return NO;
    
    BOOL ret = [db executeUpdate:@"DELETE FROM tag WHERE name=? AND code=?", name, [NSNumber numberWithInt:code]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:TAG_DB_PATH];
    return ret;
}

+ (void)updateTagPriority:(NSNumber *)priority ofTag:(NSString *)name withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"UPDATE tag SET priority=? WHERE name = ? AND code = ?", priority, name, [NSNumber numberWithInt:code]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:TAG_DB_PATH];
}

+ (void)updateTagName:(NSString *)name ofTag:(Tag *)tag
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return;
    
    [db executeUpdate:@"UPDATE tag SET name=? WHERE name = ? AND code = ?", name, tag.name, [NSNumber numberWithInt:tag.code]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:TAG_DB_PATH];
}

+ (void)updateTagItems:(NSString *)column items:(NSArray *)items ofTag:(NSString *)name withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return;

    NSMutableArray *sItems = [NSMutableArray array];
    for (NSNumber *item in items) {
        NSString *sItem = [NSString stringWithFormat:@"%@", item];
        if (![sItems containsObject:sItem]) {
            [sItems addObject:sItem];
        }
    }

    [db executeUpdate:[NSString stringWithFormat:@"UPDATE tag SET %@=? WHERE name=? AND code=?", column], [sItems componentsJoinedByString:@","], name, [NSNumber numberWithInt:code]];
    
    [db close];
    [[SyncDatabaseManager sharedInstance] delayPush:TAG_DB_PATH];
}

+ (void)addTagItems:(NSString *)column rowId:(NSInteger)rowId ofTag:(NSString *)name withCode:(LanguageCode)code
{
    [UserDatabaseHelper addTag:name withCode:code];
    
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return;

    NSMutableArray *items = [[UserDatabaseHelper getTagItems:column ofTag:name withCode:code] mutableCopy];
    [items insertObject:[NSNumber numberWithInteger:rowId] atIndex:0];
    [UserDatabaseHelper updateTagItems:column items:items ofTag:name withCode:code];
    
    [db close];
}

+ (NSMutableArray *)convertTagStringItems:(NSString *)items
{
    NSMutableArray *arrayItems = [[NSMutableArray alloc] init];
    if (!items.length) {
        return arrayItems;
    }
    for (NSString *item in [items componentsSeparatedByString:@","]) {
        [arrayItems addObject:[NSNumber numberWithInteger:[item integerValue]]];
    }
    return arrayItems;
}

+ (NSArray *)getTagItems:(NSString *)column ofTag:(NSString *)name withCode:(LanguageCode)code
{
    FMDatabase *db = [UserDatabaseHelper tagDatabase];
    
    if (![db open]) return nil;
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM tag WHERE name = ? AND code = ?", name, [NSNumber numberWithInt:code]];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    if ([s next]) {
        items = [UserDatabaseHelper convertTagStringItems:[s stringForColumn:column]];
    }
    [db close];
    
    return items;
}

+ (NSArray *)getSuggestWords:(NSString *)word code:(NSString *)stringCode
{
    NSMutableArray *words = [@[] mutableCopy];
    
    FMDatabase *db = [UserDatabaseHelper suggestDatabase];
    if (![db open]) return nil;
    
    NSString *statement = [NSString stringWithFormat:@"SELECT word FROM %@ WHERE word GLOB ?", stringCode];
    FMResultSet *s = [db executeQuery:statement, [NSString stringWithFormat:@"%@*", word]];
    while ([s next]) {
        [words addObject:[s stringForColumn:@"word"]];
    }

    return words;
}

@end
