//
//  SearchAllHistory.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 9/11/20.
//  Copyright © 2020 Watnapahpong. All rights reserved.
//

#import "SearchAllHistory.h"

@implementation SearchAllHistory

- (NSDictionary *)dictionaryValue
{
    return @{
        @"rowid": [NSNumber numberWithInteger:self.rowId],
        @"keywords": self.keywords,
        @"type": [NSNumber numberWithInteger:self.type],
        @"detail": self.detail,
        @"state": [NSNumber numberWithInteger:self.state],
        @"note" : self.note ? self.note : @"",
        @"note_state": [NSNumber numberWithUnsignedInteger:self.noteState],
        @"priority": [NSNumber numberWithInteger:self.priority],
        @"created":[NSNumber numberWithDouble:self.created.timeIntervalSince1970]
    };
}

@end
