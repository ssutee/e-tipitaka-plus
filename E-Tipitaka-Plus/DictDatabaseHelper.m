//
//  DictDatabaseHelper.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/1/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "DictDatabaseHelper.h"
#import "Lexicon.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AFNetworking/AFNetworking.h>

@implementation DictDatabaseHelper

+ (void)destroyDictionaryTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:PALI_DICT_DB_PATH];
    if (![db open]) return;
    [db executeUpdate:@"DELETE FROM pali_thai"];
    [db close];
}

+ (BOOL)checkDictionaryTable
{
    FMDatabase *db = [FMDatabase databaseWithPath:PALI_DICT_DB_PATH];
    if (![db open]) return NO;
    FMResultSet *s = [db executeQuery:@"SELECT sql FROM SQLITE_MASTER WHERE name = pali_thai"];
    BOOL ret = [s next];
    [db close];
    return ret;
}

+ (NSArray *)queryEnglishDictionary:(NSString *)keyword
{
    FMDatabase *db = [FMDatabase databaseWithPath:ENG_DICT_DB_PATH];
    if (![db open]) return nil;
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    if (keyword.length == 0) {
        return  result;
    }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM english WHERE head GLOB ? ORDER BY head", [NSString stringWithFormat:@"%@*", keyword.lowercaseString]];
    
    while ([s next]) {
        Lexicon *lexicon = [[Lexicon alloc] initWithHead:[s stringForColumn:@"head"] andTranslation:[s stringForColumn:@"translation"]];
        [result addObject:lexicon];
    }
    return result;
}

+ (NSArray *)queryThaiDictionary:(NSString *)keyword
{
    FMDatabase *db = [FMDatabase databaseWithPath:THAI_DICT_DB_PATH];
    if (![db open]) return nil;
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    if (keyword.length == 0) {
        return  result;
    }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM thai WHERE head GLOB ? ORDER BY head", [NSString stringWithFormat:@"%@*", keyword]];
    
    while ([s next]) {
        Lexicon *lexicon = [[Lexicon alloc] initWithHead:[s stringForColumn:@"head"] andTranslation:[s stringForColumn:@"translation"]];
        [result addObject:lexicon];
    }
    return result;
}


+ (NSArray *)queryDictionary:(NSString *)keyword
{
    FMDatabase *db = [FMDatabase databaseWithPath:PALI_DICT_DB_PATH];
    if (![db open]) return nil;

    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    if (keyword.length == 0) {
        return  result;
    }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM pali_thai WHERE head GLOB ? ORDER BY head", [NSString stringWithFormat:@"%@*", keyword]];

    while ([s next]) {
        Lexicon *lexicon = [[Lexicon alloc] initWithHead:[s stringForColumn:@"head"] andTranslation:[s stringForColumn:@"translation"]];
        [result addObject:lexicon];
    }    
    return result;
}

@end
