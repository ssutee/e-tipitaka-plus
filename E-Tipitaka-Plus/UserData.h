//
//  UserData.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 16/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSNumber *gid;
@property (nonatomic, strong) NSString *platform;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, assign) BOOL adding;
@property (nonatomic, assign) BOOL deleting;
@property (nonatomic, assign) NSInteger rowId;

- (instancetype)initWithUsername:(NSString *)username platform:(NSString *)platform path:(NSString *)path gid:(NSNumber *)gid;

@end
