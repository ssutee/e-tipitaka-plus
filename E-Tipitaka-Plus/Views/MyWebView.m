//
//  MyWebView.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 6/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "MyWebView.h"

@implementation MyWebView

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{    
    return [super canPerformAction:action withSender:sender];
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

@end
