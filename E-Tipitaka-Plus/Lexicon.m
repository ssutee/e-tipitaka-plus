//
//  Lexicon.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/1/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "Lexicon.h"

@implementation Lexicon

@synthesize head = _head;
@synthesize translation = _translation;
@synthesize type = _type;

- (id)initWithHead:(NSString *)head andTranslation:(NSString *)translation
{
    self = [super init];
    if (self) {
        self.head = head;
        self.translation = translation;
    }
    return self;
}

- (NSDictionary *)dictionaryValue
{
    return @{@"head":self.head, @"translation":self.translation, @"type":[NSNumber numberWithInt:self.type]};
}

- (BOOL)isEqual:(id)object
{
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[Lexicon class]]) {
        return NO;
    }
    Lexicon *lexicon = object;
    return [self.head isEqualToString:[object head]] && [self.translation isEqualToString:lexicon.translation];
}

- (NSUInteger)hash
{
    NSString *s = [NSString stringWithFormat:@"%@ %@ %d", self.head, self.translation, self.type];
    return [s hash];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", self.head];
}

@end
