//
//  Highlight.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 7/2/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "Highlight.h"

@implementation Highlight

@synthesize rowId = _rowId;
@synthesize note = _note;
@synthesize selection = _selection;
@synthesize position = _position;
@synthesize range = _range;
@synthesize type = _type;
@synthesize volume = _volume;
@synthesize page = _page;
@synthesize code = _code;

- (id)initFromResultSet:(FMResultSet *)s
{
    self = [super init];
    if (self) {
        self.rowId = [s intForColumn:@"ROWID"];
        self.selection = [s stringForColumn:@"selection"];
        self.note = [s stringForColumn:@"note"];
        self.range = NSMakeRange([s intForColumn:@"start"], [s intForColumn:@"end"] - [s intForColumn:@"start"]);
        self.type = [s intForColumn:@"type"];
        self.volume = [s intForColumn:@"volume"];
        self.page = [s intForColumn:@"page"];
        self.code = [s intForColumn:@"code"];
        self.position = [s intForColumn:@"position"];
        self.color = [s intForColumn:@"color"];
    }
    return self;
}

- (NSDictionary *)dictionaryValue
{
    return @{@"rowid":[NSNumber numberWithInteger:self.rowId],
             @"selection":self.selection,
             @"position": [NSNumber numberWithInteger:self.position],
             @"note": self.note,
             @"start":[NSNumber numberWithInteger:self.range.location],
             @"end":[NSNumber numberWithInteger:self.range.location+self.range.length],
             @"type":[NSNumber numberWithInt:self.type],
             @"code":[NSNumber numberWithInt:self.code],
             @"volume":[NSNumber numberWithInteger:self.volume],
             @"color":[NSNumber numberWithInteger:self.color],
             @"page":[NSNumber numberWithInteger:self.page]};
}

@end
