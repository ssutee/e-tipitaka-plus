//
//  UIWebView+Javascript.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 19/8/14.
//  Copyright (c) 2014 Watnapahpong. All rights reserved.
//

#import "UIWebView+Javascript.h"

@implementation UIWebView (Javascript)

- (void)loadJavascriptFileFromResourceBundle:(NSString *)fileName
{
    NSString *jsFile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    NSError *error = nil;
    NSString *js = [NSString stringWithContentsOfFile:jsFile encoding:NSUTF8StringEncoding error:&error];
    [self stringByEvaluatingJavaScriptFromString:js];
}

@end
