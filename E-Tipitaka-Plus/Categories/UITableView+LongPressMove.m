//
//  UITableView+LongPressMove.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import "UITableView+LongPressMove.h"
#import "UIView+Snapshot.h"
#import <objc/runtime.h>

@implementation UITableView (LongPressMove)

static void * longPressMoveDelegatePropertyKey = &longPressMoveDelegatePropertyKey;

- (id<LongPressMoveDelegate>)longPressMoveDelegate
{
    return objc_getAssociatedObject(self, longPressMoveDelegatePropertyKey);
}

- (void)setLongPressMoveDelegate:(id<LongPressMoveDelegate>)longPressMoveDelegate
{
     objc_setAssociatedObject(self, longPressMoveDelegatePropertyKey, longPressMoveDelegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)enableLongPressMove:(NSInteger)numberOfTouchesRequired
{
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressForMoving:)];
    recognizer.numberOfTouchesRequired = numberOfTouchesRequired;
    recognizer.minimumPressDuration = 0.5;
    recognizer.delegate = self;
    [self addGestureRecognizer:recognizer];
}


// Copy from http://www.raywenderlich.com/63089/cookbook-moving-table-view-cells-with-a-long-press-gesture
-(void)handleLongPressForMoving:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:self];
    NSIndexPath *indexPath = [self indexPathForRowAtPoint:location];
    
    if ([self.longPressMoveDelegate skipLongPressMove:indexPath]) {
        return;
    }
    
    UIGestureRecognizerState state = gestureRecognizer.state;
    
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                UITableViewCell *cell = [self cellForRowAtIndexPath:indexPath];
                
                // Take a snapshot of the selected row using helper method.
                snapshot = [cell customSnapshot];
                
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [self addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    cell.alpha = 0.0;
                } completion:^(BOOL finished) {
                    cell.hidden = YES;
                }];
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                [self moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                [self.longPressMoveDelegate doLongPressMove:sourceIndexPath target:indexPath];
                sourceIndexPath = indexPath;
            }
            break;
        }
            
        default: {
            // Clean up.
            UITableViewCell *cell = [self cellForRowAtIndexPath:sourceIndexPath];
            cell.hidden = NO;
            cell.alpha = 0.0;
            
            [UIView animateWithDuration:0.25 animations:^{
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                cell.alpha = 1.0;
            } completion:^(BOOL finished) {
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
            }];
            break;
        }
    }
}



@end
