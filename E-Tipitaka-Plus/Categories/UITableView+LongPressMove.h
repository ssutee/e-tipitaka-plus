//
//  UITableView+LongPressMove.h
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 22/12/15.
//  Copyright © 2015 Watnapahpong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LongPressMoveDelegate <NSObject>

- (BOOL)skipLongPressMove:(NSIndexPath *)indexPath;
- (void)doLongPressMove:(NSIndexPath *)source target:(NSIndexPath *)target;

@end

@interface UITableView (LongPressMove)<UIGestureRecognizerDelegate>

- (void)enableLongPressMove:(NSInteger)numberOfTouchesRequired;
@property (nonatomic, strong) id<LongPressMoveDelegate> longPressMoveDelegate;

@end
