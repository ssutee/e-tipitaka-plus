//
//  NSString+Thai.m
//  E-Tipitaka-Plus
//
//  Created by Sutee Sudprasert on 5/11/13.
//  Copyright (c) 2013 Watnapahpong. All rights reserved.
//

#import "NSString+Thai.h"

@implementation NSString (Thai)

- (NSString *)stringByReplacingThaiNumeric
{
    NSString *result = [self stringByReplacingOccurrencesOfString:@"0" withString:THAI_0];
    result = [result stringByReplacingOccurrencesOfString:@"1" withString:THAI_1];
    result = [result stringByReplacingOccurrencesOfString:@"2" withString:THAI_2];
    result = [result stringByReplacingOccurrencesOfString:@"3" withString:THAI_3];
    result = [result stringByReplacingOccurrencesOfString:@"4" withString:THAI_4];
    result = [result stringByReplacingOccurrencesOfString:@"5" withString:THAI_5];
    result = [result stringByReplacingOccurrencesOfString:@"6" withString:THAI_6];
    result = [result stringByReplacingOccurrencesOfString:@"7" withString:THAI_7];
    result = [result stringByReplacingOccurrencesOfString:@"8" withString:THAI_8];
    result = [result stringByReplacingOccurrencesOfString:@"9" withString:THAI_9];
    return result;
}

- (NSInteger)integerValueFromThaiNumeric
{
    NSString *result = [self stringByReplacingOccurrencesOfString:THAI_0 withString:@"0"];
    result = [result stringByReplacingOccurrencesOfString:THAI_1 withString:@"1"];
    result = [result stringByReplacingOccurrencesOfString:THAI_2 withString:@"2"];
    result = [result stringByReplacingOccurrencesOfString:THAI_3 withString:@"3"];
    result = [result stringByReplacingOccurrencesOfString:THAI_4 withString:@"4"];
    result = [result stringByReplacingOccurrencesOfString:THAI_5 withString:@"5"];
    result = [result stringByReplacingOccurrencesOfString:THAI_6 withString:@"6"];
    result = [result stringByReplacingOccurrencesOfString:THAI_7 withString:@"7"];
    result = [result stringByReplacingOccurrencesOfString:THAI_8 withString:@"8"];
    result = [result stringByReplacingOccurrencesOfString:THAI_9 withString:@"9"];
    return [result integerValue];
}

- (NSNumber *)numberValueFromThaiNumberic
{
    return [NSNumber numberWithInteger:[self integerValueFromThaiNumeric]];
}

@end
